﻿Imports System.Net.Sockets
Imports System.Net

Public Class User
    Public Name As String
    Public IP As Net.IPAddress
    Public Painter As Boolean
    Public Rank As Integer
    Public Client As TcpClient
    Public Serial As String
    Public Score As Integer
    Public UniqueReports As New List(Of String)
    Public LastReport As DateTime
    Public Location As String

    Public BlockedUsers As New List(Of String)

    Public ActualName As String
    Public UnChangedAccountName As String

    Public ReadOnly Property Reports As Integer
        Get
            Return UniqueReports.Count
        End Get
    End Property

    Public ReadOnly Property RankName As String
        Get
            If Rank = 0 Then Return "Player"
            If Rank = 1 Then Return "Moderator"
            If Rank = 2 Then Return "Admin"
            If Rank > 2 Then Return "Manager"
            Return "Error."
        End Get
    End Property

    Public ReadOnly Property Manager As Boolean
        Get ' User that is the server
            Return Rank > 2
        End Get
    End Property

    Public ReadOnly Property Admin As Boolean
        Get ' Manager essentialy has full permissions of Server.
            Return Rank > 1
        End Get
    End Property
    Public ReadOnly Property Moderator As Boolean
        Get
            Return Rank > 0
        End Get
    End Property
    Public Function AddReport(reporter As String) As Boolean
        Dim now = DateTime.Now()
        Dim diff = now - LastReport
        If diff.TotalSeconds > 60 Then
            ' we reset now, since 1 minute has passed since last report
            ' and we presume the previous reports are indiffernt
            UniqueReports.Clear()
        End If
        If UniqueReports.Contains(reporter) Then
            Return False
        End If
        UniqueReports.Add(reporter)
        LastReport = DateTime.Now()
        Return True
    End Function
    Public Sub New(_name As String, _ip As IPAddress, _rank As Integer, _client As TcpClient, _serial As String)
        Name = _name
        IP = _ip
        Painter = False
        Rank = _rank
        Client = _client
        Score = 0
        Serial = _serial
        LastReport = DateTime.Now()
    End Sub
End Class
