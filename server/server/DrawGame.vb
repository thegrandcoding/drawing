﻿Public Class DrawGame
    Public Painter As String
    Public ThingToDraw As DrawItem
    Public Winner As String

    Public Sub New(pnt As String, drw As DrawItem)
        Painter = pnt
        ThingToDraw = drw
    End Sub

    ''' <summary>
    ''' Checks to see if inputted value is either:
    ''' - The main word itself
    ''' - An alias of the word
    ''' </summary>
    ''' <param name="guess">Phrase inputted by user</param>
    ''' <returns>True if it is the correct answer.</returns>
    Public Function IsCorrectGuess(guess As String) As Boolean
        ' Should fix the upper case needs.
        Dim values As New List(Of String)
        values.Add(ThingToDraw.Main)
        values.AddRange(ThingToDraw.Aliases)
        guess = guess.Trim()
        For Each value As String In values
            value = value.Trim()
            value = value.Replace(".", "").Replace(",", "") ' punctuation.
            If value.ToLower = guess.ToLower() Then
                Return True
            End If
        Next
        Return False
    End Function
End Class

Public Class DrawItem
    Private Shared Rnd As New Random(DateTime.Now.Millisecond * DateTime.Now.Second)
    Public Property Main As String ' Read-only public access
    Public Property Aliases As List(Of String)
    Public ReadOnly ToDraw As String

    Public Sub New(_main As String, _aliases As List(Of String))
        Main = _main
        Aliases = _aliases
        Dim index As Integer = Rnd.Next(-1, Aliases.Count - 1)
        If index = -1 Then
            ToDraw = Main
        Else
            ToDraw = Aliases.Item(index)
        End If
    End Sub
End Class
