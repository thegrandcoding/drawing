﻿Imports System.IO
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Public Module LogHandle
    ' I will now just have a single log file.

    Public SavePath As String = "logs/"
    Public LogSaveFilePath As String = SavePath + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"

    ''' <summary>
    ''' Can be set in config.json: levels below this will simply not be logged 
    ''' (but will still be in console)
    ''' 
    ''' Please note that the first few console messages will be logged regardless
    ''' </summary>
    Public ReadOnly Property LogExtent As LogSeverity
        Get
            If _CONFIG Is Nothing Then Return LogSeverity.Debug ' console will log some messages before this is read
            Dim ext As JToken = CType(_CONFIG("log-level"), JToken)
            Dim value As String = ext.Value(Of String)
            Select Case value.ToLower()
                Case "debug"
                    LogExtent = LogSeverity.Debug
                Case "info"
                    LogExtent = LogSeverity.Info
                Case "warning", "warn"
                    LogExtent = LogSeverity.Warning
                Case "error"
                    LogExtent = LogSeverity.Error
                Case "none", "off", "disabled", "disable", ""
                    LogEnabled = False
                    LogExtent = LogSeverity.Error
                Case Else
                    LogExtent = LogSeverity.Debug
                    LogMsg("Config file invalid, value unknown: " + value.ToLower(), "LogExtent")
            End Select
        End Get
    End Property

    Private FileInUse As Boolean = False
    Private LogEnabled As Boolean = True
    Private Sub WaitLog()
        While FileInUse
            Threading.Thread.Sleep(1)
        End While
        FileInUse = True
    End Sub

    Dim hooksUrl As String = ""

    ''' <summary>
    ''' Logs the LogMessage - I would advise you use the other LogMsg overloads.
    ''' </summary>
    Public Sub LogMsg(lgMsg As LogMessage)
        If String.IsNullOrWhiteSpace(hooksUrl) Then
            Dim currentPath = IO.Directory.GetCurrentDirectory()
            If currentPath.Contains("server\server\bin\Debug") Or currentPath.Contains("server\server\bin\Release") Then
                Dim parent = Directory.GetParent(currentPath) ' server/server/bin
                parent = Directory.GetParent(parent.FullName) ' server/server
                parent = Directory.GetParent(parent.FullName) ' server
                parent = Directory.GetParent(parent.FullName) ' repository base
                Dim githookPlace = parent.FullName + "\.git\hooks"
                hooksUrl = githookPlace
                If Directory.Exists(githookPlace) Then
                    Dim preCommitHook As String = githookPlace + "\pre-commit"
                    If File.Exists(preCommitHook) Then
                        ' do nothing, since its already set up
                    Else
                        ' we need to write the pre-commit hook (from Resources)
                        File.WriteAllBytes(preCommitHook, My.Resources.pre_commit)
                    End If
                End If
            End If
        End If
        If lgMsg.SeverityInt < Convert.ToInt32(LogExtent) Then
            Return
        End If
        Try
            If LogEnabled = False Then Return
            WaitLog()
            If System.IO.Directory.Exists(SavePath) = False Then
                Directory.CreateDirectory(SavePath)
            End If
            Dim toWrite As String = lgMsg.ToString() + vbCrLf
            File.AppendAllText(LogSaveFilePath, toWrite)
        Catch ex As Exception
        Finally
            FileInUse = False
        End Try
    End Sub

    ''' <summary>
    ''' Writes a message to the log.
    ''' </summary>
    Public Sub LogMsg(msg As String)
        Dim log As New LogMessage(msg)
        LogMsg(log)
    End Sub

    ''' <summary>
    ''' Writes a message to log, with the indicated source
    ''' </summary>
    ''' <param name="msg">Message to log</param>
    ''' <param name="src">Location where the message is coming from</param>
    Public Sub LogMsg(msg As String, src As String)
        LogMsg(New LogMessage(msg, src))
    End Sub

    ''' <summary>
    ''' Writes message to log, with indicated source and severity
    ''' </summary>
    ''' <param name="sev">The severity of the messsage</param>
    Public Sub LogMsg(sev As LogSeverity, msg As String, src As String)
        LogMsg(New LogMessage(sev, msg, src))
    End Sub

    ''' <summary>
    ''' Logs an exception in correct formatting
    ''' </summary>
    ''' <param name="exp"></param>
    ''' <param name="src"></param>
    Public Sub LogMsg(exp As Exception, src As String)
        LogMsg(New LogMessage(exp, src))
    End Sub

    Public Enum LogSeverity
        Debug = 0
        Info = 1
        Warning = 2
        [Error] = 3
    End Enum

    Public Structure LogMessage
        Public Message As String
        Public Source As String
        Public Severity As LogSeverity
        Public [Error] As Exception

        Public ReadOnly Property SeverityInt As Integer
            Get
                Return Convert.ToInt32(Severity)
            End Get
        End Property

        Public Sub New(msg As String)
            Message = msg
            Source = "App"
            Severity = LogSeverity.Debug
        End Sub

        Public Sub New(msg As String, src As String)
            Message = msg
            Source = src
            Severity = LogSeverity.Debug
        End Sub

        Public Sub New(sev As LogSeverity, msg As String, src As String)
            Message = msg
            Source = src
            Severity = sev
        End Sub

        Public Sub New(exp As Exception, src As String)
            Severity = LogSeverity.Error
            Source = src
            [Error] = exp
        End Sub

        Public Overrides Function ToString() As String
            Return $"{DateTime.Now.ToString("hh:mm:ss.fff")} [{Severity}] {Source}: {If([Error] Is Nothing, Message, [Error].ToString())}"
        End Function
    End Structure
End Module
