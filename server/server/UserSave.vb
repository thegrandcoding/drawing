﻿Public Structure UserSave
    Dim AccountName As String
    Dim Serial As String
    Dim Score As Integer
    Dim Rank As Integer

    Public Overrides Function ToString() As String
        Return AccountName + " " + Serial + " " + Score.ToString() + " " + Rank.ToString()
    End Function
End Structure