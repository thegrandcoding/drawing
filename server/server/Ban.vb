﻿Public Class Ban
    Public Name As String
    Public IP As String
    Public ActualName As String
    Public Serial As String
    Public Reason As String
    Public IP_Ban As Boolean
    Public Serial_Ban As Boolean
    Public Name_Ban As Boolean
    Public Sub New(_user As User, type As String, _reason As String)
        Name = _user.Name
        ActualName = _user.ActualName
        Reason = _reason
        IP = _user.IP.ToString()
        Serial = _user.Serial
        Name_Ban = True
        IP_Ban = False
        Serial_Ban = False
        If type = "IP" Then
            IP_Ban = True
        ElseIf type = "Serial" Then
            Serial_Ban = True
        ElseIf type = "Both" Then
            IP_Ban = True
            Serial_Ban = True
        Else
        End If
    End Sub
    Public Sub New()

    End Sub
    Public Overrides Function ToString() As String
        Return Name + "," + IP.ToString() + "," + Serial + "," + IP_Ban.ToString() + "," + Serial_Ban.ToString() + "," + ActualName.Replace(" ", "_")
    End Function
    Public Shared Function FromString(_line As String) As Ban
        Dim lineSplit = _line.Split(",")
        Dim newBan As Ban = New Ban With {
        .Name = lineSplit(0),
        .IP = lineSplit(1),
        .Serial = lineSplit(2),
        .IP_Ban = Boolean.Parse(lineSplit(3)),
        .Serial_Ban = Boolean.Parse(lineSplit(4)),
        .ActualName = lineSplit(5).Replace("_", " ")
    }
        Return newBan
    End Function
End Class