Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("server")>
<Assembly: AssemblyDescription("")>
<Assembly: AssemblyCompany("")>
<Assembly: AssemblyProduct("server")>
<Assembly: AssemblyCopyright("")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("b6b76f3b-48f7-4708-8d98-14b78da33317")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers
' by using the '*' as shown below:

#Disable Warning BC42366 ' The specified version string does not conform to the recommended format
<Assembly: AssemblyVersion("6.0.6808.22454")>
<Assembly: AssemblyFileVersion("6.0.6808.22454")>
#Enable Warning BC42366 ' The specified version string does not conform to the recommended format
