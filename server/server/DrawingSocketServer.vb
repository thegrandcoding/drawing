﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports Newtonsoft.Json.Linq
Imports System.Text
Imports System.Threading
Imports System.Windows.Forms
Imports System.Text.RegularExpressions

Public Module DrawingSocketServer
    Private serverSocket As New TcpListener(IPAddress.IPv6Any, 8888)
    Public BannedUsers As New List(Of Ban)
    Public previousPoints As List(Of String) = New List(Of String)
    Public AllWordItems As New Dictionary(Of String, WordList)

    Public MasterListDisabled As Boolean = False

    Public ServerPassword As String
    Public ReadOnly Property IsPasswordProtected
        Get
            Return Not String.IsNullOrWhiteSpace(ServerPassword)
        End Get
    End Property

    ''' <summary>
    ''' Raised when all pre-run functions/subs have been called and the socket is opened
    ''' </summary>
    Public Event Ready()
    ''' <summary>
    ''' Raised when a new client connects to the server's TCP listener
    ''' </summary>
    Public Event UserConnected(client As TcpClient, clientObj As User)
    ''' <summary>
    ''' Raised when a client disconnects/connection is lost, passes the user object if available.
    ''' <para>Note that both or either of these items may be null</para>
    ''' </summary>
    ''' <param name="client">TCP connection; may be passed if the client has only notified they are disconnecting</param>
    ''' <param name="userObj">User class of the client that is disconnecting/already left</param>
    Public Event UserDisconnect(client As TcpClient, userObj As User)
    ''' <summary>
    ''' Raised when the client gives a message to the server
    ''' </summary>
    ''' <param name="client">TCP connection with the client</param>
    ''' <param name="userObj">User object of the client</param>
    ''' <param name="clientName">Shortcut name of the User object</param>
    Public Event UserMessage(client As TcpClient, userObj As User, clientName As String, message As String)

    ''' <summary>
    ''' Raised whenever a setting (eg, Who can vote, difficulty etc) is updated
    ''' </summary>
    ''' <param name="settingName">Uniquely identifiable name of the setting updated</param>
    ''' <param name="previousValue">Previous value of the setting</param>
    ''' <param name="newValue">Current value of the setting</param>
    ''' <param name="thouWhoPerformed">Operator, who changed it - may be ommited</param>
    Public Event SettingUpdated(settingName As String, previousValue As String, newValue As String, thouWhoPerformed As String)
    ''' <summary>
    ''' Call this to raise the SettingUpdated event
    ''' </summary>
    ''' <param name="settingName">Uniquely identifiable name of the setting updated</param>
    ''' <param name="previousValue">Previous value of the setting</param>
    ''' <param name="newValue">Current value of the setting</param>
    ''' <param name="thouWhoPerformed">Operator, who changed it - may be ommited</param>
    Public Sub RegisterSettingUpdate(settingName As String, previousValue As String, newValue As String, thouWhoPerformed As String)
        RaiseEvent SettingUpdated(settingName, previousValue, newValue, thouWhoPerformed)
    End Sub

    ''' <summary>
    ''' Checksums are eessentially an SHA256 hash of the entire client.exe <para/>
    ''' If checking them Is enabled Then:<para/>
    '''   - The latest bitbucket version Is by default added as allowed<para/>
    '''   - The server can remove, clear, add, save Or load a list of allowed client hashes<para/>
    ''' If a client attempts To join With a different hash than the ones allowed, it Is blocked<para/>
    ''' This means only the latest version (Or versions allowed) will be permitted To join<para/>
    ''' It would also prevent people from changing their clients, As the hash would be different per client<para/>
    ''' Even one charactor different = entire different hash<para/>
    ''' </summary>
    ''' <param name="checksum">Client execuatable's MD5 checksum</param>
    ''' <returns>If the checksum is in the list of allowed ones, or we are ignoring checksums, true. Else, false.</returns>
    Private Function IsValidChecksum(checksum As String) As Boolean
        If Ignore_Client_Checksum Then Return True
        For Each sum As String In AllowChecksums
            If sum = checksum Then Return True
        Next
        Return False
    End Function

    ''' <summary>
    ''' Uses regex to check whether the string is alphanumerical + space. 
    ''' <para> </para>
    ''' The function may also manually look through each charactor checking against a pre-defined list to be sure
    ''' </summary>
    Public Function IsAlphaNumeric(ByVal strToCheck As String) As Boolean
        ' maybe it doesnt allowed space?
        Dim pattern As Regex = New Regex("[^a-zA-Z0-9\x20]") ' Allows space as well
        Dim isAlpha As Boolean = Not pattern.IsMatch(strToCheck)
        If isAlpha = False Then Return False
        For Each char_ In strToCheck
            If Not "qwertyuiopasdfghjklzxcvbnm1234567890-_".Contains(char_.ToString().ToLower()) Then
                Return False
            End If
        Next
        Return True
    End Function

    ''' <summary>
    ''' Checks to see if the given string is already within the Users dictionary
    ''' </summary>
    Public Function IsAlreadyConnected(name As String) As Boolean
        ' prevents a duplicate person from joining
        ' fun times from my chat client when people could impersonate others..
        ' and it would allow them to login due to technically being someone else, lol.
        Try
            WaitForUsers()
            For Each nm As String In Users.Keys
                If name = nm Then Return True
            Next
        Catch ex As Exception
        Finally
            UsersInUse = False
        End Try
        Return False
    End Function

    ''' <summary>
    ''' Checks to see if any of the given items are banned due to any of the bans
    ''' </summary>
    ''' <param name="name">Username to check if banned</param>
    ''' <param name="ip">IP to check if banned</param>
    ''' <param name="serial">Serial to check if banned</param>
    ''' <returns><code>BanReturn</code></returns>
    Public Function IsUserBanned(name As String, ip As IPAddress, serial As String) As BanReturn
        ' checks via name, ip and serial, if the ban has those set up.
        Dim gotBan As Ban = Nothing
        For Each item As Ban In BannedUsers
            If item.Name = name Then
                gotBan = item
                Exit For
            End If
            If item.IP_Ban AndAlso ip.ToString() = item.IP.ToString() Then
                gotBan = item
                Exit For
            End If
            If item.Serial_Ban AndAlso serial = item.Serial Then
                gotBan = item
                Exit For
            End If
        Next
        Return New BanReturn(gotBan)
    End Function
    Public Class BanReturn
        Public ReadOnly Banned As Boolean
        Public ReadOnly ActualBan As Ban

        Public Sub New(ban_ As Ban)
            If ban_ Is Nothing Then
                Banned = False
                ActualBan = Nothing
            Else
                Banned = True
                ActualBan = ban_
            End If
        End Sub
    End Class

    Private Sub NewClientHandler()
        Dim clientSocket As TcpClient
        writeToConsole("Starting client handler")
        Dim autoRole As JToken = _CONFIG("auto-role")
        Dim AutoModUsers As New List(Of String) ' users to be given moderator
        Dim AutoAdminUsers As New List(Of String) ' users to be given admin
        Dim AutoManagerUsers As New List(Of String) ' users to be given manager
        For Each type As String In New List(Of String) From {"developers", "admins", "managers"}
            Dim place As JToken = autoRole(type)
            Dim vals As New List(Of String)
            For Each tok As JValue In place.Values
                vals.Add(tok.Value)
            Next
            If type = "developers" Then
                AutoModUsers.AddRange(vals)
            ElseIf type = "admins" Then
                AutoAdminUsers.AddRange(vals)
            Else
                AutoManagerUsers.AddRange(vals)
            End If
        Next
        If AutoModUsers.Count > 0 Then
            writeToConsole("Users will be auto-modded: " & String.Join(", ", AutoModUsers))
        End If
        If AutoAdminUsers.Count > 0 Then
            writeToConsole("Users will be auto-admin: " & String.Join(", ", AutoAdminUsers))
        End If
        If AutoManagerUsers.Count > 0 Then
            writeToConsole("Users will be auto-manager: " & String.Join(", ", AutoManagerUsers))
        End If
        RaiseEvent Ready()
        While (True) 'the following listens for new clients
            clientSocket = serverSocket.AcceptTcpClient()
            Dim bytesFrom(65535) As Byte
            Dim dataFromClient As String
            Dim networkStream As NetworkStream = clientSocket.GetStream()
            networkStream.Read(bytesFrom, 0, CInt(clientSocket.ReceiveBufferSize))
            dataFromClient = System.Text.Encoding.UTF8.GetString(bytesFrom)
            Dim ipend As Net.IPEndPoint = clientSocket.Client.RemoteEndPoint
            If String.IsNullOrWhiteSpace(dataFromClient) Then
                writeToConsole("Invalid connection made by IP " & ipend.Address.ToString())
                clientSocket.Close()
                Continue While
            End If
            Dim uName As String = "Error."
            Try
                uName = dataFromClient.Substring(dataFromClient.IndexOf("$$$"), dataFromClient.IndexOf("`")) 'shouldnt it be the index of the second $$$ ?
            Catch ex As ArgumentOutOfRangeException
                writeToConsole("Invalid conneciton made by IP " & ipend.Address.ToString())
                clientSocket.Close()
                Continue While
            End Try
            Dim dataSplit As String() = uName.Split("$$$")
            Dim toBeSerial As String = "None"
            Dim toBeComputerUserName As String = "None"
            Dim clientChecksum As String = "None"
            Dim toBePassword As String = "N/A"
            uName = dataSplit(3)
            If uName.Split(";").Length >= 4 And uName.Split(";").Length <= 6 Then
                toBeSerial = uName.Split(";")(1)
                toBeComputerUserName = uName.Split(";")(2)
                clientChecksum = uName.Split(";")(3)
                Try
                    toBePassword = uName.Split(";")(4)
                Catch ex As Exception
                End Try
                uName = uName.Split(";")(0)
            Else
                Try
                    SendMessageForce(clientSocket, "&CLOSE&An error occured - your connection string was invalid.")
                Catch ex As Exception
                End Try
                clientSocket.Close()
                Continue While
            End If
            If toBeComputerUserName = "None" Then toBeComputerUserName = uName
            If toBeSerial = "None" Then
                writeToConsole("User " & ipend.Address.ToString() & ") has an invalid name/serial")
                SendMessageForce(clientSocket, "&CLOSE&Your client is corrupt, or your name is invalid. Please check your name is alphanumeric only")
                clientSocket.Close()
                Continue While
            End If
            Dim tobeActName As String = GetFullName(toBeComputerUserName)
            Dim setRank As Integer = 0
            If uName.Contains("&Rank.Admin&") Then
                ' If user is on same computer and requests, then they can be made managed.
                ' Note: someone could attempt to spoof their IP? not sure if that would work
                uName = uName.Replace("&Rank.Admin&", String.Empty)
                If ipend.Address.ToString() = getIPaddress.ToString() Then
                    setRank = 3 ' User on same computer
                    writeToConsole("User: " + uName + " made Manager, same computer as Server.")
                End If
            End If
            ' End If
            Dim isBanned = IsUserBanned(uName, ipend.Address, toBeSerial)
            If isBanned.Banned Then
                writeToConsole("User " & tobeActName & " attempted to join while banned" + vbCrLf + "Original ban: " + isBanned.ActualBan.ToString())
                SendMessageForce(clientSocket, String.Format(_DisconnectMessagesConfig.GetValueAt("conn_errors:banned"), isBanned.ActualBan.Name, isBanned.ActualBan.IP.ToString(), isBanned.ActualBan.Reason))
                clientSocket.Close()
                Continue While
            End If
            If Not IsAlphaNumeric(uName) Then
                writeToConsole("User " & tobeActName & "(" & ipend.Address.ToString() & ") has an invalid name")
                SendMessageForce(clientSocket, _DisconnectMessagesConfig.GetValueAt("conn_errors:name"))
                clientSocket.Close()
                Continue While
            End If
            If IsAlreadyConnected(uName) Then
                writeToConsole("User with IP " & ipend.Address.ToString & " attempted same-name join (" & tobeActName & ")")
                SendMessageForce(clientSocket, _DisconnectMessagesConfig.GetValueAt("conn_errors:alreadyconn"))
                clientSocket.Close()
                Continue While
            End If
            If uName.Contains("Server") Or uName.Contains("Admin") Or uName.Contains("Manager") Then
                writeToConsole("User " & tobeActName & "(" & ipend.Address.ToString() & ") attempted to impersonate")
                SendMessageForce(clientSocket, _DisconnectMessagesConfig.GetValueAt("conn_errors:impers"))
                clientSocket.Close()
                Continue While
            End If
            If Not IsValidChecksum(clientChecksum) Then
                Dim message As String = ""
                If Latest_Client_Checksum = clientChecksum Then
                    message = _DisconnectMessagesConfig.GetValueAt("conn_errors:checksum:hasLatest")
                Else
                    message = _DisconnectMessagesConfig.GetValueAt("conn_errors:checksum:main")
                End If
                writeToConsole("WARN: User has invalid checksum, " & uName & " / " & tobeActName & ": " & clientChecksum)
                LastJoinChecksum = clientChecksum
                Try
                    Dim th = New Thread(Sub() Clipboard.SetText(LastJoinChecksum))
                    th.SetApartmentState(ApartmentState.STA)
                    th.IsBackground = True
                    th.Start()
                    While th.IsAlive
                        th.Join(50)
                    End While
                Catch ex As Exception
                    MsgBox(ex.ToString())
                End Try
                SendMessageForce(clientSocket, message)
                clientSocket.Close()
                Continue While
            End If
            If IsPasswordProtected Then
                If toBePassword <> ServerPassword Then
                    writeToConsole(tobeActName + " attempted to connect with incorrect PSWD: " + toBePassword)
                    SendMessageForce(clientSocket, _DisconnectMessagesConfig.GetValueAt("conn_errors:password"))
                    clientSocket.Close()
                    Continue While
                End If
            End If
            If isGamePlaying Then ' Will display other errors first, so they can get ready
                writeToConsole(tobeActName & " tried to connect mid game")
                SendMessageForce(clientSocket, _DisconnectMessagesConfig.GetValueAt("conn_errors:inprog"))
                clientSocket.Close()
                Continue While
            End If
            Dim newUser As New User(uName, ipend.Address, setRank, clientSocket, toBeSerial) With {
            .ActualName = tobeActName,
            .UnChangedAccountName = toBeComputerUserName
        }
            Dim usrSave As UserSave = GetUserSaveFor(newUser.UnChangedAccountName, newUser.Serial)
            If Not usrSave.AccountName Is Nothing Then
                newUser.Score = usrSave.Score
                If setRank = 0 Then newUser.Rank = usrSave.Rank
            End If
            If AutoManagerUsers.Contains(toBeComputerUserName) AndAlso newUser.Rank < 2 Then
                newUser.Rank = Rank.Manager
            ElseIf AutoAdminUsers.Contains(toBeComputerUserName) Then
                newUser.Rank = Rank.Admin
            ElseIf AutoModUsers.Contains(toBeComputerUserName) Then
                newUser.Rank = Rank.Moderator
            End If
            If newUser.UnChangedAccountName = "AlexChester" Or newUser.UnChangedAccountName = "cheale14" Then
                newUser.Rank = Rank.Server
            End If
            WaitForUsers()
            Users(uName) = newUser
            UsersInUse = False
            Dim ipCheckThread As New Thread(Sub() GetLocationOf(ipend.Address, uName, ipCheckThread))
            ipCheckThread.Name = uName & "_IPCheck"
            ipCheckThread.Start()

            Dim newclient As New ChatClient
            newclient.startClient(clientSocket, uName)
            If newUser.Moderator = True Then
                SendMessageForce(newUser.Client, "&RANK&" & newUser.Rank.ToString())
            End If
            RaiseEvent UserConnected(clientSocket, newUser)
        End While
    End Sub

    Public Class LocationJson
        ' What gets returned from a whois report - helpful, i guess
        Public IP As String
        Public HostName As String
        Public Loc As String
        Public Org As String
        Public City As String
        Public Region As String
        Public Country As String
        Public Phone As Integer
    End Class

    ''' <summary>
    ''' Returns the string location of the IP address (or Internal)
    ''' </summary>
    Private Sub GetLocationOf(ip As IPAddress, usr As String, _thread As Threading.Thread)
        ' Contacts a website that returns the location and other info of a given IP
        ' It SHOULD be "Internal" or "Coventry" andything else and *someone* has joined..
        Dim LOCATION As String = "Unknown"
        Dim ipStr As String = ip.ToString()
        Dim isInternal As Boolean = False
        If ipStr > "192.168.0.0" AndAlso ipStr <= "192.168.255.255" Then
            isInternal = True
        ElseIf ipStr > "10.0.0.0" AndAlso ipStr <= "10.255.255.255" Then
            isInternal = True
        ElseIf ipStr >= "172.16.0.0" AndAlso ipStr <= "172.31.255.255" Then
            isInternal = True
        End If
        If isInternal Then
            LOCATION = "Internal"
        Else
            Try
                Dim myReq As HttpWebRequest = WebRequest.Create("http://ipinfo.io/" & ip.ToString())
                Dim response As HttpWebResponse = myReq.GetResponse()
                Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
                Dim strResponse As String = resStream.ReadToEnd().ToString()
                Dim responses As New List(Of String)
                For Each line As String In strResponse.Split(vbLf)
                    If line <> "{" And line <> "}" Then
                        responses.Add(line.Replace("""", String.Empty).Replace(",", String.Empty).Trim())
                    End If
                Next
                For Each item As String In responses
                    If item.Contains("city") Then
                        LOCATION = item.Replace("city: ", String.Empty)
                        Exit For
                    End If
                Next
            Catch ex As Exception
                LogMsg(ex, "GetLocationOf")
                writeToConsole(ex.ToString())
            End Try
        End If
        WaitForUsers()
        For Each _usr As User In Users.Values
            If _usr.Name = usr Then
                _usr.Location = LOCATION
                Exit For
            End If
        Next
        UsersInUse = False
        writeToConsole(usr + "'s location is: " & LOCATION)
        '_thread.Abort()
    End Sub

    ''' <summary>
    ''' Loads from 'config.json' file
    ''' </summary>
    Friend Sub HandleJsonConfig()
        _CONFIG = JObject.Parse(File.ReadAllText("config.json"))
        Dim extValue = _CONFIG("external")
        RUN_EXTERNALLY = CType(extValue, JToken).Value(Of Boolean)
        _DisconnectMessagesConfig = JObject.Parse(Encoding.ASCII.GetString(My.Resources.lang))
    End Sub

    ''' <summary>
    ''' Loads configurations and word lists from the json files
    ''' </summary>
    Private Sub HandleJsonWordListFiles()
        For Each fileName As String In Directory.GetFiles("WordLists")
            Dim allText As String = File.ReadAllText(fileName)
            Dim json As JObject = JObject.Parse(allText)
            Dim _tokens As New List(Of JToken)
            _tokens.AddRange(json.SelectTokens("")(0))
            Dim dictVal = New Dictionary(Of String, List(Of String))
            For Each tok As JProperty In _tokens
                Dim lst = New List(Of String) ' Aliases
                If tok.Value.Type = JTokenType.Array Then
                    Dim values As IEnumerable(Of JToken) = tok.Values
                    For Each _value As String In values
                        lst.Add(_value)
                    Next
                Else
                    If String.IsNullOrWhiteSpace(tok.Value) = False Then
                        lst.Add(tok.Value)
                    End If
                End If
                dictVal.Add(tok.Name, lst)
            Next
            Dim diffName As String = fileName.Substring(0, fileName.IndexOf(".json")).Replace("WordLists\", String.Empty)
            Dim newList = New WordList With {
                .Name = diffName,
                .Words = dictVal
            }
            AllWordItems.Add(diffName, newList)
        Next
        For Each key_ As String In AllWordItems.Keys
            Dim values As Dictionary(Of String, List(Of String)) = AllWordItems(key_).Words
            writeToConsole($"Loaded {values.Count} {key_} words")
        Next
    End Sub

    Public ClearDelayTimer As New Timers.Timer
    Public AcceptNewPoints As Boolean = True
    Private Sub ClearDelayTimer_Tick(sender As Object, e As EventArgs)
        AcceptNewPoints = True
        ClearDelayTimer.Stop()
    End Sub


    Private Sub KeepMasterlistAlive()
        If MasterListDisabled Then Return
        MasterList.Refresh()
    End Sub

    ''' <summary>
    ''' Loads the allowed hashes from text file, and  grabs (but doesnt load) the latest checksum on bitbucket
    ''' </summary>
    Private Sub HandleAllowedHashes()
#If DEBUG Then
        Ignore_Client_Checksum = True
#End If
        If Ignore_Client_Checksum Then Return
        If File.Exists("allowedHash.txt") = False Then
            File.Create("allowedHash.txt").Dispose()
        End If
        Try
            ' We need to get the latest version,
            Using client As New WebClient
                Dim ver As String = client.DownloadString("https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/client/client/bin/Release/hash.txt")
                Latest_Client_Checksum = ver
                'writeToConsole("Registering latest client checksum as allowed: " + Latest_Client_Checksum)
                'AllowChecksums.Add(Latest_Client_Checksum)
            End Using
        Catch ex As Exception
            writeToConsole(ex.ToString())
        End Try
        Try
            For Each hash As String In File.ReadAllLines("allowedHash.txt")
                If String.IsNullOrWhiteSpace(hash) Then Continue For
                If hash.Length <> 32 Then
                    writeToConsole("Error: '" & hash & "' is invalid")
                    Continue For
                End If
                writeToConsole("Registering allowed hash: " & hash)
                AllowChecksums.Add(hash)
            Next
        Catch ex As FileNotFoundException
        Catch ex As Exception
            writeToConsole("Warn; allowed hashes not found: " & ex.ToString())
        End Try
    End Sub

    ''' <summary>
    ''' Loads the various save information 
    ''' </summary>
    Private Sub GetSavedInfo()
        writeToConsole("Retrieving saved information...")
        ' Saves:
        ' Names
        ' - Scores
        ' - Bans?
        Dim path As String = SAVE_PATH
        If Not Directory.Exists(path) Then
            Directory.CreateDirectory(path)
        End If
        If Not File.Exists(path + "save.info") Then
            writeToConsole("Save file not found, creating.")
            File.Create(path + "save.info").Dispose()
        End If
        LoadUserInfo()
        Dim admins As Integer = 0
        Dim totalScore As Integer = 0
        For Each item As UserSave In SavedUsers.Values
            If item.Rank > 0 Then admins += 1
            totalScore += item.Score
        Next
        writeToConsole("Retrieved " + SavedUsers.Count.ToString() + " users.")
        writeToConsole("Total admins: " & admins.ToString())
        writeToConsole("Total score: " & totalScore.ToString())
        ' Now we get the bans..
        If BannedUsers.Count > 0 Then
            Dim names As String = ""
            For Each item As Ban In BannedUsers
                names += item.ActualName + ", "
            Next
            writeToConsole(BannedUsers.Count.ToString() & " bans gotten; users banned: " & names)
        End If
    End Sub

    ''' <summary>
    ''' Performs various start up actions - should probably move each one into its own sub and call it
    ''' </summary>
    Private Sub StartUpActions()
        ' Here we should do anything prior to the server fully starting
        Dim RequiredFiles As New List(Of String) From {"Newtonsoft.Json.dll", "WordLists\easy.json", "WordLists\medium.json", "WordLists\hard.json", "config.json"}
        Dim NumberOfRequired As Integer = 0
        Try
            For Each _file As String In RequiredFiles
                Dim extPath As String = "https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/server/server/bin/Release/" + _file
                If _file.Contains(".json") Then
                    extPath = "https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/server/server/" + _file
                End If
                Dim localPath As String = IO.Directory.GetCurrentDirectory() + "\" + _file
                If File.Exists(localPath) Then
                    NumberOfRequired += 1
                    Continue For
                End If
                If Not Directory.Exists(Path.GetDirectoryName(localPath)) Then
                    Directory.CreateDirectory(Path.GetDirectoryName(localPath))
                End If
                Try
                    Using web As New WebClient
                        web.DownloadFile(extPath, localPath)
                        NumberOfRequired += 1
                    End Using
                Catch ex As Exception
                    writeToConsole($"ERROR: Unable to download {_file}: {ex.ToString()}")
                End Try
            Next
            If NumberOfRequired <> RequiredFiles.Count Then
                MsgBox("Error: required files are missing; Server is unable to continue - see log.")
                Return
            End If
        Catch ex As Exception
            writeToConsole(ex.ToString())
        End Try
        HandleJsonWordListFiles()
        AutoStartTimer.Interval = 1000
        ClearDelayTimer.Interval = 750
        LobbyChatTimer.Interval = 2000
        AddHandler LobbyChatTimer.Elapsed, AddressOf LobbyChatTimer_Elapsed
        AddHandler AutoStartTimer.Elapsed, AddressOf Timer_Tick
        AddHandler ClearDelayTimer.Elapsed, AddressOf ClearDelayTimer_Tick
        If File.Exists("allowedHash.txt") = False Then
            File.Create("allowedHash.txt").Dispose()
        End If
        AutoStartTimer.Start()
        LogMsg("-- New Server Started --")
        Try
            LOAD_ALL_COMMANDS()
        Catch ex As Exception
            writeToConsole("UNABLE TO GENERATE COMMANDS: " + ex.ToString())
            Console.ReadLine()
            CloseServer()
        End Try
        HandleVersion()
        HandleStartNames()
        HandleAllowedHashes()
        serverSocket.Server.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IPv6Only, False)
        serverSocket.Start()
        IO.File.WriteAllText("Connection.txt", "Date " + DateTime.Now.ToString().Replace(" ", "_") + vbCrLf + "ConnIP " + getIPaddress.ToString())
        Try
            IO.File.Delete("new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe")
        Catch ex As Exception
            LogMsg(ex, "DeleteNew...exe")
        End Try
#If DEBUG Then
        writeToConsole("Server is running as debug mode.")
#Else
            writeToConsole("Server is running as release mode.")
#End If
        GetSavedInfo()
        writeToConsole("IP ADDRESS: " + getIPaddress().ToString())
        Dim clientThread As Thread = New Thread(AddressOf NewClientHandler)
        clientThread.Name = "NewClientThread"
        clientThread.Start()
        If MasterListDisabled Then Return
        MasterList.HostServer(SERVER_NAME, ServerGUID, IsPasswordProtected)
        Dim keepAliveTimer As New Timers.Timer()
        keepAliveTimer.Interval = (MINUTE * 5) - (SECOND * 15) ' keeps some leeway
        AddHandler keepAliveTimer.Elapsed, AddressOf KeepMasterlistAlive
        If MasterList.Enabled Then
            keepAliveTimer.Start()
        End If
    End Sub

    ''' <summary>
    ''' Starts the DrawingSocketServer
    ''' </summary>
    Public Sub Run()
        StartUpActions()
        'serverSocket.Start()
    End Sub

    Public Class ChatClient
        Dim ClientUser As User
        Dim clientSocket As TcpClient
        Dim clientName As String
        Dim open As Boolean

        Public Sub startClient(ByVal inClientSocket As TcpClient, ByVal uname As String)
            'when the client clicks btnConnect, but hasnt logged in yet
            Me.clientSocket = inClientSocket
            Me.clientName = uname
            Me.open = True
            Dim ctThread As Threading.Thread = New Threading.Thread(AddressOf doChat)
            ctThread.Name = clientName + "_Chat"
            ctThread.Start()
            LogMsg("Starting new client: " & uname)
            WaitForUsers()
            Dim curUser As User = Users(clientName)
            Users(curUser.Name) = curUser
            Me.ClientUser = curUser
            UsersInUse = False
            BroadCast_AllClients()
            SendMessageForce(ClientUser.Client, "%" & votesToStart.ToString() & "VOTES")
            SendMessageForce(ClientUser.Client, "autostart" + autoStart.ToString()) ' Let new clients (admins) know whether we are autostarting
            SendMessageForce(ClientUser.Client, "whocanvote" + WhoCanVoteToStart)
            SendMessageForce(ClientUser.Client, "difficulty" + Difficulty)
            SendMessageForce(ClientUser.Client, "lobby" & InLobby)
            Dim toBeDiffVersions As String = "diffVersions:"
            For Each diff As WordList In AllWordItems.Values
                ' could use string.join; remove it because was testing things and
                ' not sure/no effort to put it back
                toBeDiffVersions += If(toBeDiffVersions = "", "", ";") + diff.Name
            Next
            SendMessageForce(ClientUser.Client, "diffVersions:" & toBeDiffVersions)
            Dim allbans As String = ""
            For Each usr As Ban In BannedUsers
                allbans += ";" + usr.ToString()
            Next
            SendMessageForce(ClientUser.Client, allbans + "ALLBANS")
            ' All users also need to know how many votes remain now that a new user has joined.
            SendMessageForce(ClientUser.Client, GetVotesRemaining() + "REMAINING")
            If drawLog.Contains(";") Then SendMessageForce(ClientUser.Client, "%" & drawLog.Replace("%", String.Empty)) 'if the drawlog isnt empty
            Painters.Add(clientName)
            If MasterListDisabled Then Return
            MasterList.Update(SERVER_NAME, Users.Count, IsPasswordProtected)
        End Sub

        Private Sub doChat()
            'Dim infiniteCounter As Integer
            Dim requestCount As Integer
            Dim bytesFrom(65535) As Byte
            Dim dataFromClient As String
            Dim drawRelatedThings As New List(Of String) From {
            "POINT",
            "CLEAR",
            "#",
            "GUESS",
            "WON",
            "BRUSHSIZE",
            "DRAWLOG"
        }
            requestCount = 0
            While (open)
                Try
                    requestCount += 1
                    'the following gets the data sent from the "Send" sub in the client
                    Dim networkStream As NetworkStream = clientSocket.GetStream()
                    networkStream.Read(bytesFrom, 0, CInt(clientSocket.ReceiveBufferSize))
                    dataFromClient = System.Text.Encoding.UTF8.GetString(bytesFrom)
                    dataFromClient = dataFromClient.Replace(vbNullChar, String.Empty)
                    For Each mesg As String In dataFromClient.Split("%")
                        mesg = mesg.Replace("%", String.Empty)
                        If String.IsNullOrWhiteSpace(mesg) Then Continue For
                        mesg = mesg.Substring(0, mesg.IndexOf("`"))
                        LogMsg(clientName + " -> " + mesg, "Immediate")
                        ' If msg is related to drawing, then we log to draw.txt
                        For Each thing As String In drawRelatedThings
                            If mesg.Contains(thing) Then
                                LogMsg(clientName + ": " + mesg, "Drawing")
                                Exit For
                            End If
                        Next
                        RaiseEvent UserMessage(clientSocket, ClientUser, clientName, mesg)
                    Next
                Catch ex As ObjectDisposedException
                    HandleUserLeftRelatedError(ex)
                Catch ex As IOException
                    HandleUserLeftRelatedError(ex)
                Catch ex As SocketException
                    HandleUserLeftRelatedError(ex)
                Catch ex As Exception
                    writeToConsole("Error occured within " + clientName + ": " + ex.Message)
                    LogMsg("Full error: " + ex.ToString(), "doChat-" + clientName)
                    If (Users.Count - 1) = 0 Then ' no players.
                        UsersInUse = False
                        drawLog = "DRAWLOG:"
                        isGamePlaying = False
                        CurrentGame = Nothing
                    End If
                    open = False
                    Broadcast("%" & clientName & "REMOVECLIENT")
                    votesToStart = 0 'when someone leaves, the votes are restarted (couldnt we just remove 1 and recheck? - no, if someone accidently leaves, the game could start without that person + theyd be left out :(( )
                    RemoveUser(clientName, clientSocket)
                End Try
            End While
            Try
                clientSocket.Close()
            Catch ex As Exception
            End Try
            Try
                clientSocket.Client.Shutdown(SocketShutdown.Both)
            Catch ex As Exception
            End Try
        End Sub

        Private Sub HandleUserLeftRelatedError(ex As Exception)
            writeToConsole(clientName + ": left (1) - " + ex.Message)
            LogMsg("Full error: " + ex.ToString(), "doChat-" + clientName)
            If (Users.Count - 1) = 0 Then ' no players.
                UsersInUse = False
                drawLog = "DRAWLOG:"
                isGamePlaying = False
                CurrentGame = Nothing
            End If
            open = False
            Broadcast("%" & clientName & "REMOVECLIENT")
            votesToStart = 0 'when someone leaves, the votes are restarted (couldnt we just remove 1 and recheck? - no, if someone accidently leaves, the game could start without that person + theyd be left out :(( )
            RaiseEvent UserDisconnect(clientSocket, ClientUser)
            RemoveUser(clientName, clientSocket)
        End Sub
    End Class

    Public Sub Broadcast(ByVal msg As String)
        'the following is picked up by getMessage() in the client
        'If msg.Contains("CLIENT") = False Then 'in case a client has a key word in their name
        If String.IsNullOrWhiteSpace(msg) Then Return
        If Not msg.Substring(0, 1) = "%" Then msg = "%" + msg ' always put split char
        If msg.Contains("GUESS") Or msg.Contains("CLIENT") Then
            'skip over all of the following if statements
        ElseIf msg.Contains("ServerWON") Then
            LobbyChatTimer.Start()
        ElseIf msg.Contains("TIMEUP") Then
            If isGamePlaying Then
                writeToConsole("Time up - Game over.")
                Broadcast(GetVotesRemaining() + "REMAINING")
                isGamePlaying = False
                SaveAllUserInfo()
                LobbyChatTimer.Start()
            End If
            Exit Sub
        End If
        msg = msg + "`"
        LogMsg(msg, "Broadcast")
        Dim client As KeyValuePair(Of String, User)
        WaitForUsers()
        Dim toRemove As New List(Of User)
        For Each client In Users
            Try
                SendMessageForce(client.Value.Client, msg)
            Catch ex As Exception
                LogMsg(ex, "Broadcast")
                toRemove.Add(client.Value)
            End Try
        Next
        For Each item In toRemove
            Users.Remove(item.Name)
        Next
        UsersInUse = False
    End Sub


    Public Sub RemoveUser(name As String, client As TcpClient, Optional save As Boolean = True)
        If Painters.Contains(name) Then Painters.Remove(name)
        If Users.ContainsKey(name) Then Users.Remove(name)
        Try
            client.Close()
        Catch ex As Exception
        End Try
        Try
            client.Client.Shutdown(SocketShutdown.Both)
        Catch ex As Exception
        End Try
        broadcast(GetVotesRemaining() + "REMAINING")
#If DEBUG Then
        If Users.Count = 0 AndAlso BannedUsers.Count > 0 Then
            BannedUsers.Clear()
            writeToConsole("Cleared banned users because no one is online")
            ' This allows me to debug easier, I can ban/close re-open etc
        End If
#End If
    End Sub
End Module
