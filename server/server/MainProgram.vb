﻿Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Reflection
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Threading
Imports System.Windows.Forms
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports MasterlistDLL
Imports System.Security.Cryptography

#Disable Warning IDE1006 ' Naming Styles
Public Module MainProgram
    ' ---- UPDATED BY ALEX: -----
    ' See the Drawing Trello for a full view on changes/things.
    '% indicates the start of a new msg
    'the coords are sent in the form x,y-x,y forming two points (point-point) to draw a line
    '$$$ is.. a separator too? - yes, it is used to denote the start and end of the username (should prevent a coord from getting into a name somehow)

    Public MasterList As MasterlistServer

    Friend Users As New Dictionary(Of String, User)
    Friend rand As Random = New Random()
    Friend drawLog As String = "DRAWLOG:"
    Friend votesToStart As Integer = 0
    Friend Painters As New List(Of String)
    Friend isGamePlaying As Boolean = False
    Friend autoStart As Boolean = False
    Friend painter As String
    Public WhoCanVoteToStart As String = "Everyone"
    Public InLobby As Boolean = True

    Private MainRegistry As String = "HKEY_CURRENT_USER\Liliana_Drawing\Server"
    ''' <summary>
    ''' Either the stored guid in registry, or generates & stores a new one
    ''' </summary>
    Public ReadOnly Property ServerGUID As Guid
        Get
            Dim _stored As String = My.Computer.Registry.CurrentUser.GetValue(MainRegistry + "\GUID", "")
            Dim _guid As Guid = Guid.Empty
            If String.IsNullOrWhiteSpace(_stored) Then
                _stored = Guid.NewGuid().ToString()
                My.Computer.Registry.CurrentUser.SetValue(MainRegistry + "\GUID", _stored)
                writeToConsole("Generated new Server Identifier: " + _stored)
            End If
            _guid = Guid.Parse(_stored)
            Return _guid
        End Get
    End Property

    Public Const SERVER_NAME As String = "Serverlol"
    Public RUN_EXTERNALLY As Boolean ' this should be true if the server is running in a port-forward state.
    ' it means that the masterlist will show the server's ip as its external one, rather than its internal
    ' THIS SHOULD BE SET TO FALSE WHEN COMMITING.
    ' this is now hanlded by config.json - do not change RUN EXTERNALLY value

    Public Enum Rank
        Player = 0
        Moderator = 1
        Admin = 2
        Manager = 3
        Server = 4 ' SHOULD NOT BE ASSIGNED
    End Enum
    Public Ignore_Client_Checksum = False ' do we ignore checksums all together?
    Public Latest_Client_Checksum = "" ' latest checksum of Bitbucket client version
    Public AllowChecksums As New List(Of String) ' versions explicitly said as allowed
    Friend LastJoinChecksum As String = "" ' set when a client fails to join due to outdated client

    Public _CONFIG As JObject ' config.json file
    Public _DisconnectMessagesConfig As JObject ' shrug

    'Private Property BannedUsers As List(Of Ban) => DrawingSocketServer.BannedUsers


    Public Difficulty As String = "Easy"
    Public CurrentGame As DrawGame

    Public UsersInUse As Boolean = False
    Public Sub WaitForUsers()
        While UsersInUse
            Threading.Thread.Sleep(1)
        End While
        UsersInUse = True
    End Sub

    Dim LatestVersion As Version = New Version("0.0.0.0")
    Dim ThisThingVersion As Version = New Version("0.0.0.0")
    Public HasRecievedPoint As Boolean = False ' Records whether we have recieved points since last CLEAR message.
    ' possibly should remove this - could be thing causing inability to change colour between CLEAR -> next draw


    Function getIPaddress() As Net.IPAddress
        Dim ipaddress As Net.IPAddress
        Dim strhostname As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = Net.Dns.GetHostEntry(strhostname)

        For Each ipheal As Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                ipaddress = ipheal
                Exit For
            End If
        Next
        Return ipaddress
    End Function

    Public Function EncryptSHA256Managed(ByVal ClearString As String) As String
        ' Gets the SHA256 cryptograhpic hash of the inputted plaintext
        ' This is used in:
        ' - Getting the client's hash (to ensure it hasnt been modified)
        ' - Serials - unique per computer (or possibly per account?)
        ' - Other things i guess
        Dim uEncode As New UnicodeEncoding()
        Dim bytClearString() As Byte = uEncode.GetBytes(ClearString)
        Dim sha As New System.Security.Cryptography.SHA256Managed()
        Dim hash() As Byte = sha.ComputeHash(bytClearString)
        Return Convert.ToBase64String(hash)
    End Function

    Public Sub SendMessageForce(clientSocket As TcpClient, msg As String)
        ' sends message to client
        If String.IsNullOrWhiteSpace(msg) Then Return
        If Not msg.Substring(0, 1) = "%" Then msg = "%" + msg
        Dim broadcastStream As NetworkStream = clientSocket.GetStream()
        broadcastStream.Flush()
        Dim broadcastBytes As [Byte]()
        broadcastBytes = Encoding.UTF8.GetBytes(msg + "`")
        broadcastStream.Write(broadcastBytes, 0, broadcastBytes.Length)
        broadcastStream.Flush()
    End Sub

    Private FirstAdminSet As Boolean = False

#Region "SECTION: User Scores, Saving and Loading"
    Public SavedUsers As New Dictionary(Of String, UserSave) ' Username - Info
    Private SavedUsersInUse As Boolean = False
    Public Const SAVE_PATH As String = "Saves\"
    Private Sub WaitForSavedUsers()
        While SavedUsersInUse
            Threading.Thread.Sleep(1)
        End While
        SavedUsersInUse = True
    End Sub
    Public Sub LoadUserInfo()
        Dim savePath As String = SAVE_PATH + "save.info"
        Dim contents As String = ""
        Try
            contents = File.ReadAllText(savePath)
        Catch ex As Exception
            writeToConsole(ex.ToString())
        End Try
        If String.IsNullOrWhiteSpace(contents) Then Return
        Dim save As SaveData = JsonConvert.DeserializeObject(Of SaveData)(contents)
        For Each item As UserSave In save.Users
            Try
                SavedUsers.Add(item.AccountName, item)
            Catch ex As Exception
            End Try
        Next
        BannedUsers = save.Bans
    End Sub
    Public Sub SaveAllUserInfo()
        Dim savePath As String = SAVE_PATH + "save.info"
        writeToConsole("Saving scores of all users")
        WaitForUsers()
        For Each usr As User In Users.Values
            Dim saveNew As New UserSave With {
            .AccountName = usr.UnChangedAccountName,
            .Rank = usr.Rank,
            .Score = usr.Score,
            .Serial = usr.Serial
        }
            If SavedUsers.Keys.Contains(usr.UnChangedAccountName) Then
                SavedUsers(usr.UnChangedAccountName) = saveNew
            Else
                SavedUsers.Add(usr.UnChangedAccountName, saveNew)
            End If
        Next
        UsersInUse = False
        Dim newSave As New SaveData()
        WaitForSavedUsers()
        newSave.Users = SavedUsers.Values.ToList()
        SavedUsersInUse = False
        newSave.Bans = BannedUsers
        Dim str As String = JsonConvert.SerializeObject(newSave)
        Try
            File.WriteAllText(savePath, str)
        Catch ex As Exception
            writeToConsole(ex.ToString())
        End Try
    End Sub

    Public Function GetUserSaveFor(name As String, serial As String) As UserSave
        Try
            WaitForSavedUsers()
            For Each usr As UserSave In SavedUsers.Values
                If usr.AccountName = name AndAlso usr.Serial = serial Then
                    Return usr
                End If
            Next
            Return New UserSave
        Catch ex As Exception
        Finally
            SavedUsersInUse = False
        End Try
    End Function

#End Region

#Region "SECTION:Client usernames to real names"
    Private NameDict As New Dictionary(Of String, String)
    Private nameDictInUse As Boolean = False
    Public Sub HandleStartNames()
        Dim fullNames As New List(Of String) From {"Abdul Shafiq",
        "Alex Chester",
        "Amir Manafikhi",
        "Anushan Mukunthan",
        "Benjamin Pavitt",
        "Borhan Baksh",
        "Charlie Seddon",
        "Curtis Loakes",
        "Danesh Balasubramaniam",
        "Finley Vigor",
        "George Leftwich",
        "Harry Hyde",
        "Harry Wiltshire",
        "Iian Fletcher",
        "Jake Millen",
        "Jared Seymour",
        "Jon Blount",
        "Joshua Edge",
        "Liliana Odjo",
        "Reece Brown",
        "Ryan Griffiths",
        "Ryan Benson",
        "Sohail Mohammed-Khail",
        "Thomas Burns"}
        For Each name As String In fullNames
            Dim allLower As String = name.ToLower()
            Dim firstName As String = allLower.Split(" ")(0)
            Dim lastName As String = allLower.Split(" ")(allLower.Split(" ").Length - 1)
            Dim usrName As String = lastName.Substring(0, 3) + firstName.Substring(0, 3) + "14"
            NameDict(usrName) = name
        Next
        NameDict("thodanst") = "Mr Thomson"
        NameDict("teacher") = "Staff Teacher"
    End Sub
    Public Function GetFullName(usrName As String) As String
        While nameDictInUse
            Threading.Thread.Sleep(1)
        End While
        nameDictInUse = True
        Dim toReturn As String = ""
        For Each nm As String In NameDict.Keys
            If nm = usrName Then toReturn = NameDict(nm)
        Next
        nameDictInUse = False
        If toReturn = "" Then Return usrName
        Return toReturn
    End Function
#End Region

    Public Sub BroadCast_AllClients()
        ' Tells all the clients exactly who is online
        Dim allClients As String = ""
        WaitForUsers()
        For Each usr As User In Users.Values
            allClients = allClients & "." & usr.Name & ";" & usr.Score & ";" & usr.Rank & ";" & usr.ActualName.Replace(" ", "_")
        Next
        UsersInUse = False
        Broadcast("%" & allClients.Substring(1) & "ALLCLIENTS") '..while the new client needs the full list of clients connected
    End Sub

    Friend Sub UnhandledErrors(ByVal sender As Object, ByVal e As UnhandledExceptionEventArgs)
        Dim ex As Exception = CType(e.ExceptionObject, Exception)
        LogMsg(ex, "UnhandledErrors")
        CloseServer(False)
        writeToConsole("Unhandled exception: " & e.ToString())
        Throw ex
    End Sub

#Region "SECTION:Version Checking"
    Public Function GetLatestVersion(url As String) As Version
        Try
            Dim myReq As HttpWebRequest = WebRequest.Create(url)
            Dim response As HttpWebResponse = myReq.GetResponse()
            Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
            Dim strResponse As String = resStream.ReadToEnd().ToString()
            Return New Version(strResponse)
        Catch ex As Exception
            LogMsg(ex, "GetLatestVersion: " + url)
            MsgBox(ex.ToString())
        End Try
        Return New Version("0.0.0.0")
    End Function

    Public Sub UpdateVersionOneAhead(path As String)
        If Not IO.File.Exists(path) Then
            writeToConsole("Unable to update version, server is running independent?")
            Return
        End If
        Dim oldLines As String() = IO.File.ReadAllLines(path)
        Dim loopText As String() = IO.File.ReadAllLines(path)
        For Each line As String In loopText
            If line.Contains("AssemblyFileVersion") Then
                Dim newLine As String = "<Assembly: AssemblyFileVersion(" + ControlChars.Quote
                Dim latestMinor As Integer = LatestVersion.Build + 1
                Dim newVersion As Version = New Version(LatestVersion.Major.ToString + "." + LatestVersion.Minor.ToString + "." + latestMinor.ToString + "." + LatestVersion.Revision.ToString)
                newLine += newVersion.ToString() + ControlChars.Quote + ")>"
                oldLines(Array.IndexOf(oldLines, line)) = newLine
            ElseIf line.Contains("AssemblyVersion") Then
                Dim newLine As String = "<Assembly: AssemblyVersion(" + ControlChars.Quote
                Dim latestMinor As Integer = LatestVersion.Build + 1
                Dim newVersion As Version = New Version(LatestVersion.Major.ToString + "." + LatestVersion.Minor.ToString + "." + latestMinor.ToString + "." + LatestVersion.Revision.ToString)
                newLine += newVersion.ToString() + ControlChars.Quote + ")>"
                oldLines(Array.IndexOf(oldLines, line)) = newLine
            End If
        Next
        File.WriteAllLines(path, oldLines)
        MsgBox("Please re-run this " & clName & " through Visual Studio again." & vbCrLf & vbCrLf & "(Server must quit because the version has been updated)" + vbCrLf + "To prevent this message, run the " + clName + " from a folder that is NOT named 'Debug' or 'Release'")
        End
    End Sub

    Public Const clName As String = "server"
    Public Sub HandleVersion()
        LatestVersion = GetLatestVersion("https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/" + clName + "/" + clName + "/bin/Release/.version")
        writeToConsole("This " & clName & " is running on version " & ThisThingVersion.ToString())
        writeToConsole("The latest version is " & LatestVersion.ToString())
        If ThisThingVersion.CompareTo(LatestVersion) = 0 Then
            ' same version, so we update our one to be ahead, if we are in VS..
            writeToConsole("Running on same version (updating to ahead)")
            Dim curDir As String = IO.Directory.GetCurrentDirectory()
            Dim dirSplit As String() = curDir.Split("\")
            If dirSplit(dirSplit.Count - 1) = "Debug" OrElse dirSplit(dirSplit.Count - 1) = "Release" Then
                Dim toBePath As String = ""
                For Each item As String In dirSplit
                    If Array.IndexOf(dirSplit, item) > dirSplit.Count - 3 Then
                        Continue For
                    End If
                    toBePath += item + "\"
                Next
                toBePath += "\My Project\AssemblyInfo.vb"
                UpdateVersionOneAhead(toBePath)
            End If
        ElseIf ThisThingVersion.CompareTo(LatestVersion) = 1 Then
            writeToConsole("Running ahead of latest.")
        Else
            writeToConsole("WARNING: This " & clName & " is running behind.")
            MsgBox("Warning:" & vbCrLf & "This " & clName & " is not running the latest version" & vbCrLf & "Your version: " & ThisThingVersion.ToString() & vbCrLf &
               "Latest Version: " & LatestVersion.ToString() & vbCrLf & "Attempting to download the latest version...")
            writeToConsole("Downloading latest....")
            Dim url As String = "https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/" & clName & "/" & clName & "/bin/Release/" & clName & ".exe"
            Dim newName As String = "new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe"
            Try
                Dim web_Download As New WebClient
                If File.Exists(newName) Then
                    Dim dlVersion As Version = New Version(FileVersionInfo.GetVersionInfo(newName).FileVersion)
                    If Not dlVersion = LatestVersion Then
                        File.Delete(newName) ' not the latest version
                        web_Download.DownloadFile(url, newName)
                    End If
                Else
                    web_Download.DownloadFile(url, newName)
                End If
                MsgBox("New " & clName & " has been downloaded. Checking version...") ' we dont tell them of the old version.
                Dim downloadVersion As Version = New Version(FileVersionInfo.GetVersionInfo(newName).FileVersion)
                If downloadVersion = LatestVersion Then
                    MsgBox("Version is valid. Running new client now.. this client will close.")
                    Process.Start(newName)
                    Threading.Thread.Sleep(5)
                    End
                Else
                    writeToConsole("ERROR: Please download manually.")
                    MsgBox("Attempts to download the new version have failed." & vbCrLf & "Please download manually.")
                End If
            Catch ex As UnauthorizedAccessException
                MsgBox("You do not have permissions to download/write at that folder" & vbCrLf & "Please copy this client to your personal drive, or run as administrator, and try again.")
            Catch ex As Exception
                LogMsg(ex, "HandleVersion, download file")
                MsgBox(ex.ToString())
            End Try
        End If
        If IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath).ToString = "new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe" Then
            MsgBox("WARNING:" & vbCrLf & "It is advised that you download the complete Visual Studio project of the client." & vbCrLf & "You may ignore this message.")
        End If
    End Sub
#End Region

    Public Sub CloseServer(Optional doClose As Boolean = True)
        Try
            For Each usr As String In Users.Keys
                If usr = "Server" Then Continue For
                Dim usrClient As TcpClient = Users(usr).Client
                Try
                    usrClient.Client.Shutdown(SocketShutdown.Both)
                Catch ex As Exception
                    LogMsg(ex, "CloseServer - client shutdown")
                End Try
            Next
        Catch ex As Exception
            LogMsg(ex, "CloseServer")
        End Try
        BugReporting.BugReporting.SendReport(LogSaveFilePath, "Server", "Drawing", "Server #Log #Not_Looked_At")
        If doClose Then End
    End Sub

    Public AutoStartTimer As New System.Timers.Timer
    Public LobbyChatTimer As New System.Timers.Timer
    Public Sub LobbyChatTimer_Elapsed(sender As Object, e As EventArgs)
        LobbyChatTimer.Stop()
        If isGamePlaying Then
            Return
        End If
        InLobby = True
        Broadcast("lobbyTrue")
        Threading.Thread.Sleep(50)
        SendLobbyChat("Previous round over.", "Server", "Manager")
        If CurrentGame IsNot Nothing Then
            SendLobbyChat("Previous painter: " & CurrentGame.Painter, "Server", "Manager")
            SendLobbyChat("Previous item: " & CurrentGame.ThingToDraw.ToDraw, "Server", "Manager")
            If CurrentGame.Winner IsNot Nothing Then
                SendLobbyChat("Round winner: " & CurrentGame.Winner, "Server", "Manager")
            End If
        End If
        If autoStart Then
            AutoStartTimer.Start()
        End If
    End Sub
    Private AutoStartCountdown As Integer = 26
    Public Sub Timer_Tick(sender As Object, e As EventArgs)
        AutoStartCountdown -= 1
        If LobbyChatTimer.Enabled Then
            AutoStartCountdown += 1
            Return
        End If
        If autoStart = False Then
            AutoStartTimer.Stop()
            AutoStartCountdown = 26
            Return
        End If
        If isGamePlaying Then
            AutoStartCountdown = 26
            AutoStartTimer.Stop()
            Return
        End If
        If Users.Count < 2 Then
            AutoStartCountdown = 26
            Return
        End If
        If AutoStartCountdown > 0 Then
            If AutoStartCountdown = 25 Then
                SendLobbyChat("Next round automatically starting in 25 seconds", "Server", "Autostart")
            ElseIf AutoStartCountdown = 15 Then
                SendLobbyChat("Next round automatically starting in 15 seconds", "Server", "Autostart")
            ElseIf AutoStartCountdown = 10 Then
                SendLobbyChat("Next round starts in 10 seconds", "Server", "Autostart")
            ElseIf AutoStartCountdown <= 5 Then
                SendLobbyChat("Next round starts in " & AutoStartCountdown & " seconds!", "Server", "Autostart")
            End If
        Else
            If Users.Count > 1 Then
                If isGamePlaying = False Then
                    writeToConsole("Game auto-started")
                    AutoStartTimer.Stop()
                    startAgame()
                Else
                    AutoStartTimer.Stop()
                End If
            Else
                writeToConsole("Not enough players to auto-start")
            End If
            AutoStartCountdown = 26
        End If
    End Sub

    Public Sub HandleCommand(input As String, oper As User)
        ' Handles both the commands inputted by the console
        ' AND the commands done by managers
        ' Yes, technically managers can perform any of the following command
        ' That just isnt set up in the current client
        If input.StartsWith("/") Then input = input.Substring(1)
        Dim cmdSplit = input.Split(" ").Where(Function(x) String.IsNullOrWhiteSpace(x) = False).ToList()
        Dim cmdName = cmdSplit(0)
        cmdSplit.RemoveAt(0)
        Commands.ExecuteCommand(cmdName, oper, cmdSplit.ToArray())

        'Dim console As Boolean = oper = "Console"
        'If String.IsNullOrEmpty(input) Then Return
        'If input.Substring(0, 1) <> "/" Then
        '    input = "/" + input
        'End If
        'LogMsg($"{oper}: {input}", "Incoming")
        'Dim inputSplit As New List(Of String)
        'For Each item As String In input.Split(" ")
        '    inputSplit.Add(item)
        'Next
        'Dim cmd As String = inputSplit.Item(0).Substring(1)
        'inputSplit.RemoveAt(0)
        'If cmd = "help" Then
        '    If console Then writeToConsole("Available cmds:" + vbCrLf + "/forcestart - force the game to start" + vbCrLf + "/forcereset - force end the  game" + vbCrLf + "/rank [user] [id] - sets user to rank" + vbCrLf + "/message [message] - sends msg to all users" & vbCrLf & "/close - closes server" & vbCrLf & "/autostart [True/False]")
        'ElseIf cmd = "forcestart" Then
        '    If console Then writeToConsole("Force started the game.")
        '    startAgame()
        'ElseIf cmd = "forcereset" Then
        '    If console Then writeToConsole("Force ending and resetting the game")
        '    isGamePlaying = False
        '    CurrentGame = Nothing
        '    Broadcast("%RESET")
        'ElseIf cmd = "forceend" Then
        '    If console Then
        '        writeToConsole("Force ending the game")
        '    Else
        '        SendAChat("User " & oper & " force ended the game.", "Server", "Server")
        '    End If
        '    isGamePlaying = False
        '    CurrentGame = Nothing
        '    Broadcast("ServerWON")
        'ElseIf cmd = "message" Then
        '    If console Then writeToConsole("Message broadcast")
        '    Broadcast("%&MESSAGE&Server Message: " & String.Join(" ", inputSplit))
        'ElseIf cmd = "close" Then
        '    If console Then writeToConsole("Server close initiated")
        '    CloseServer()
        'ElseIf cmd = "ml" Then
        '    MasterList.SendMasterList(String.Join(" ", inputSplit))
        'ElseIf cmd = "rank" Then
        '    If inputSplit.Count = 2 Then
        '        If Integer.TryParse(inputSplit(1), 1) Then
        '            Dim setRank As Integer = Convert.ToInt32(inputSplit(1))
        '            If setRank >= 0 AndAlso setRank <= 3 Then
        '                If Users.ContainsKey(inputSplit(0)) Then
        '                    WaitForUsers()
        '                    Dim targetUser As User = Users(inputSplit(0))
        '                    targetUser.Rank = setRank
        '                    If console Then writeToConsole("Successfully updated rank, now: " & targetUser.RankName)
        '                    Users(targetUser.Name) = targetUser
        '                    UsersInUse = False
        '                    SendMessageForce(targetUser.Client, "&RANK&" & targetUser.Rank.ToString())
        '                    BroadCast_AllClients()
        '                Else
        '                    writeToConsole("Unknown user: " & inputSplit(0))
        '                End If
        '            Else
        '                writeToConsole("Error; invalid rank: " & inputSplit(1).ToString())
        '            End If
        '        Else
        '            writeToConsole("Error: invalid rank: " & inputSplit(1).ToString())
        '        End If
        '    End If
        'ElseIf cmd = "score" Then
        '    If inputSplit.Count = 1 Then
        '        If Users.ContainsKey(inputSplit(0)) Then
        '            WaitForUsers()
        '            If console Then writeToConsole("Score: " & Users(inputSplit(0)).Score.ToString())
        '            UsersInUse = False
        '        Else
        '            If console Then writeToConsole("Error: unknown player.")
        '        End If
        '    Else
        '        If console Then writeToConsole("Usage: /score [user]")
        '    End If
        'ElseIf cmd = "autostart" Then
        '    If inputSplit.Count = 1 Then
        '        If Boolean.TryParse(inputSplit(0), True) Then
        '            autoStart = Boolean.Parse(inputSplit(0))
        '            If autoStart = True Then
        '                AutoStartTimer.Start()
        '                If console Then writeToConsole("Auto start enabled")
        '            Else
        '                AutoStartTimer.Stop()
        '                If console Then writeToConsole("Auto start disabled")
        '            End If
        '            Broadcast("%autostart" & autoStart.ToString())
        '        Else
        '            ' Not a boolean value
        '            If console Then writeToConsole("Error: must be Boolean value")
        '        End If
        '    Else
        '        If console Then writeToConsole("Usage: /autostart [True/False]")
        '    End If
        'ElseIf cmd = "allow" Then
        '    If inputSplit.Count = 1 Then
        '        AllowChecksums.Add(inputSplit(0))
        '        writeToConsole("New hash registered as allowed: " & inputSplit(0))
        '    Else
        '        If String.IsNullOrWhiteSpace(LastJoinChecksum) Then
        '            If console Then writeToConsole("Usage: /allow [hash]")
        '        Else
        '            AllowChecksums.Add(LastJoinChecksum)
        '            writeToConsole("Last hash registered: " + LastJoinChecksum)
        '        End If
        '    End If
        'ElseIf cmd = "list" Then
        '    Dim conten = String.Join(" , ", AllowChecksums)
        '    writeToConsole("Allowed hashes: " + conten)
        'ElseIf cmd = "reload" Then
        '    AllowChecksums = File.ReadAllText("allowedHash.txt").Split("\r\n").ToList()
        '    writeToConsole("Hashes reloaded")
        'ElseIf cmd = "clear" Then
        '    File.WriteAllText("allowedHash.txt", "")
        '    AllowChecksums.Clear()
        '    writeToConsole("Hashes cleared")
        'ElseIf cmd = "save" Then
        '    File.WriteAllText("allowedHash.txt", String.Join(Environment.NewLine, AllowChecksums))
        '    writeToConsole("Hashes saved")
        'ElseIf cmd = "removelatest" Then
        '    If AllowChecksums.Contains(Latest_Client_Checksum) Then
        '        AllowChecksums.Remove(Latest_Client_Checksum)
        '        writeToConsole("Removed latest client checksum")
        '    Else
        '        writeToConsole("Latest client checksum not loaded")
        '    End If
        'ElseIf cmd = "unbanall" Then
        '    BannedUsers.Clear()
        '    writeToConsole("Unbanned all users")
        '    SaveAllUserInfo()
        'ElseIf cmd = "password" OrElse cmd = "pwd" Then
        '    Dim opt = inputSplit(0)
        '    If opt = "see" Then
        '        If IsPasswordProtected Then
        '            writeToConsole("Password: " + ServerPassword)
        '        Else
        '            writeToConsole("This server has no password, use '/password set [pswd]' to set one")
        '        End If
        '    ElseIf opt = "set" Then ' set
        '        If inputSplit.Count > 1 Then
        '            ServerPassword = inputSplit(1)
        '        Else
        '            ServerPassword = ""
        '        End If
        '        MasterList.Update(SERVER_NAME, Users.Count, IsPasswordProtected)
        '        writeToConsole("Password set to: " + ServerPassword)
        '    End If
        'Else
        '    If console Then writeToConsole("Unknown command, see /help")
        'End If
    End Sub

    Private Sub ServerConnectionReady()
        writeToConsole("-- Server has returned and notified that it is ready to recieve connections")
    End Sub

    Private Sub HandleUserMessage(client As TcpClient, ClientUser As User, clientName As String, mesg As String)
        If mesg.Contains("&LOBBY&") Then
            ' this must be first so players cant enter like 'GUESS' or something
            mesg = mesg.Replace("&LOBBY&", String.Empty)
            If mesg.Substring(0, 1) = "/" AndAlso ClientUser.Rank > 0 Then
                If mesg.Substring(1, 1) = "a" Then
                    If ClientUser.Rank = 2 Then
                        ' Admin - orange
                        mesg = "/a-o " & mesg.Substring(2)
                    Else ' Manager - red
                        mesg = "/a-r " & mesg.Substring(2)
                    End If
                Else
                    If ClientUser.Rank = 1 Then
                        ' Moderator - italic
                        mesg = "\" & mesg.Substring(1)
                    End If
                End If
            End If
            SendLobbyChat(mesg, clientName, ClientUser.RankName)
            Return
        ElseIf mesg.Contains("&ADMINCHAT&") Then
            ' again, another user input.. so it should be first
            Dim msg As String = mesg.Replace("&ADMINCHAT&", String.Empty)
            writeToConsole(clientName + ": " + msg)
            SendAChat(msg, ClientUser.Name, ClientUser.RankName)
            Return ' Dont send this admin chat message
        ElseIf mesg.Contains("GUESS:") Then
            ' We want to sent the guess FIRST;
            ' then if it is correct, send a WON message.
            Broadcast(mesg)
            Dim msgSplt = mesg.Replace("GUESS:", String.Empty).Split(":")
            Dim guess = msgSplt(1)
            guess = guess.Trim()
            If CurrentGame.IsCorrectGuess(guess) Then
                isGamePlaying = False
                Dim nameWhoWon As String = clientName
                Dim usrWon As User = Users(nameWhoWon)
                Dim usrPainter As User = Users(CurrentGame.Painter)
                usrWon.Score += 1
                usrPainter.Score += 1
                WaitForUsers()
                Users(usrWon.Name) = usrWon
                Users(usrPainter.Name) = usrPainter
                UsersInUse = False
                CurrentGame.Winner = usrWon.Name
                writeToConsole(usrWon.Name & " won the game")
                SaveAllUserInfo() ' Game over, person won.
                SendAChat("Game over: " & nameWhoWon & " won", "Server", "Update")
                LobbyChatTimer.Start()
                Broadcast(clientName + "WON")
                MasterList.AddScore(usrWon.UnChangedAccountName)
                MasterList.AddScore(usrPainter.UnChangedAccountName)
            End If
            Return
        ElseIf mesg = "GET_GUID" Then
            Dim serverHash = EncryptSHA256Managed(ServerGUID.ToString())
            SendMessageForce(client, "GUID:" + serverHash)
            Return
        ElseIf mesg.Contains("&SETRANK&") Then
            Dim without As String = mesg.Replace("&SETRANK&", String.Empty)
            Dim uname As String = without.Split(";")(0)
            Dim rank As Integer = Integer.Parse(without.Split(";")(1))
            If ClientUser.Manager Then
                If rank < ClientUser.Rank Then
                    WaitForUsers()
                    Dim targetUser As User
                    If Users.ContainsKey(uname) Then
                        UsersInUse = False
                        targetUser = Users(uname)
                        If targetUser.Rank < ClientUser.Rank Then
                            WaitForUsers()
                            targetUser.Rank = rank
                            Users(targetUser.Name) = targetUser
                            UsersInUse = False
                            writeToConsole(clientName + " ranked " & targetUser.Name & " to " & targetUser.RankName)
                            SendAChat(clientName + " set " + targetUser.Name + " to " + targetUser.RankName, "Server", "Manager")
                            SendMessageForce(targetUser.Client, "&RANK&" & targetUser.Rank.ToString())
                        Else
                            writeToConsole(clientName + "failed to change " & targetUser.Name & ", same rank.")
                        End If
                    Else
                        UsersInUse = False
                        writeToConsole(clientName + " failed to change rank, none existent user.")
                    End If
                Else
                    writeToConsole(clientName + " failed to change rank, invalid.")
                End If
            Else
                writeToConsole(clientName + " failed to change rank, no perms.")
            End If
            BroadCast_AllClients()
            Return
        ElseIf mesg.Contains("WON") Then
            isGamePlaying = False
            Dim nameWhoWon As String = mesg.Substring(0, mesg.IndexOf("WON"))
            Dim usrWon As User = Users(nameWhoWon)
            Dim usrPainter As User = Users(CurrentGame.Painter)
            usrWon.Score += 1
            usrPainter.Score += 1
            WaitForUsers()
            Users(usrWon.Name) = usrWon
            Users(usrPainter.Name) = usrPainter
            UsersInUse = False
            CurrentGame.Winner = usrWon.Name
            writeToConsole(usrWon.Name & " won the game")
            SaveAllUserInfo() ' Server won?
            SendAChat("Game over: " & nameWhoWon & " won", "Server", "Update")
            LobbyChatTimer.Start()
        ElseIf mesg.Contains("CLEAR") Then
            drawLog = "DRAWLOG:"
            HasRecievedPoint = False
            AcceptNewPoints = False
            ClearDelayTimer.Start()
        ElseIf ((mesg.Contains("BRUSHSIZE") Or mesg.Contains("#")) And mesg.Contains("DRAWLOG") = False) Then
            'the things we want to append to the drawlog: coords, brushsize and brush colour
            'also, drawlog is only appended to with the community draw, no need for it after that
            If HasRecievedPoint = False Then
                LogMsg("Skipping point because CLEAR has not been recieved.", "Draw")
                Return
            ElseIf AcceptNewPoints = False Then
                LogMsg("Skipping point becase CLEAR timeout has not passed", "Draw")
                Return
            End If
            drawLog = drawLog & ";" & mesg.Replace("POINT:", String.Empty)
        ElseIf mesg.Contains("@TESTING_CONNECTION@") Then
            Return 'just ignore it
        ElseIf mesg.Contains("&Leaving&") Then
            writeToConsole(clientName + " has said they left")
            If (Users.Count - 1) = 0 Then ' no players.
                UsersInUse = False
                drawLog = "DRAWLOG:"
                isGamePlaying = False
                CurrentGame = Nothing
                InLobby = True
            End If
            Broadcast("%" & clientName & "REMOVECLIENT")
            votesToStart = 0 'when someone leaves, the votes are restarted (couldnt we just remove 1 and recheck? - no, if someone accidently leaves, the game could start without that person + theyd be left out :(( )
            RemoveUser(clientName, client)
            Return
        ElseIf mesg.Contains("Get_DrawLog") Then
            SendMessageForce(ClientUser.Client, drawLog.Replace("%", String.Empty))
            Return ' Returns the draw log to the player that wanted it.
        ElseIf mesg.Contains("POINT") Then
            If isGamePlaying And ClientUser.Painter = False Then
                LogMsg("Point has been skipped because player is not a painter", "Draw")
                Return
            End If
            If previousPoints.Count > 40 Then
                previousPoints.RemoveAt(0)
            End If
            If previousPoints.Contains(mesg) Then
                LogMsg("Ignoring duplicate point from user " & clientName, "Draw")
                Return
            End If
            previousPoints.Add(mesg)
            HasRecievedPoint = True
        ElseIf mesg.Contains("STARTVOTE") Then
            If WhoCanVoteToStart = "Admins" AndAlso ClientUser.Moderator = False Then
                writeToConsole(clientName + ": Unable to accept vote to start")
                Return
            End If
            writeToConsole(clientName + ": Voted to start.")
            votesToStart += 1
            writeToConsole("There are now " + votesToStart.ToString() + " votes (out of " & GetVotesNeededInt().ToString & " needed)")
            If votesToStart >= GetVotesNeededInt() Then
                startAgame()
            Else
                Broadcast("%" & votesToStart.ToString() & "VOTES")
                Broadcast(GetVotesRemaining() + "REMAINING")
            End If
            Return
        ElseIf mesg.Contains("&REPORT&") Then
            mesg = mesg.Replace("&REPORT&", String.Empty)
            Dim reason As String = mesg.Substring(mesg.IndexOf(";") + 1)
            Dim targ As String = mesg.Substring(0, mesg.IndexOf(";"))
            WaitForUsers()
            Dim targetUser As User = Users(targ)
            UsersInUse = False
            If targetUser.AddReport(clientName) Then
                ' succeeded
                SendAChat(clientName + " reported " + targ + " for: " + reason + " - (Count: " & targetUser.Reports.ToString() + ")", "Server", "Server")
                WaitForUsers()
                Users(targ) = targetUser
                UsersInUse = False
            Else
                writeToConsole(clientName + ": unable to report " + targetUser.Name)
            End If

        ElseIf mesg.Contains("&RESET&") Then
            Dim uName As String = mesg.Substring(0, mesg.IndexOf("&RESET&"))
            WaitForUsers()
            Users(uName).Score = 0
            UsersInUse = False
            writeToConsole(clientName + ": Reset score of " & uName)
            BroadCast_AllClients()
            Broadcast(GetVotesRemaining() + "REMAINING")
            Return
        ElseIf mesg.Contains("&UNBAN&") Then
            Dim uName As String = mesg.Substring(0, mesg.IndexOf("&UNBAN&"))
            If ClientUser.Manager Then
                UnBanUser(uName, clientName)
            Else
                writeToConsole(clientName + ": unable to unban " & uName + ", no perms")
            End If
            Return ' Should not continue this.
        ElseIf mesg.Contains("&TOGGLE_IP&") Then
            Dim usrName As String = mesg.Replace("&TOGGLE_IP&", String.Empty)
            If Not ClientUser.Manager Then
                writeToConsole(clientName + ": unable to set serial of " + usrName + ", no perms")
                Return
            End If
            Dim oldBan As Ban = New Ban()
            For Each ban As Ban In BannedUsers
                If ban.Name = usrName Then
                    oldBan = ban
                End If
            Next
            If oldBan.Name Is Nothing Then
                writeToConsole(clientName + ": unable to toggle ip on ban: " & usrName + ", void.")
                Return
            End If
            oldBan.IP_Ban = Not oldBan.IP_Ban
            writeToConsole(clientName + ": made " & usrName + " ip ban: " & oldBan.IP_Ban.ToString())
            UpdateBan(usrName, oldBan)
        ElseIf mesg.Contains("&TOGGLE_SERIAL&") Then
            Dim usrName As String = mesg.Replace("&TOGGLE_SERIAL&", String.Empty)
            If Not ClientUser.Manager Then
                writeToConsole(clientName + ": unable to set serial of " + usrName + ", no perms")
                Return
            End If
            Dim oldBan As Ban = New Ban()
            For Each ban As Ban In BannedUsers
                If ban.Name = usrName Then
                    oldBan = ban
                End If
            Next
            If oldBan.Name Is Nothing Then
                writeToConsole(clientName + ": unable to toggle serial on ban: " & usrName + ", void.")
                Return
            End If
            oldBan.Serial_Ban = Not oldBan.Serial_Ban
            writeToConsole(clientName + ": made " & usrName + " serial ban: " & oldBan.Serial_Ban.ToString())
            UpdateBan(usrName, oldBan)
        ElseIf mesg.Contains("PAINTERLEFT") Then
            writeToConsole("The painter left the game.")
            isGamePlaying = False
            AutoStartTimer.Start()
            Return
        ElseIf mesg.Contains("&DIFFICULTY&") Then
            ' Client (non-admin) suggestion on difficulty
            Dim propose As String = mesg.Replace("&DIFFICULTY&", String.Empty)
            ' propose is the name of the difficulty
            SendAChat(clientName + " proposes setting the word list to '" + propose + "'", "Server", "Server")
            Return
        ElseIf mesg.Contains("&U_MESSAGE&") Then
            mesg = mesg.Replace("&U_MESSAGE&", String.Empty)
            Dim propmsg As String = mesg.Substring(0, mesg.IndexOf(";"))
            Dim prousr As String = mesg.Substring(mesg.IndexOf(";") + 1)
            WaitForUsers()
            If Users.ContainsKey(prousr) Then
                UsersInUse = False
                Dim targetUser As User = Users(prousr)
                If Not targetUser.BlockedUsers.Contains(ClientUser.ActualName) Then
                    SendMessageForce(targetUser.Client, "&MESSAGE&Private message from " & clientName & ":" & vbCrLf & propmsg)
                    writeToConsole(clientName + " PM " + prousr + ": " + propmsg)
                    SendAChat(clientName + " PM " + prousr + ": " + propmsg, "Server", "PMs")
                Else
                    SendMessageForce(ClientUser.Client, "&MESSAGE&Your private message to " + targetUser.Name + " was not recieved.")
                End If
            Else
                UsersInUse = False
                writeToConsole(clientName + " failed to PM " + prousr)
            End If
            Return
        ElseIf mesg.Contains("&BLOCK&") Then
            mesg = mesg.Replace("&BLOCK&", String.Empty)
            WaitForUsers()
            Dim target = Users(mesg)
            UsersInUse = False
            If Not ClientUser.BlockedUsers.Contains(target.ActualName) Then
                ClientUser.BlockedUsers.Add(target.ActualName)
            End If
            Return
        ElseIf mesg.Contains("difficulty") Then
            Dim previous = Difficulty
            Difficulty = mesg.Replace("difficulty", String.Empty)
            DrawingSocketServer.RegisterSettingUpdate("Difficulty", previous, Difficulty, clientName)
        ElseIf mesg.Contains("whocanvote") Then
            Dim previous = WhoCanVoteToStart
            WhoCanVoteToStart = mesg.Replace("whocanvote", String.Empty)
            Broadcast(GetVotesRemaining() + "REMAINING") ' Re-calculate who needs to vote.
            DrawingSocketServer.RegisterSettingUpdate("Who Can Vote", previous, WhoCanVoteToStart, clientName)
        ElseIf mesg.StartsWith("/") Then
            writeToConsole(clientName + ": " & mesg)
            HandleCommand(mesg, ClientUser) ' Allows users to perform commands
            Return
        End If
        Broadcast("%" & mesg)
    End Sub

    Private Sub HandleUserDisconnect(client As TcpClient, _user As User)
        SendLobbyChat($"{_user.Name} has disconnected/left", "Server", "Status")
    End Sub

    Private Sub HandleUserConnection(client As TcpClient, clientObj As User)
        Dim ipend As Net.IPEndPoint = client.Client.RemoteEndPoint
        writeToConsole(clientObj.Name + "(" + ipend.Address.ToString + ")[" + clientObj.Serial + "]{" + clientObj.ActualName + "} connected to the drawing server; Rank: " & clientObj.RankName)
        SendLobbyChat($"{clientObj.Name} has connected.", "Server", "Status")
    End Sub

    Private Sub HandleSettingsUpdate(settingName As String, previousValue As String, newValue As String, thouWhoPerformed As String)
        Dim message = $"{thouWhoPerformed} changed {settingName} from {previousValue} to a new value of {newValue}"
        SendAChat(message, "Server", "Manager")
        writeToConsole(message)
    End Sub

    Private Function ResolveAssemblies(sender As Object, e As System.ResolveEventArgs) As Reflection.Assembly
        Dim desiredAssembly = New Reflection.AssemblyName(e.Name)
        If desiredAssembly.Name = "MasterlistDLL" Then
            Return Nothing 'Reflection.Assembly.Load(My.Resources.MasterlistDLL) 'replace with your assembly's resource name
        Else
            Return Nothing
        End If
    End Function

    Private Sub MasterlistReceiveMessage(sender As Object, e As ReceiveMessageEventArgs)
        writeToConsole("MASTERLIST: from: " + e.LastOperation + "; " + e.Message)
        ' handle details here
        Dim mesg = e.Message
        If mesg.StartsWith("200") = True Then
            ' 200 = OK http response code
        Else
            ' some other response, error or request
            If mesg = "GetDetails" Then
                Dim toSend As String = ""
                WaitForUsers()
                toSend += InLobby.ToString() + ","
                toSend += If(CurrentGame IsNot Nothing, CurrentGame.Painter, "-") + ","
                toSend += If(CurrentGame IsNot Nothing, CurrentGame.ThingToDraw.ToDraw, "-") + ","
                For Each usr As User In Users.Values
                    toSend += usr.Name + ":" + usr.Score.ToString() + ";"
                Next
                UsersInUse = False
                If toSend = "" Then
                    toSend = "[None]"
                End If
                MasterList.SendMasterList("GAME:" + toSend)
            Else
                writeToConsole("While attempting a " + MasterList.LastOperationDone + ", masterlist returned a " + mesg + " error")
            End If
        End If
    End Sub

    Private Sub MasterListLog(sender As Object, e As String)
        writeToConsole("MASTERLIST: " + e)
    End Sub

    Public Sub Main()
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledErrors
        AddHandler AppDomain.CurrentDomain.AssemblyResolve, AddressOf ResolveAssemblies
        AddHandler DrawingSocketServer.Ready, AddressOf ServerConnectionReady
        AddHandler DrawingSocketServer.UserMessage, AddressOf HandleUserMessage
        AddHandler DrawingSocketServer.UserDisconnect, AddressOf HandleUserDisconnect
        AddHandler DrawingSocketServer.UserConnected, AddressOf HandleUserConnection
        AddHandler DrawingSocketServer.SettingUpdated, AddressOf HandleSettingsUpdate
        HandleJsonConfig()
        writeToConsole("Masterlist verison " + DLL.GetDLLVersion)
        Dim mlOnline = False
        Try
            MasterList = New MasterlistServer(RUN_EXTERNALLY, MasterList_GameType.Drawing)
            mlOnline = String.IsNullOrWhiteSpace(MasterList.MASTERLIST_IP) = False
        Catch ex As MasterListException
            writeToConsole("Masterlist is offline")
        End Try
        If mlOnline Then
            AddHandler MasterList.RecieveMessage, AddressOf MasterlistReceiveMessage
            AddHandler MasterList.LogMessage, AddressOf MasterListLog
            writeToConsole("Connected to masterlist at " + MasterList.MASTERLIST_IP)
        Else
            writeToConsole("Masterlist connection failed to respond - masterlist offline")
            MasterListDisabled = True
        End If
        ThisThingVersion = Assembly.GetExecutingAssembly().GetName().Version
        File.WriteAllText(".version", ThisThingVersion.ToString())
        DrawingSocketServer.Run()
        While True
            ' input commands
            Dim input As String = Console.ReadLine()
            If String.IsNullOrEmpty(input) Then Continue While
            Dim servUser As New User("Server", Nothing, Rank.Server, Nothing, "--Sauron--")
            HandleCommand(input, servUser)
        End While
    End Sub

    Public Const SECOND = 1000
    Public Const MINUTE = SECOND * 60
    Public Sub BanUser(targetUser As User, whoBy As String, reason As String)
        SendMessageForce(targetUser.Client, "&CLOSE&You were banned by " & whoBy & vbCrLf & "You are no longer able to join these games." + vbCrLf + reason)
        writeToConsole(targetUser.Name + " was banned by " & whoBy + "; reason: " + reason)
        SendAChat(whoBy + " banned " + targetUser.Name + "; reason: " + reason, "Server", "Server")
        RemoveUser(targetUser.Name, targetUser.Client, False)
        BroadCast_AllClients()
        Dim temp_ As New Ban(targetUser, "IP", reason)
        BannedUsers.Add(temp_)
        Dim allbans As String = ""
        For Each usr As Ban In BannedUsers
            allbans += ";" + usr.ToString()
        Next
        WaitForUsers()
        For Each usr As User In Users.Values
            If usr.Moderator Then
                SendMessageForce(usr.Client, allbans + "ALLBANS")
            End If
        Next
        UsersInUse = False
        SaveAllUserInfo()
    End Sub

    Public Function GetBan(name As String) As Ban
        For Each ban As Ban In BannedUsers
            If ban.Name = name Then
                Return ban
            End If
        Next
        Return Nothing
    End Function

    Public Sub UpdateBan(oldName As String, newBan As Ban)
        Dim index As Integer = -1
        For Each ban As Ban In BannedUsers
            If ban.Name = oldName Then
                index = BannedUsers.IndexOf(ban)
            End If
        Next
        If index > -1 Then
            BannedUsers(index) = newBan
        End If
        Dim allbans As String = "" ' TODO: make this a function.
        For Each usr As Ban In BannedUsers
            allbans += ";" + usr.ToString()
        Next
        WaitForUsers()
        For Each usr As User In Users.Values
            If usr.Moderator Then
                SendMessageForce(usr.Client, allbans + "ALLBANS")
            End If
        Next
        UsersInUse = False
        SaveAllUserInfo()
    End Sub

    Public Sub UnBanUser(name As String, oper As String)
        Dim index As Integer = -1
        For Each ban As Ban In BannedUsers
            If ban.Name = name Then
                index = BannedUsers.IndexOf(ban)
            End If
        Next
        If index > -1 Then
            BannedUsers.RemoveAt(index)
            writeToConsole(oper + ": un-banned user " & name)
            SendAChat(oper + ": un-banned user " & name, "Server", "Server")
        Else
            writeToConsole(oper + ": unable to unban user " + name + ", not banned.")
        End If
        Dim allbans As String = ""
        For Each usr As Ban In BannedUsers
            allbans += ";" + usr.ToString()
        Next
        WaitForUsers()
        For Each usr As User In Users.Values
            If usr.Moderator Then
                SendMessageForce(usr.Client, allbans + "ALLBANS")
            End If
        Next
        UsersInUse = False
        SaveAllUserInfo()
    End Sub

    Public Sub startAgame()
        InLobby = False
        LobbyChatTimer.Stop()
        isGamePlaying = True
        If Painters.Count = 0 Then
            Painters = New List(Of String)
            WaitForUsers()
            For Each item In Users.Keys
                Painters.Add(item)
            Next
            UsersInUse = False
        End If
        If CurrentGame IsNot Nothing AndAlso CurrentGame.Winner IsNot Nothing AndAlso CurrentGame.Winner <> "Server" AndAlso Painters.Contains(CurrentGame.Winner) Then
            ' Ensures there was a previous game, AND
            ' Ensures the winner is not empty, AND
            ' Ensures the winner is not server (manual end), AND
            ' Ensures the winner has not previously drawn (thus preventing it from being back-forth between a few people)
            ' If any one of the above is false, it will not evaluate the remaining conditions
            painter = CurrentGame.Winner
            writeToConsole("New game painter is previous game winner")
        Else
            painter = Painters(rand.Next(Painters.Count - 1))
        End If
        Painters.Remove(painter) 'once someone has painted one, they wont paint again until everyone else has
        votesToStart = 0
        Dim list_ As Dictionary(Of String, List(Of String)) = AllWordItems.Item(Difficulty).Words
        Dim key As String
        Dim index As Integer = rand.Next(0, list_.Count - 1)
        key = list_.Keys(index)
        Dim thingToDraw As DrawItem = New DrawItem(key, list_.Item(key))
        CurrentGame = New DrawGame(painter, thingToDraw)
        writeToConsole(vbCrLf & "-- New Game --")
        writeToConsole("Item: " & CurrentGame.ThingToDraw.ToDraw)
        writeToConsole("Painter: " & CurrentGame.Painter)
        writeToConsole("!- New Game -!")
        SendAChat("New game! Painter: " & CurrentGame.Painter, "Server", "Update")
        WaitForUsers()
        For Each client In Users.Keys
            ' GUESSER/WHO IS PAINTER
            ' PAINTER/WHO IS PAINTER/ITEM
            If Not client = painter Then
                SendMessageForce(Users(client).Client, "%" & "THINGTODRAW:GUESSER/" + painter + "/<null>")
                Users(client).Painter = False ' Clients are not told the answer, so no reverse engineering possible
            Else
                SendMessageForce(Users(client).Client, "%" & "THINGTODRAW:PAINTER/" + painter + "/" + thingToDraw.ToDraw)
                Users(client).Painter = True
            End If
        Next
        UsersInUse = False
    End Sub

    Sub writeToConsole(ByVal mesg As String)
        'puts the stuff from the Send() sub in the client on the server console
        mesg.Trim()
        mesg.Replace("   ", " ") ' should help remove excessive whitespace
        Console.WriteLine("[" + DateTime.Now.ToString("hh:mm:ss.fff") + "] " + mesg)
        LogMsg(LogSeverity.Info, mesg, "Console")
    End Sub

    Public Function GetVotesRemaining() As String
        Return (GetVotesNeededInt() - votesToStart).ToString()
    End Function

    Public Function GetVotesNeededInt() As Integer
#If DEBUG Then
        Return 0
#Else
        ' How we calculate the votes:
        ' If only admins can vote:
        ' - Requires all admin votes.
        ' If anyone can vote:
        ' - Require 3/4 majority
        If WhoCanVoteToStart = "Admins" Then
            WaitForUsers()
            Dim admins As Integer = 0
            For Each usr As User In Users.Values
                If usr.Moderator Then admins += 1
            Next
            UsersInUse = False
            Return admins
        Else
            Dim totalUsers As Integer = Users.Count
            Dim quaterOfThat As Double = (totalUsers / 4)
            Dim minimumVotes As Integer = Math.Round(quaterOfThat * 3)
            If totalUsers < 4 Then
                Return 4 - totalUsers
            End If
            Return minimumVotes
        End If
#End If
    End Function

    Public Sub SendLobbyChat(msg As String, sender As String, rank As String)
        LogMsg(sender + " (" + rank + "): " + msg, "LOBBY")
        Dim sending As String = "&LOBBY&" & sender + ";" + rank + ";" + msg
        Broadcast(sending)
    End Sub

    Public Sub SendAChat(msg As String, sender As String, rank As String)
        LogMsg(sender + " (" + rank + "): " + msg, "ADMIN")
        WaitForUsers()
        msg = sender + ";" + rank + ";" + msg
        For Each usr As User In Users.Values
            If usr.Moderator Then
                SendMessageForce(usr.Client, msg + "&ADMINCHAT&")
            End If
        Next
        UsersInUse = False
    End Sub
End Module
