﻿Imports System.Reflection
Imports System.Linq
Imports System.IO
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Threading
Imports System.Windows.Forms
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Runtime.Serialization
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CommandInfo
    Public Property Name As String
    Public Property Summary As String
    Public Property Paramaters As List(Of ParameterInfo)
    Public Property Method As MethodInfo
    Public Property MinimumRank As MainProgram.Rank
    Public Property Group As Integer

    Public ReadOnly Property FullUsage As String
        Get
            Dim str As String = "/" + Name + " "
            For Each param In Paramaters
                If param.IsOptional Then
                    str += "<" + param.Name + ">"
                Else
                    str += "[" + param.Name + "]"
                End If
                str += " "
            Next
            Return str
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return FullUsage
    End Function


    Public Sub Execute(usr As User, ParamArray args() As Object)
        args = args.Where(Function(x) x IsNot Nothing).ToArray()
        Dim newContext As TheActualCommands = New TheActualCommands()
        If usr Is Nothing Then
            newContext.Context = New CommandContext(Nothing, Nothing, False)
        Else
            newContext.Context = New CommandContext(usr.Client, usr)
        End If
        If MinimumRank > usr.Rank Then
            newContext.Reply("Error: you do not have the required permissions to perform that command")
            Return
        End If
        Dim requiredParams = Paramaters.Where(Function(x) x.IsOptional = False).Count
        If args.Length < requiredParams Then
            If Paramaters.LastOrDefault().IsRemainder = False Then
                newContext.Reply("Usage: " + FullUsage)
                Return
            End If
        End If
        If args.Length > Paramaters.Count Then
            If Paramaters.LastOrDefault().IsRemainder = False Then
                newContext.Reply("Usage: " + FullUsage)
                Return
            End If
        End If
        Dim nowArgs As New List(Of Object) ' to hold the parsed arguments according to what they should be
        For i As Integer = 0 To args.Length - 1
            Dim givenArg = args(i)
            Dim parameter = Paramaters(i)
            If parameter.IsRemainder Then
                ' remainder should (must) always be a string, so we can just join the remainder of the array
                Dim finals As New List(Of String)
                For x As Integer = i To args.Length - 1
                    finals.Add(Convert.ToString(args(x)))
                Next
                nowArgs.Add(String.Join(" ", finals))
                Exit For
            End If
            Try
                Dim arg = Nothing
                Select Case parameter.ParamType
                    Case GetType(String)
                        arg = CStr(givenArg)
                    Case GetType(Integer)
                        arg = CInt(givenArg)
                    Case GetType(Boolean)
                        arg = CBool(givenArg)
                    Case GetType(User)
                        WaitForUsers()
                        Dim nUser As User = Nothing
                        Users.TryGetValue(givenArg, nUser)
                        If nUser Is Nothing Then
                            ' we will try to use the computer name
                            nUser = Users.Values.FirstOrDefault(Function(x) x.UnChangedAccountName.ToLower() = givenArg.ToString().ToLower())
                        End If
                        UsersInUse = False
                        If nUser Is Nothing Then
                            newContext.Reply("Error: user not found '" + givenArg + "' (for paramater " + parameter.Name + ")")
                            Return
                        End If
                        arg = nUser
                    Case Else
                        arg = givenArg
                End Select
                If arg.GetType() <> parameter.ParamType Then
                    newContext.Reply("Error: unable to parse your given '" + givenArg + "' for paramater of type " + parameter.ParamType.ToString() + " for " + parameter.Name)
                End If
                nowArgs.Add(arg)
            Catch ex As InvalidCastException
                newContext.Reply($"Error: paramater {parameter.Name} needs to be of type {parameter.ParamType.Name}")
                Return
            Catch ex As Exception
                writeToConsole($"Errored while executing {Name} for {If(newContext.Context.User Is Nothing, "Console", newContext.Context.User.Name)}, error: {ex.ToString()}")
                newContext.Reply($"Error occured while attempting to parse paramater {parameter.Name}")
                Return
            End Try
        Next
        While nowArgs.Count < Paramaters.Count ' since Invoke() needs the paramaters to be there, but wants them 'Missing'
            nowArgs.Add(Type.Missing)
        End While
        Method.Invoke(newContext, nowArgs.ToArray())
        'CallByName(newContext, Method.Name, CallType.Method, nowArgs)
    End Sub
End Class

Public Class CommandException
    Inherits Exception

    Public Sub New()
    End Sub

    Public Sub New(message As String)
        MyBase.New(message)
    End Sub

    Public Sub New(message As String, innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    Protected Sub New(info As SerializationInfo, context As StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class

Public Class ParameterInfo
    Public ParamType As Type
    Public Name As String
    Public IsOptional As Boolean
    Public IsRemainder As Boolean
    Public DefaultValue As Object

    Public Overrides Function ToString() As String
        Return $"{ParamType.FullName} {Name}{If(IsOptional, " Optional", "")}{If(IsRemainder, " Remainder", "")}{If(DefaultValue Is Nothing, "", " Default: " + DefaultValue)}"
    End Function
End Class

Public Class CommandResult
    Public Success As Boolean
    Public ErrorMessage As String

    Public Sub New()
        Success = True
        ErrorMessage = ""
    End Sub

    Public Sub New(message As String)
        Success = False
        ErrorMessage = message
    End Sub

    Public Sub New(except As Exception)
        Success = False
        ErrorMessage = except.ToString()
    End Sub
End Class


''' <summary>
''' Used to give the actual name of the command, what is used to call it
''' </summary>
<AttributeUsage(AttributeTargets.Method, AllowMultiple:=False)>
Public Class NameAttribute
    Inherits Attribute
    Public ReadOnly Name As String
    Public Sub New(_name As String)
        Name = _name
    End Sub
    Public Overrides Function ToString() As String
        Return Name
    End Function
End Class

''' <summary>
''' Used to describe the purpose of the command
''' </summary>
<AttributeUsage(AttributeTargets.Method, AllowMultiple:=False)>
Public Class SummaryAttribute
    Inherits Attribute
    Public ReadOnly Summary As String
    Public Sub New(_summary As String)
        Summary = _summary
    End Sub
    Public Overrides Function ToString() As String
        Return Summary
    End Function
End Class

''' <summary>
''' Used to provide the minimum rank to the command
''' </summary>
<AttributeUsage(AttributeTargets.Method, AllowMultiple:=False)>
Public Class RankAttribute
    Inherits Attribute
    Public ReadOnly Required As MainProgram.Rank
    Public Sub New(_rank As MainProgram.Rank)
        Required = _rank
    End Sub
    Public Overrides Function ToString() As String
        Return Required.ToString()
    End Function
End Class

''' <summary>
''' If a paramater is marked as Remainder, then any following arguments passed will apply to this one
''' 
''' <!-- Please note: this command should be put at the END as the FINAL paramater, as any following will be part of it -->
''' </summary>
<AttributeUsage(AttributeTargets.Parameter, AllowMultiple:=False)>
Public Class RemainderAttribute
    Inherits Attribute
End Class

<AttributeUsage(AttributeTargets.Method, AllowMultiple:=False)>
Public Class GroupAttribute
    Inherits Attribute
    Public ReadOnly Group As Integer
    Public Sub New(_group As Integer)
        Group = _group
    End Sub
    Public Overrides Function ToString() As String
        Return Group
    End Function
End Class


Public Module Commands
    ' List of EVERY Command

    Friend ALL_COMMANDS As New Dictionary(Of String, CommandInfo)

    Public Sub ExecuteCommand(name As String, usr As User, ParamArray args() As Object)
        Try
            ALL_COMMANDS(name).Execute(usr, args)
        Catch ex As KeyNotFoundException
            If usr.Client Is Nothing Then
                writeToConsole("Error: that command was not found")
            Else
                SendMessageForce(usr.Client, "&MESSAGE&That command was not found")
            End If
        Catch ex As Exception
            writeToConsole(ex.ToString())
            If usr IsNot Nothing Then writeToConsole("Unable to execute " + name + " for " + usr.Name)
        End Try
    End Sub

    Public Sub LOAD_ALL_COMMANDS()
        Dim typeInfo = GetType(TheActualCommands).GetTypeInfo()

        Dim allMethods = typeInfo.DeclaredMethods
        Dim validMethods = New List(Of MethodInfo)
        For Each method As MethodInfo In allMethods
            If IsValidCommandDefinition(method) Then
                validMethods.Add(method)
            End If
        Next

        Dim _commands As New List(Of CommandInfo)

        For Each method As MethodInfo In validMethods
            Dim cmd = New CommandInfo()
            cmd.Method = method
            cmd.Paramaters = New List(Of ParameterInfo)()
            cmd.Group = 0 ' default
            Dim attributes = method.GetCustomAttributes()
            For Each attr As Attribute In attributes
                Select Case attr.GetType()
                    Case GetType(NameAttribute)
                        Dim attr_ = CType(attr, NameAttribute)
                        cmd.Name = attr_.Name
                    Case GetType(SummaryAttribute)
                        Dim attr_ = CType(attr, SummaryAttribute)
                        cmd.Summary = attr_.Summary
                    Case GetType(RankAttribute)
                        Dim attr_ = CType(attr, RankAttribute)
                        cmd.MinimumRank = attr_.Required
                    Case GetType(GroupAttribute)
                        Dim attr_ = CType(attr, GroupAttribute)
                        cmd.Group = attr_.Group
                End Select
            Next
            If cmd.Name = Nothing Then
                Throw New InvalidOperationException("Command name not present: " + method.Name)
            End If
            If cmd.Summary = Nothing Then
                Throw New InvalidOperationException("Command summary not present: " + method.Name)
            End If
            If cmd.MinimumRank = Nothing Then
                writeToConsole($"Command {cmd.Name} has method {method.Name}(), but is missing RankAttribute - defaulting to Player")
                cmd.MinimumRank = Rank.Player
            End If
            Dim paramaters = method.GetParameters()
            For Each _param In paramaters
                Dim paraminfo = New ParameterInfo()
                paraminfo.Name = _param.Name
                paraminfo.IsOptional = _param.IsOptional
                paraminfo.IsRemainder = False
                For Each attr In _param.CustomAttributes
                    If attr.AttributeType Is GetType(RemainderAttribute) Then
                        paraminfo.IsRemainder = True
                        Exit For
                    End If
                Next
                paraminfo.DefaultValue = If(_param.HasDefaultValue, _param.DefaultValue, Nothing)
                paraminfo.ParamType = _param.ParameterType
                cmd.Paramaters.Add(paraminfo)
            Next
            _commands.Add(cmd)
        Next
        _commands = _commands.OrderBy(Function(cmd) cmd.Group).ThenBy(Function(cmd) cmd.MinimumRank).ThenBy(Function(cmd) cmd.Name).ToList()
        For Each cmd In _commands
            ALL_COMMANDS.Add(cmd.Name, cmd)
        Next
        Dim str As String = "Loaded " + ALL_COMMANDS.Keys.Count.ToString() + " commands:"
        For Each cmd In ALL_COMMANDS
            str += $"{vbCrLf}[{cmd.Value.MinimumRank.ToString()}]  {cmd.Value.FullUsage} - {cmd.Value.Summary}"
        Next
        writeToConsole(str)
    End Sub
    Private Function IsValidCommandDefinition(method As MethodInfo) As Boolean
        Dim names As Boolean = False
        For Each attr In method.CustomAttributes
            If attr.AttributeType = GetType(NameAttribute) Then
                names = True
                Exit For
            End If
        Next
        Return names AndAlso (method.ReturnType = GetType(Void)) AndAlso (method.IsStatic = False) AndAlso (method.IsGenericMethod = False)
    End Function
End Module

Public Class CommandContext
    Public SocketClient As TcpClient
    Public User As User
    Public Location As ContextLocation

    Public Sub New(client As TcpClient, usr As User, Optional adminChat As Boolean = False)
        SocketClient = client
        User = usr
        If client Is Nothing Then
            Location = ContextLocation.Console
        Else
            If adminChat Then
                Location = ContextLocation.AdminChat
            Else
                Location = ContextLocation.Lobby
            End If
        End If

    End Sub
End Class

Public Enum ContextLocation
    Lobby
    AdminChat
    Console
    PopUpMessage ' shouldnt be used except for in Send function
End Enum

Public Class CommandModuleBase
    Public Property Context As CommandContext

    ''' <summary>
    ''' Responds to where-ever the command was exectued in, but only to the command operator
    ''' </summary>
    ''' <param name="message"></param>
    Public Function Reply(message As String) As Task
        If Context.Location = ContextLocation.Lobby Then
            Dim sending As String = $"&LOBBY&Server;Server;CMD: {message}"
            MainProgram.SendMessageForce(Context.SocketClient, sending)
        ElseIf Context.Location = ContextLocation.AdminChat Then
            Dim sending As String = $"Server;Server;{message}&ADMINCHAT&"
            MainProgram.SendMessageForce(Context.SocketClient, sending)
        Else
            writeToConsole("CMD: " + message)
        End If
        Return Task.CompletedTask
    End Function

    ''' <summary>
    ''' Responds the message to whereever the cmd was executed, visible to all users in that channel.
    ''' </summary>
    ''' <param name="message"></param>
    ''' <returns></returns>
    Public Function Channel(message As String) As Task
        If Context.Location = ContextLocation.Lobby Then
            MainProgram.SendLobbyChat(message, "Server", "Server")
        ElseIf Context.Location = ContextLocation.AdminChat Then
            MainProgram.SendAChat(message, "Server", "Server")
        Else
            writeToConsole("CMD: " + message)
        End If
        Return Task.CompletedTask
    End Function
End Class

Public Module CommandExtensions

    ''' <summary>
    ''' Sends the user a message in the given context
    ''' </summary>
    ''' <param name="usr">User to be messaged</param>
    ''' <param name="message">Message to be sent</param>
    ''' <param name="location">Context to be applied to - PopUpMessage opens a MsgBox() on client side</param>
    <Extension()>
    Public Sub SendChat(usr As User, message As String, Optional location As ContextLocation = ContextLocation.Lobby)
        If location = ContextLocation.Lobby Then
            SendMessageForce(usr.Client, $"&LOBBY&Server;Server;{message}")
        ElseIf location = ContextLocation.AdminChat Then
            SendMessageForce(usr.Client, $"Server;Server;{message}&ADMINCHAT&")
        ElseIf location = ContextLocation.PopUpMessage Then
            SendMessageForce(usr.Client, "&MESSAGE&" + message)
        End If
    End Sub
End Module

Public Class TheActualCommands
    Inherits CommandModuleBase

    Enum Group
        General = 0 ' not used
        UserManagement
        Checksums
        Password
        GameRelated
    End Enum

#Region "General Commands"
    <Name("help"), Summary("Lists all commands available")>
    Public Sub ListAllCommands()
        Dim str = "Commands:"
        For Each cmd As CommandInfo In ALL_COMMANDS.Values
            If Context.User.Rank >= cmd.MinimumRank Then
                str += $"{vbCrLf}{cmd.FullUsage} - {cmd.Summary}"
            End If
        Next
        Reply(str)
    End Sub

    <Name("players")>
    <Summary("Lists all players online")>
    Public Sub ListAllUsers()
        WaitForUsers()
        Dim str As String = "Players Online (" + Users.Count.ToString() + ")"
        For Each keypair In Users
            str += vbCrLf
            str += keypair.Key + " "
            If Context.User.Moderator Then
                str += "(" + keypair.Value.ActualName + ") "
            End If
            If Context.User.Admin Then
                str += "[" + keypair.Value.IP.ToString() + "] "
            End If
            If Context.User.Manager Then
                str += "<" + keypair.Value.Serial + ">"
            End If
        Next
        UsersInUse = False
        Reply(str)
    End Sub

    <Name("msg")>
    <Summary("Sends the message to the user")>
    <Rank(Rank.Manager)>
    Public Sub SendUserMessageFromUser(usr As User, <Remainder> message As String)
        usr.SendChat(message)
    End Sub

    <Name("broadcast"), Summary("Broadcasts a pop-up to all users")>
    <Rank(Rank.Manager)>
    Public Sub BroadCastToAllUsers(<Remainder> message As String)
        Broadcast("&MESSAGE&" + message)
    End Sub

    <Name("score"), Summary("Sets a user's score")>
    <Rank(Rank.Admin)>
    Public Sub SetUserScore(usr As User, score As Integer)
        usr.Score = score
    End Sub

    <Name("close"), Summary("Closes the server")>
    <Rank(Rank.Server)>
    Public Sub CommandCloseServer()
        CloseServer()
    End Sub

#End Region

#Region "User-Management Commands"

    <Name("rank")>
    <Summary("Sets user rank")>
    <Group(Group.UserManagement), Rank(Rank.Admin)> ' admins can set people to moderator, but not vic-versa
    Public Sub SetUserRank([user] As User, rank As Integer)
        If [user] Is Nothing Then
            Reply("Error: unknown user")
            Return
        End If
        If Context.User.Rank > rank Then
            If Context.User.Rank > [user].Rank Then
                [user].Rank = rank
                BroadCast_AllClients()
                SendMessageForce([user].Client, "&RANK&" + [user].Rank.ToString())
                Reply("Rank of " + [user].Name + " has been set to " + [user].RankName)
            Else
                Reply("Error: incorrect permissions to set that user's rank")
            End If
        Else
            Reply("Error: incorrect permissions to set that rank")
        End If
    End Sub


    <Name("kick"), Summary("Kicks the user from the server")>
    <Group(Group.UserManagement), Rank(Rank.Moderator)>
    Public Sub CmdKick(target As User, <Remainder> reason As String)
        If target.Rank < Context.User.Rank Then
            If Context.User.Rank = Rank.Moderator Then
                ' Mods can only kick if someone has reported
                If target.Reports <= 0 Then
                    Reply("Error: you cannot kick that user")
                    Return
                End If
            End If
            SendMessageForce(target.Client, $"&CLOSE&You were kicked by {Context.User.Name}{vbCrLf}Reason: {reason}")
            SendAChat($"{target.Name}({target.ActualName}) kicked by {Context.User.Name} for {reason}", "Server", "Server")
            RemoveUser(target.Name, target.Client)
            If target.Name = painter Then
                Broadcast("ServerWON")
            End If
            Reply("Target was kicked")
        Else
            Reply("Error: you cannot kick that user")
        End If
    End Sub

    <Name("ban"), Summary("Bans the user from the server, permenantly")>
    <Group(Group.UserManagement), Rank(Rank.Admin)>
    Public Sub CmdBan(target As User, <Remainder> reason As String)
        If target.Rank < Context.User.Rank Then
            BanUser(target, Context.User.Name, reason)
            Reply("User banned")
        Else
            Reply("Error: invalid permissions to ban user")
        End If
    End Sub

#End Region

#Region "Hash Commands"

    <Name("allow"), Summary("Adds a hash to be allowed for connection")>
    <Rank(Rank.Server), Group(Group.Checksums)>
    Public Sub AllowHashToConnect(Optional hash As String = "")
        If hash = "" Then
            hash = LastJoinChecksum
        End If
        If hash.Length <> 32 Then
            Reply("Error: hash must be 32-characters in length")
            Return
        End If
        If AllowChecksums.Contains(hash) = False Then
            AllowChecksums.Add(hash)
            Reply("Checksum added")
        Else
            Reply("Checksum was alreadu present")
        End If
    End Sub

    <Name("list"), Summary("Lists all hashses allowed")>
    <Rank(Rank.Manager), Group(Group.Checksums)>
    Public Sub ListHashesAllowed()
        Reply("Hashes:" + String.Join(vbCrLf, AllowChecksums))
    End Sub

    <Name("clear"), Summary("Clears the list of all allowed hashes")>
    <Rank(Rank.Server), Group(Group.Checksums)>
    Public Sub ClearAllowedHashes()
        AllowChecksums.Clear()
        Reply("Hashes cleared")
    End Sub

    <Name("save"), Summary("Saves the list of hashes to file")>
    <Rank(Rank.Server), Group(Group.Checksums)>
    Public Sub SaveHashes()
        File.WriteAllText("allowedHash.txt", String.Join(vbCrLf, AllowChecksums))
        Reply("Checksums saved to file")
    End Sub

    <Name("removelatest"), Summary("Removes the checksum of the version on bitbucket")>
    <Rank(Rank.Server), Group(Group.Checksums)>
    Public Sub RemoveBitbuckethash()
        If Latest_Client_Checksum Is Nothing Then
            Reply("Latest bitbucket version either was never added or has already been removed")
        Else
            Try
                AllowChecksums.Remove(Latest_Client_Checksum)
                Latest_Client_Checksum = Nothing
                Reply("Latest bitbucket version removed")
            Catch ex As Exception
            End Try
        End If
    End Sub

#End Region

#Region "Password Commands"
    <Name("pw_see"), Summary("Prints  the current password, if one is set")>
    <Group(Group.Password), Rank(Rank.Moderator)>
    Public Sub SeeCurrentPassword()
        If IsPasswordProtected Then
            Reply("Server password: " + ServerPassword)
        Else
            Reply("There is no current password." + vbCrLf + "Use /pw_set to set one")
        End If
    End Sub

    <Name("pw_set"), Summary("Sets the server password")>
    <Group(Group.Password), Rank(Rank.Manager)>
    Public Sub SetServerPassword(<Remainder> Optional password As String = "")
        ServerPassword = password
        MasterList.Update(SERVER_NAME, Users.Count, IsPasswordProtected)
        Reply("Password set to " + ServerPassword)
    End Sub
#End Region

#Region "Game-related Commands"
    <Name("autostart"), Summary("Sets or toggles whether server autostarts")>
    <Rank(Rank.Admin), Group(Group.GameRelated)>
    Public Sub ChangeAutoStart(value As Boolean)
        autoStart = value
        Reply("Autostart is now set to: " + autoStart.ToString())
    End Sub

    <Name("forcestart"), Summary("Starts a drawing game")>
    <Rank(Rank.Manager), Group(Group.GameRelated)>
    Public Sub ForceGameToStart()
        If isGamePlaying Then
            Reply("Game is already in progress")
            Return
        End If
        startAgame()
    End Sub

    <Name("forceend"), Summary("Forces the current game to stop")>
    <Rank(Rank.Manager), Group(Group.GameRelated)>
    Public Sub ForceGameToEnd()
        If Not isGamePlaying Then
            Reply("There is no current game")
        Else
            isGamePlaying = False
            CurrentGame = Nothing
            Broadcast("ServerWON")
        End If
    End Sub

    <Name("gameinfo"), Summary("Prints info related to current game")>
    <Rank(Rank.Server), Group(Group.GameRelated)>
    Public Sub GetGameInfo()
        If Not isGamePlaying Then
            Reply("There is no current game")
        Else
            Dim _items As New List(Of String)
            _items.Add(CurrentGame.ThingToDraw.Main)
            _items.AddRange(CurrentGame.ThingToDraw.Aliases)
            Reply($"Painter: {CurrentGame.Painter}{vbCrLf}Painter sees: {CurrentGame.ThingToDraw.ToDraw}{vbCrLf}Full allowed: {String.Join(vbCrLf, _items)}")
        End If
    End Sub

#End Region
End Class



