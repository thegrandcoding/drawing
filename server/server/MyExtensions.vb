﻿Imports System.Runtime.CompilerServices
Imports System.Text
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Module MyExtensions
    <Extension()>
    Public Function GetValueAt(ByVal thing As JObject, s As String) As String
        ' : seperated path
        Dim errorConfig As Object = JObject.Parse(Encoding.ASCII.GetString(My.Resources.lang))
        Dim path As String() = s.Split(":")
        Dim _path As New List(Of String)
        _path.AddRange(path)
        Try
            For Each item As String In _path
                errorConfig = errorConfig(item)
            Next
            Dim val As JValue = JValue.FromObject(errorConfig)
            Try
                If val.Values(Of String).Count = 1 Then
                    Return val.Value.ToString()
                Else
                    Throw New ArgumentException("Given path " & s & " value is not a string")
                End If
            Catch ex As InvalidOperationException
                Return val.Value.ToString()
            End Try
        Catch ex As Exception
            LogMsg("Error: Value unable: " + ex.ToString(), "GetValueAt-extension")
            Return ""
        End Try
    End Function
End Module
