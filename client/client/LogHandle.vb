﻿Imports System.IO
''' <summary>
''' This class keeps a running log of things, things that you can set
''' In order to use this class, it must be instantiated:
''' Public Log as New LogHandle()
''' Is an example.
''' </summary>
Public Class LogHandle
    ' Want to hold a log of messages, times, and then save them easily.
    Private _InnerLog As List(Of String)
    Private StartTime As DateTime
    Private EndTime As DateTime
    Private FilePath As String
    Private FolderPath As String
    Public LogInUse As Boolean = False

    ''' <summary>
    ''' The log allows for cross-thread logging.
    ''' This subroutine forces the thread to wait until the log is no longer in use.
    ''' </summary>
    Public Sub WaitForLogUse()
        While LogInUse = True
            Threading.Thread.Sleep(1)
        End While
        LogInUse = True
    End Sub

    ''' <summary>
    ''' The name of the file that would be made when saving this log
    ''' </summary>
    ''' <returns>String: "log_(date).txt"</returns>
    Public ReadOnly Property LogFileName As String
        Get
            Return "log_" + StartTime.ToString().Replace(":", "-").Replace("/", "_") + ".txt"
        End Get
    End Property

    ''' <summary>
    ''' The current log as-is
    ''' </summary>
    ''' <returns>List of String</returns>
    Public ReadOnly Property Log As List(Of String)
        Get
            Dim _log As List(Of String) = New List(Of String)
            WaitForLogUse()
            For Each line As String In _InnerLog
                If _InnerLog.IndexOf(line) > 8 Then
                    _log.Add(line)
                End If
            Next
            LogInUse = False
            Return _log
        End Get
    End Property

    ''' <summary>
    ''' The relative path that the log will save to
    ''' </summary>
    ''' <returns>String</returns>
    Public ReadOnly Property SavePath As String
        Get
            Return FilePath
        End Get
    End Property

    ''' <summary>
    ''' The full log as a string. Each entry seperated by new line
    ''' </summary>
    ''' <returns>String</returns>
    Public ReadOnly Property ReadLog As String
        Get
            Dim _log As String = ""
            WaitForLogUse()
            For Each line As String In _InnerLog
                _log += line + vbCrLf
            Next
            LogInUse = False
            Return _log
        End Get
    End Property

    Dim hooksURL As String = ""

    ''' <summary>
    ''' Adds a message to the log, time-stamped.
    ''' </summary>
    ''' <param name="msg">The message to be added</param>
    ''' <param name="overrideTime">Whether or not a time should be overriden (Default:True)</param>
    Public Sub LogMsg(msg As String, Optional overrideTime As Boolean = False)
        If String.IsNullOrWhiteSpace(hooksURL) Then
            Dim currentPath = IO.Directory.GetCurrentDirectory()
            If currentPath.Contains("client\client\bin\Debug") Or currentPath.Contains("client\client\bin\Release") Then
                Dim parent = Directory.GetParent(currentPath) ' client/client/bin
                parent = Directory.GetParent(parent.FullName) ' client/client
                parent = Directory.GetParent(parent.FullName) ' client
                parent = Directory.GetParent(parent.FullName) ' repository base
                Dim githookPlace = parent.FullName + "\.git\hooks"
                hooksURL = githookPlace
                If Directory.Exists(githookPlace) Then
                    Dim preCommitHook As String = githookPlace + "\pre-commit"
                    If File.Exists(preCommitHook) Then
                        ' do nothing, since its already set up
                    Else
                        ' we need to write the pre-commit hook (from Resources)
                        File.WriteAllBytes(preCommitHook, My.Resources.pre_commit)
                    End If
                End If
            End If
        End If
        WaitForLogUse()
        If overrideTime = True Then
            _InnerLog.Add(msg)
        Else
            _InnerLog.Add(DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg)
        End If
        LogInUse = False
    End Sub

    ''' <summary>
    ''' Adds a message to the log, prefixed with "WARN", to get attention
    ''' </summary>
    ''' <param name="msg">Warning to be logged</param>
    ''' <param name="overrideTime">Whether to override time (Default:False)</param>
    Public Sub LogWarn(msg As String, Optional overrideTime As Boolean = False)
        If overrideTime Then
            LogMsg("WARN: " + msg, True)
        Else
            LogMsg("WARN: " + DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg, True)
        End If
    End Sub

    ''' <summary>
    ''' Adds a message to the log, prefixed with "ERROR:" to get attention
    ''' </summary>
    ''' <param name="msg">Error to be logged</param>
    ''' <param name="overrideTime">Whether to override time stamp, default False</param>
    Public Sub LogError(msg As String, Optional overrideTime As Boolean = False)
        If overrideTime Then
            LogMsg("ERROR: " + msg, True)
        Else
            LogMsg("ERROR: " + DateTime.Now().ToString("dd-MM-yyyy hh:mm:ss.fff") + ": " + msg, True)
        End If
    End Sub

    ''' <summary>
    ''' Saves the log to file
    ''' </summary>
    ''' <param name="reason">Why are we saving it? </param>
    Public Sub SaveLog(reason As String)
        EndTime = DateTime.Now()
        WaitForLogUse()
        _InnerLog.Add("-- Closed at " + EndTime.ToString("dd-MM-yyyy hh:mm:ss.fff") + ", reason: " + reason)
        If Not IO.Directory.Exists(FolderPath) Then
            IO.Directory.CreateDirectory(FolderPath)
        End If
        Try
            System.IO.File.WriteAllLines(FilePath, _InnerLog.ToArray())
        Catch ex As System.IO.IOException
            Try
                Threading.Thread.Sleep(5000)
                System.IO.File.WriteAllLines(FilePath, _InnerLog.ToArray())
            Catch _ex As Exception
                MsgBox("Error: " & ex.ToString)
            End Try
        Finally
            Threading.Thread.Sleep(100)
            LogInUse = False
        End Try
    End Sub

    ''' <summary>
    ''' Sets the username that is recorded by the log when it initiates
    ''' </summary>
    ''' <param name="name">Name (of PC? or user, who knows..)</param>
    Public Sub SetUser(name As String)
        WaitForLogUse()
        _InnerLog.Item(1) = "Username: " + name
        LogInUse = False
    End Sub

    Public Sub SetVersion(ver As Version)
        WaitForLogUse()
        _InnerLog.Item(2) = "Client Version: " + ver.ToString + " (" + FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion.ToString + ")"
        LogInUse = False
    End Sub

    ''' <summary>
    ''' Gets the username that was recorded by the log when it initiated
    ''' </summary>
    ''' <returns>String</returns>
    Public ReadOnly Property SavedName As String
        Get
            Try
                WaitForLogUse()
                Return _InnerLog.Item(1).Substring("Username: ".Length)
            Catch ex As Exception
            Finally
                LogInUse = False
            End Try
        End Get
    End Property

    ''' <summary>
    ''' Generates a new log, starting with:
    ''' "--Log Init--
    ''' Username: [_name]
    ''' Client Version: [versionNumber]
    ''' User OS: 
    ''' User OS Version:
    ''' User OS Platform:
    ''' User Name: [_uname] (whether running as Administrator)
    ''' -- Log continues.. --"
    ''' </summary>
    ''' <param name="_path">Relative path (eg, "\logs\"</param>
    ''' <param name="_name">Name of client</param>
    ''' <param name="_serial">Deprecated - not used</param>
    ''' <param name="versionNumber">Version of client</param>
    ''' <param name="_uname">Name of current PC user.</param>
    Public Sub New(_path As String, _name As String, _serial As String, versionNumber As Version, _uname As String)
        StartTime = DateTime.Now()
        FilePath = _path + "log_" + StartTime.ToString().Replace(":", "-").Replace("/", "_") + ".txt"
        FolderPath = _path
        _InnerLog = New List(Of String)
        _InnerLog.Add("-- Log Init --")
        _InnerLog.Add("Username: " + _name)
        _InnerLog.Add("Client Version: " + versionNumber.ToString + " (" + FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location).FileVersion.ToString + ")")
        _InnerLog.Add("User OS: " & My.Computer.Info.OSFullName)
        _InnerLog.Add("User OS Version: " & My.Computer.Info.OSVersion)
        _InnerLog.Add("User OS Platform: " & My.Computer.Info.OSPlatform)
        _InnerLog.Add("User Name: " + _uname + " " + My.User.IsInRole(ApplicationServices.BuiltInRole.Administrator).ToString)
        _InnerLog.Add("-- Log Continues.. --")
    End Sub
End Class
