﻿Public Class User 'a struct would be better (but i dont know how to use it lol)
    Public Name As String
    Public Score As Integer
    Public Rank As Integer
    Public ActualName As String
    Public ReadOnly Property IsMod As Boolean
        Get
            Return Rank > 0
        End Get
    End Property
    Public ReadOnly Property IsAdmin As Boolean
        Get
            Return Rank > 1
        End Get
    End Property
    Public ReadOnly Property IsManager As Boolean
        Get
            Return Rank > 2
        End Get
    End Property
    Public Sub New(nm As String, sc As Integer, rnk As Integer, acNm As String)
        Name = nm
        Score = sc
        Rank = rnk
        ActualName = acNm
    End Sub
End Class
