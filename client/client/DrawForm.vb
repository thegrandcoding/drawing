﻿Imports System.ComponentModel
Imports System.Net
Imports System.Net.Sockets
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Cryptography
Imports System.Text
Imports System.Linq
Imports Microsoft.Win32
Imports MasterlistDLL
Imports DiscordRPC
Imports System.IO

#Disable Warning IDE1006 ' Naming Styles


Public Class DrawForm
    ' So, you wanted me to comment the code..
    Public Const DisableMasterList As Boolean = False

    Dim clientSocket As New System.Net.Sockets.TcpClient()
    Dim serverStream As NetworkStream
    Dim readData As String 'the stuff given from the broadcast sub in the server
    Dim infiniteCounter As Integer
    Dim connectedClients As String() ' names of clients connected
    Dim pencolour As Color = Color.Black
    Dim painter As Boolean = True
    Dim thingToDraw As String = ""
    Dim pts(2) As Point
    Dim timeLeft As Integer = 50
    Dim gameStartTimeStamp As Long ' holds the UNIX-timestamp of the start of the game
    Dim pensize As Integer = 1
    Dim changepensize As Integer = 1
    Dim votesToStart As Integer
    Dim whoIsDrawing As String
    Dim whoWon As String
    Dim pt1 As Point = Nothing
    Dim pt2 As Point = Nothing
    Dim DrawingStraightLineTool As Boolean = False ' if true, then it means the player has clicked on the button
    Dim DrawingRectangleTool As Boolean = False
    Dim DrawingCircleTool As Boolean = False
    Dim DiscordClient As DiscordRpcClient

    Dim ServerUniqueID As String = "N/A"

    Public Sub DISCORD_RELOAD()
        If ServerUniqueID <> "N/A" Then
            GamePresence.Secrets = New Secrets() With {
        .JoinSecret = ServerUniqueID
        }
            GamePresence.Party = New Party() With {
            .ID = "thisDoesNotReallyMatter",
            .Max = Users.Keys.Count,
            .Size = 0
            }
        End If
        DiscordClient.SetPresence(GamePresence)
    End Sub


    Public Users As New Dictionary(Of String, User)
    Public FinalTextBound As String = "Draw!" ' Me.Text at start (so it can be added to easily)
    ' Versions and Stuff'
    Dim LatestVersion As Version = New Version("0.0.0.0")
    Dim ThisThingVersion As New Version("0.0.0.0")
    Public _Log As LogHandle

    Public Serial As String = "None"
    Public MainReg As String = "HKEY_CURRENT_USER\Liliana_Drawing" ' where registry things are saved

    Private Rank As Integer = 0 ' current users rank
    Private EveryoneCanVote As Boolean = False
    Private WhoCanDraw As String = "Everyone" ' readonly; change has no affect serverside
    Public Bans As New List(Of Ban) ' bans for the BanForm
    Public Structure Ban
        Public Name As String
        Public ActualName As String
        Public IP As IPAddress
        Public Serial As String
        Public IP_Ban As Boolean
        Public Serial_Ban As Boolean
        Public Name_Ban As Boolean
    End Structure
    Public MasterList As New Dictionary(Of String, ClientDisplayServer)
    Public ReadOnly Property TotalPlayers As Integer
        Get
            Dim val As Integer = 0
            For Each serv As ClientDisplayServer In MasterList.Values
                Dim servInt As Integer = 0
                Integer.TryParse(serv.PlayersOnline, servInt)
                ' tryparse will return the value to the servInt, if integer
                ' of course, if its not an int, then it just adds 0
                val += servInt
            Next
            Return val
        End Get
    End Property
    Public Property TotalLeaderboardScores As Integer
    Private TestingServerConnectionDotTxt As ClientDisplayServer = Nothing
    Private Sub RefreshMasterlistDGV()
        ' Updates/sets the list of servers
        If Me.InvokeRequired Then
            Me.Invoke(Sub() RefreshMasterlistDGV())
            Return
        End If
        dgv_servers.Rows.Clear()
        For Each server As ClientDisplayServer In MasterList.Values
            Dim row As String() = New String() {$"{server.ServerName}{If(server.IsPasswordProtected, " (PASSWORD)", "")}", server.PlayersOnline, "Connect"}
            dgv_servers.Rows.Add(row)
        Next
        lblMasterListInfo.Text = $"There are {TotalPlayers.ToString()} players connected in {MasterList.Count.ToString()} servers, with a total leaderboard score of {TotalLeaderboardScores.ToString()}."
    End Sub
    Private Sub SetMstlblLabel(str As String)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetMstlblLabel(str))
            Return
        End If
        lblMasterListInfo.Text = str
    End Sub
    Private Sub SetMasterList()
        If DisableMasterList Then Return
        ' I think this is self-explainetory.
        ' Basically, it asks the masterlist for the servers online, and the top-score leaderboard.
        SetMstlblLabel("Fetching masterlist..")
        Dim servers() As ClientDisplayServer = Nothing
        Try
            servers = MasterlistDLL.MasterListClient.GetServers("drawing")
        Catch ex As Exception
            SetMstlblLabel("Masterlist Offline")
            Return
        End Try
        MasterList.Clear()
        For Each serv As ClientDisplayServer In servers
            MasterList.Add(serv.ServerName, serv)
        Next
        ' Dim leaderboard() As MasterlistDLL.ClientDisplayLeaderBoard = MasterlistDLL.MasterListClient.GetLeaderBoard("drawing")
        TotalLeaderboardScores = 0
        For Each lb As ClientDisplayLeaderBoard In MasterlistDLL.MasterListClient.GetLeaderBoard("drawing")
            TotalLeaderboardScores += lb.Score
        Next
        If TestingServerConnectionDotTxt IsNot Nothing Then MasterList.Add(TestingServerConnectionDotTxt.ServerName, TestingServerConnectionDotTxt)
        ' Adds the server specified in bitbucket/drawing/server/Connection.txt
        RefreshMasterlistDGV()
    End Sub
    Public ReadOnly Property IsModerator As Boolean
        Get
            Return Rank > 0
        End Get
    End Property
    Public ReadOnly Property IsAdmin As Boolean
        Get
            Return Rank > 1
        End Get
    End Property
    Public ReadOnly Property IsManager As Boolean
        Get
            Return Rank > 2
        End Get
    End Property
    Public ReadOnly Property CanVoteToStart As Boolean
        Get
            If EveryoneCanVote Then
                btnVoteToStart.Text = "Vote to start"
                Return True
            Else ' Else, only admins can vote.
#If DEBUG Then
                btnVoteToStart.Text = "Vote to start"
#Else
                If Not IsModerator Then btnVoteToStart.Text = "Disabled."
#End If
                Return IsModerator
            End If
        End Get
    End Property

    '-----------------------------DRAWING---------------------------'
    Private Sub SendShapeInfo(shape As String) 'draws a rectangle area where the shape would be drawn
        Dim topLeft = New Point(startPoint)
        Dim bottomRight = New Point(pt2)
        Dim size = New Size(bottomRight.X - topLeft.X, bottomRight.Y - topLeft.Y)
        Dim rec As New Rectangle(topLeft, New Size(size))
        Dim shapeStr = "shape" + shape + ":" + topLeft.X.ToString() + "." + topLeft.Y.ToString() + "." + size.Width.ToString() + "." + size.Height.ToString()
        Send(shapeStr) 'will be x.y.width.height, ie Rectangle constructors (rectangle is used in making an ellipse too)
    End Sub

    Private mouseIsDown As Boolean = False
    Private Sub pCanvas_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pCanvas.MouseDown
        mouseIsDown = True
        If DrawingStraightLineTool Or DrawingRectangleTool Or DrawingCircleTool Then
            If startPoint = Nothing OrElse startPoint.IsEmpty() Then 'first click
                startPoint = New Point(e.X, e.Y)
            Else 'second click
                pt1 = New Point(startPoint)
                pt2 = New Point(e.X, e.Y)
                priorCanvasImage = Nothing
                DrawingStraightLineTool = False
                If DrawingRectangleTool Then
                    SendShapeInfo("RECTANGLE")
                ElseIf DrawingCircleTool Then
                    SendShapeInfo("CIRCLE")
                Else
                    Send("POINT:" + pt1.X.ToString() + "," + pt1.Y.ToString() + "-" + pt2.X.ToString() + "," + pt2.Y.ToString())
                End If
                startPoint = Nothing
                DrawingRectangleTool = False
                DrawingCircleTool = False
                switchToolBgColours(btnRectangleTool, Color.White) 'turn them all white
            End If
        End If
    End Sub

    Private Sub pCanvas_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pCanvas.MouseUp
        mouseIsDown = False
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        If painter Then
            pCanvas.Refresh()
            Send("CLEAR")
        End If
    End Sub

    Dim priorCanvasImage As Bitmap = Nothing
    Dim startPoint As Point = Nothing

    Dim CanvasImage As Bitmap = Nothing
    Private Sub pCanvas_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pCanvas.MouseMove
        If painter Then
            pt2 = New Point(e.X, e.Y)
            If pt1 = Nothing Then pt1 = pt2
            If mouseIsDown = True Then
                If DrawingRectangleTool Or DrawingStraightLineTool Or DrawingCircleTool Then pt1 = New Point(startPoint)
                pt2 = New Point(e.X, e.Y)
                priorCanvasImage = Nothing
                DrawingStraightLineTool = False
                If DrawingRectangleTool Then
                    SendShapeInfo("RECTANGLE")
                ElseIf DrawingCircleTool Then
                    SendShapeInfo("CIRCLE")
                Else
                    Send("POINT:" + pt1.X.ToString() + "," + pt1.Y.ToString() + "-" + pt2.X.ToString() + "," + pt2.Y.ToString())
                End If
                DrawingRectangleTool = False
                DrawingCircleTool = False
                startPoint = Nothing
                switchToolBgColours(btnRectangleTool, Color.White) 'turn them all off
            Else
                ' These work as essentially:
                ' We take a screenshot of the canvas BEFORE we start to preview the line/rectangle/circle.
                ' Each time they move, we draw the prior image, then draw the line/rectangle/circle
                ' This means it essentially gets constantly updates as they move the mouse.
                ' The initial click will set up the prior image, and the second click will draw the points.
                If DrawingStraightLineTool = True AndAlso startPoint <> Nothing Then
                    If priorCanvasImage Is Nothing Then
                        priorCanvasImage = New Bitmap(CanvasImage)
                    End If
                    Using g As Graphics = Graphics.FromImage(CanvasImage)
                        g.DrawImage(priorCanvasImage, New Point(0, 0))
                        g.DrawLine(New Pen(pencolour, pensize), startPoint, pt2)
                    End Using
                    Using g As Graphics = pCanvas.CreateGraphics()
                        g.DrawImage(CanvasImage, 0, 0)
                    End Using
                End If
                If (DrawingRectangleTool Or DrawingCircleTool) AndAlso startPoint <> Nothing Then
                    If priorCanvasImage Is Nothing Then
                        priorCanvasImage = New Bitmap(CanvasImage)
                    End If
                    Using g As Graphics = Graphics.FromImage(CanvasImage)
                        g.DrawImage(priorCanvasImage, 0, 0)
                        Dim topLeft = New Point(startPoint)
                        Dim bottomRight = New Point(pt2)
                        Dim size = New Size(bottomRight.X - topLeft.X, bottomRight.Y - topLeft.Y)
                        Dim rec As New Rectangle(topLeft, size)
                        If DrawingRectangleTool Then g.DrawRectangle(New Pen(pencolour, pensize), rec)
                        If DrawingCircleTool Then g.DrawEllipse(New Pen(pencolour, pensize), rec)
                    End Using
                    Using g As Graphics = pCanvas.CreateGraphics()
                        g.DrawImage(CanvasImage, 0, 0)
                    End Using
                End If
            End If
            pt1 = New Point(pt2)
        End If
    End Sub
    '----------------------------------------------------------------'

    Public Sub MessageAChat(msg As String)
        ' ... sends a message to the admin chat
        Send(msg + "&ADMINCHAT&")
    End Sub


    Private _getIPAddress As Net.IPAddress = Nothing
    Function getIPaddress() As Net.IPAddress
        ' Gets the first IPv4 ip address on the computer (internal network ip)
        If _getIPAddress IsNot Nothing Then Return _getIPAddress
        Dim ipaddress As Net.IPAddress
        Dim strhostname As String = System.Net.Dns.GetHostName()
        Dim iphe As System.Net.IPHostEntry = Net.Dns.GetHostEntry(strhostname)

        For Each ipheal As Net.IPAddress In iphe.AddressList
            If ipheal.AddressFamily = System.Net.Sockets.AddressFamily.InterNetwork Then
                ipaddress = ipheal
                Exit For
            End If
        Next
        _getIPAddress = ipaddress
        Return ipaddress
    End Function
    Private Sub connect(Optional tryAsAdmin As Boolean = False) 'ByVal uname As String)
        ' Attempts to connect to the server
        Dim conIP = IPAddress.Parse(txtIpAddress.Text)
        clientSocket = New TcpClient(conIP.AddressFamily)
        If conIP.AddressFamily = AddressFamily.InterNetworkV6 Then
            clientSocket.Client.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.IPv6Only, False)
        End If
        'DiscordDLL.DiscordApp.UpdatePres("Connecting..", "Connecting to " & conIP.ToString(), 0, 0, DiscordDLL.DiscordApp.GameState.Lobby)
        'DiscordDLL.DiscordApp.UpdatePres(New DiscordDLL.DiscordApp.GameStatusInfo(txtName.Text, 0))
        If tryAsAdmin Then txtName.Text.Replace("&ADMIN&", String.Empty)
        Try
            _Log.LogMsg("Trying connect to " & txtIpAddress.Text.ToLower + ":8888; name: " & txtName.Text)
            _Log.SetUser(txtName.Text)
            If clientSocket.ConnectAsync(conIP, 8888).Wait(2000) = False Then
                ' Connection failed after some time.
                Throw New Exception("Failed to connect; perhaps the server is offline.")
            End If
            serverStream = clientSocket.GetStream()
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Liliana_Drawing" + "\Client", "DefaultIP", conIP.ToString())
            ' Set ip attempt so next startup will grab it
            My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Liliana_Drawing" + "\Client", "DefaultName", txtName.Text)
            ' Same as above
            Dim connectionString As New List(Of String) From {"$$$" & txtName.Text & If(tryAsAdmin Or TryAdminAlways, "&ADMIN&", "")}
            connectionString.Add(Serial)
            connectionString.Add(Environment.UserName.ToLower())
            connectionString.Add(CLIENT_EXEC_CHECKSUM)
            If Not String.IsNullOrWhiteSpace(txtPassword.Text) Then
                connectionString.Add(txtPassword.Text)
            End If
            connectionString.Add("$$$")
            Send(String.Join(";", connectionString))
            timerFlashWhenMsg.Start()
            Dim ctThread As Threading.Thread = New Threading.Thread(AddressOf getMessage)
            ctThread.Start()
            timerFlashWhenMsg.Enabled = True
            allowNewGame()
            GamePresence = New RichPresence() With {
            .Details = "In Lobby",
            .State = "Wating for game"
            }
            DISCORD_RELOAD()
        Catch ex As Exception
            _Log.LogError(ex.ToString())
            MsgBox("No connection could be made. Try a new IP address: the server displays it." + vbCrLf + "-OR- Try again later.")
        End Try
    End Sub

    Public WeHaveDisplayedERROR As Boolean = False ' prevents spamming of exceptions to the user

    Private Sub Send(msg As String, Optional DontSendMsgBoxError As Boolean = False) 'sends a message to the server
        If String.IsNullOrWhiteSpace(msg) Then Return
        If Not msg.Substring(0, 1) = "%" Then msg = "%" + msg
        ' % is used to differentiate between messages (since TCP is a stream of constant messages)
        Try
            msg = msg + "`" ' ` indicates the end of the message
            _Log.LogMsg("Send: " & msg)
            Dim outStream As Byte() = System.Text.Encoding.UTF8.GetBytes(msg)
            serverStream.Write(outStream, 0, outStream.Length)
            serverStream.Flush()
        Catch ex As Exception
            If WeHaveDisplayedERROR Then Return
            WeHaveDisplayedERROR = True
            timerFlashWhenMsg.Enabled = False
            _Log.LogError(ex.ToString())
            If DontSendMsgBoxError = False Then MsgBox("the server has probably closed :((")
            Me.Close()
        End Try
    End Sub

    Private Sub getMessage() 'gets the broadcasted stuff
        Try
            For infiniteCounter = 1 To 2
                infiniteCounter = 1
                serverStream = clientSocket.GetStream()
                Dim buffSize As Integer
                Dim inStream(65535) As Byte
                buffSize = clientSocket.ReceiveBufferSize
                serverStream.Read(inStream, 0, buffSize)
                Dim returndata As String = System.Text.Encoding.UTF8.GetString(inStream) 'the stuff that is broadcasted
                readData = "" + returndata
                If Not readData.Contains("&CLOSE&") AndAlso panelConnect.Visible = True Then
                    hideConnectPanel()
                    timerDiscordUpdate.Start()
                    timerDiscordUpdate.Interval = 2000
                End If
                readData = readData.Replace(vbNullChar, String.Empty)
                For Each msg In readData.Split("%") 'in case multiple msgs get bunched up
                    msg.Replace("%", String.Empty)
                    If String.IsNullOrWhiteSpace(msg) Then Continue For
                    Try
                        msg = msg.Substring(0, msg.IndexOf("`"))
                    Catch ex As ArgumentOutOfRangeException
                        _Log.LogError("Message was invalid: " & msg.Replace(vbNullChar, String.Empty))
                        Continue For
                    End Try
                    _Log.LogMsg("FromServer: " & msg)
                    If msg.StartsWith("GUID:") Then
                        ' Server unique ident
                        ServerUniqueID = msg.Replace("GUID:", "")
                        DISCORD_RELOAD()
                        Continue For
                    End If
                    If msg.Contains("&LOBBY&") Then
                        msg = msg.Replace("&LOBBY&", String.Empty)
                        Dim split As String() = msg.Split(";")
                        Dim uname As String = split(0)
                        Dim rank As String = split(1)
                        Dim message As String = split(2)
                        Dim clr As Color = Color.Black
                        If rank = "Admin" Or rank = "Manager" Then
                            If message.Substring(0, 1) = "/" Then
                                If message.Contains("a-o") Then
                                    clr = Color.Orange
                                ElseIf message.Contains("a-r") Then
                                    clr = Color.Red
                                End If
                            End If
                        End If
                        If uname = "Server" Then
                            If rank = "Autostart" Or rank = "Status" Then
                                clr = Color.DarkCyan
                            Else
                                clr = Color.Red
                            End If
                            uname = "Client"
                        End If
                        AddLobbyMessage(uname, message, clr)
                    ElseIf msg.Contains("GUESS:") Then
                        updateGuessHistory(Replace(msg, "GUESS:", String.Empty, , 1)) 'only replace the first instance
                    ElseIf msg.Contains("diffVersions:") Then
                        msg = msg.Replace("diffVersions:", String.Empty)
                        Dim diffiCulties As String() = msg.Split(";")
                        SetDifficulties(diffiCulties)
                    ElseIf msg.Contains("CLIENT") Then
                        'either adding or removing
                        If msg.Contains("ADD") Then
                            Dim client As String = msg.Substring(0, msg.IndexOf("ADDCLIENT")).Replace("%", String.Empty)
                            Dim clSplit As String() = client.Split(";")
                            Users.Add(clSplit(0), New User(clSplit(0), clSplit(1), Integer.Parse(clSplit(2)), clSplit(3)))
                        ElseIf msg.Contains("REMOVE") Then
                            Dim client As String = msg.Substring(0, msg.IndexOf("REMOVECLIENT"))
                            If client = whoIsDrawing Then
                                Send("PAINTERLEFT")
                                InLobby = True
                                allowNewGame()
                                timerTimeLeft.Enabled = False
                                updateLabels("The word was: " & thingToDraw, "The painter left!")
                            End If
                            Users.Remove(client)
                        ElseIf msg.Contains("ALLCLIENTS") Then
                            Users.Clear() ' also use this to reset.
                            For Each user In msg.Substring(0, msg.IndexOf("ALLCLIENTS")).Split(".")
                                Dim clSplit As String() = user.Split(";")
                                Users.Add(clSplit(0), New User(clSplit(0), clSplit(1), Integer.Parse(clSplit(2)), clSplit(3).Replace("_", " ")))
                            Next
                        End If
                        updateClientLists()
                        updateSettingsButton()
                    ElseIf msg.Contains("ALLBANS") Then
                        Dim message As String = msg.Substring(0, msg.IndexOf("ALLBANS"))
                        Bans.Clear()
                        For Each usr As String In message.Split(";")
                            If String.IsNullOrWhiteSpace(usr) Then Continue For
                            Dim newBan As New Ban()
                            Dim usrSplit As String() = usr.Split(",")
                            newBan.Name = usrSplit(0)
                            newBan.IP = IPAddress.Parse(usrSplit(1))
                            newBan.Serial = usrSplit(2)
                            newBan.IP_Ban = Boolean.Parse(usrSplit(3))
                            newBan.Serial_Ban = Boolean.Parse(usrSplit(4))
                            newBan.ActualName = usrSplit(5).Replace("_", " ")
                            newBan.Name_Ban = True
                            Bans.Add(newBan)
                        Next
                        updateBannedView()
                        BannedForm.UpdateView()
                    ElseIf msg.Contains("THINGTODRAW") Then
                        'means that a game has started
                        InLobby = False
                        Dim drawStatus As String() = msg.Replace("THINGTODRAW:", String.Empty).Split("/")
                        ' GUESSER/WHO
                        ' PAINTER/WHO/ITEM
                        whoIsDrawing = drawStatus(1)
                        If drawStatus(0).Contains("PAINTER") Then
                            thingToDraw = drawStatus(2)
                            painter = True
                            updateLabels("You are drawing: " + thingToDraw, "You are drawing")
                        Else 'guesser
                            painter = False
                            updateLabels("What is being drawn? Guess!", "You are a guesser.")
                        End If
                        readData = "CLEAR"
                        updateGuessHistory("") '(clear it)
                        drawMsg()
                        beginGame()
                    ElseIf msg.Contains("&CLOSE&") Then
                        timerFlashWhenMsg.Enabled = False
                        msg = msg.Replace("&CLOSE&", String.Empty)
                        msg = msg.Replace("$$$", String.Empty)
                        MsgBox(msg)
                        showConnectPanel()
                    ElseIf msg.Contains("&MESSAGE&") Then
                        msg = msg.Replace("&MESSAGE&", String.Empty)
                        msg = msg.Replace("$$$", String.Empty)
                        MsgBox(msg)
                    ElseIf msg.Contains("&ADMINCHAT&") Then
                        Dim rec As String = msg.Replace("&ADMINCHAT&", String.Empty)
                        Dim recSplit As String() = rec.Split(";")
                        Dim usr As String = recSplit(0)
                        Dim rnk As String = recSplit(1)
                        Dim mesg As String = recSplit(2)
                        Dim clr As Color = Nothing
                        If rnk = "Moderator" Then
                            clr = Color.Orange
                        ElseIf rnk = "Admin" Then
                            clr = Color.HotPink
                        Else
                            clr = Color.DarkCyan
                        End If
                        If usr = "Server" Then
                            If rnk = "Update" Then
                                clr = Color.FromArgb(25, 25, 112)
                            ElseIf rnk = "PMs" Then
                                clr = Color.FromArgb(178, 89, 0)
                            Else
                                clr = Color.Red
                            End If
                        End If
                        AddPlayerMessage(mesg, usr, rnk, clr, usr = "Server")
                    ElseIf msg.Contains("WON") Then
                        allowNewGame()
                        timerTimeLeft.Enabled = False
                        painter = False
                        whoWon = msg.Substring(0, msg.IndexOf("WON"))
                        If Not whoWon = "Server" Then
                            updateLabels("Game over!", whoWon & " guessed correctly!")
                            Users(whoIsDrawing).Score += 1
                            Users(whoWon).Score += 1 ' dont award points for manual closure
                        Else
                            updateLabels("Game over!", "The game was manually ended.")
                        End If
                        updateClientLists()
                    ElseIf msg.Contains("POINT") Then
                        'if msg is just a bunch of coordinates to draw
                        'the coords are in the form x,y-x,y acting as 2 points (point-point) that can draw a line
                        Try
                            For i = 0 To 1 'for each point in the pair
                                Dim ptstr As String = msg.Replace("POINT:", String.Empty).Split("-")(i)
                                pts(i) = New Point(Int(ptstr.Split(",")(0)), Int(ptstr.Split(",")(1)))
                            Next
                            drawMsg()
                        Catch ex As InvalidCastException
                            ' _Log.LogWarn(ex.ToString()) 
                        Catch ex As IndexOutOfRangeException
                            '_Log.LogWarn(ex.ToString()) 'this confuses the log when it neednt; these exceptions just mean
                            '                             that the painter has drawn off the drawing panel or something
                        End Try
                    ElseIf msg.Contains("DRAWLOG") Then 'drawing everything that was drawn before the client connected
                        Dim drawlog As String = msg.Replace("DRAWLOG:;", String.Empty) '.Replace("POINTS:", String.Empty)
                        Dim drawlogsplit As String() = drawlog.Split(";")
                        For Each log In drawlogsplit
                            If log.Contains("#") Then
                                Dim colourstr As String = log.Substring(0, log.IndexOf("#"))
                                pencolour = Color.FromName(colourstr)
                            ElseIf log.Contains("BRUSHSIZE") Then
                                pensize = Int(log.Substring(0, log.IndexOf("B")))
                            Else
                                Try
                                    For i = 0 To 1
                                        Dim ptstr As String = log.Split("-")(i)
                                        pts(i) = New Point(Int(ptstr.Split(",")(0)), Int(ptstr.Split(",")(1)))
                                    Next
                                    drawMsg()
                                Catch ex As InvalidCastException
                                Catch ex As IndexOutOfRangeException
                                End Try
                            End If
                        Next
                        'finally, display the *current* pen size + colour
                        updateBtnPen()
                    ElseIf msg.Contains("CLEAR") Or msg.Contains("shape") Then
                        drawMsg()
                    ElseIf msg.Contains("VOTES") Then
                        votesToStart = Int(msg(0).ToString()) '.Substring(0, msg.IndexOf("VOTES")))
                    ElseIf msg.Contains("REMAINING") Then
                        RemainingVotesToStart = msg.Replace("REMAINING", String.Empty)
                        updateVotesLbls()
                    ElseIf msg.Contains("#") Then
                        Dim colourstr As String = msg.Substring(0, msg.IndexOf("#"))
                        pencolour = Color.FromName(colourstr)
                        updateBtnPen()
                    ElseIf msg.Contains("RESET") Then
                        readData = "CLEAR"
                        drawMsg()
                        Dim didNotError As Boolean = False
                        Dim recursion As Integer = 0
                        While didNotError = False
                            If recursion > 200 Then
                                _Log.LogError("Unable to reset; user_score errors.")
                                Exit While
                            End If
                            Try
                                For Each user In Users.Values
                                    user.Score = 0
                                Next
                                didNotError = True
                            Catch ex As Exception
                                _Log.LogError("Reset: " & ex.Message)
                            Finally
                                recursion += 1
                            End Try
                        End While ' Yes. I did that
                        updateClientLists()
                        updateLabels("Community draw!", "The game has been reset! You are waiting for everyone to join.")
                        allowNewGame()
                        painter = True
                    ElseIf msg.Contains("BRUSHSIZE") Then
                        pensize = Int(msg.Substring(0, msg.IndexOf("B")))
                        updateBtnPen()

                    ElseIf msg.Contains("&RANK&") Then
                        Dim rnk As Integer = 0
                        Dim without As String = msg.Replace("&RANK&", String.Empty)
                        If Integer.TryParse(without, 2) Then
                            rnk = Integer.Parse(without)
                        End If
                        Rank = rnk
                        If Rank = 0 Then
                            SetMyText(FinalTextBound)
                        ElseIf Rank = 1 Then
                            SetMyText(FinalTextBound + " | Moderator")
                        ElseIf Rank = 2 Then
                            SetMyText(FinalTextBound + " | Admin")
                        Else
                            SetMyText(FinalTextBound + " | Manager")
                        End If
                        updateSettingsButton()
                    ElseIf msg.Contains("autostart") Then
                        SetAutoStart(Boolean.Parse(msg.Replace("autostart", String.Empty)))
                    ElseIf msg.Contains("whocanvote") Then
                        SetWhoCanVote(msg.Replace("whocanvote", String.Empty))
                        updateVotesLbls()
                    ElseIf msg.Contains("lobby") Then
                        SetInLobby(msg.Replace("lobby", String.Empty))
                    ElseIf msg.Contains("difficulty") Then
                        SetDifficulty(msg.Replace("difficulty", String.Empty))
                    Else
                        _Log.LogMsg("Spam Error: " & msg)
                    End If
                Next

                If ServerUniqueID = "N/A" Then
                    Send("GET_GUID")
                End If
            Next
        Catch ex As System.ObjectDisposedException
            _Log.LogError(ex.ToString())
        Catch ex As Exception
            'this error is made when the user intentionally closes the form,
            'but also when the server closes (there needs to be a way to separate this)
            '_Log.LogMsg("user probably closed their form")
            _Log.LogError(ex.ToString())
            ' MsgBox("There was an error, the server may have closed :((" + vbCrLf + "A log file has been created, with the error in it")
            _Log.SaveLog("Error - disconnect.")
            If Me.InvokeRequired Then
                Me.Invoke(Sub() Me.Close())
            Else
                Me.Close()
            End If
        End Try
    End Sub

    Private RemainingVotesToStart As String = "3"

    Public Function GetVotesNeededString() As String

#If DEBUG Then
        btnVoteToStart.Enabled = True
        Return 0
#Else
        ' It is the server that performs the actual calculations
        ' as only the server knows who is admin etc.
        If EveryoneCanVote = True Then
            ' Everyone can vote, so we need to check minimum players.
            If lvLeaderboard.Items.Count <= 3 Then
                ' Only require more players when everyone can vote
                ' If only admins can vote, then allow less than 3 players
                btnVoteToStart.Enabled = False
                btnVoteToStart.Text = "Not enough players"
                lblMoreVotesNeededlbl.Text = "more players needed"
                Return 4 - lvLeaderboard.Items.Count
            Else
                btnVoteToStart.Enabled = CanVoteToStart
                btnVoteToStart.Text = "Vote to start"
                lblMoreVotesNeededlbl.Text = "more votes needed"
            End If
        Else
            btnVoteToStart.Enabled = CanVoteToStart
            If IsModerator = False AndAlso EveryoneCanVote = False Then
                btnVoteToStart.Text = "Disabled."
            Else
                btnVoteToStart.Text = "Vote to start"
            End If
            lblMoreVotesNeededlbl.Text = "more votes needed"
        End If
        Return RemainingVotesToStart ' Server will handle calculation

        ' Since only server knows who is admin (and thus can calculate if only Admins can vote)
#End If
    End Function

#Region "-------------------------------------All of the invokes-------------------------------------"
    'this is safe way to change controls with threading
    Private Sub updateBannedView()
        If Me.InvokeRequired Then
            Me.Invoke(Sub() updateBannedView())
        Else
            If Bans.Count > 0 Then
                btn_seebans.Show()
                btn_seebans.Text = "See bans (" & Bans.Count.ToString() & ")"
            Else
                btn_seebans.Hide()
            End If
            BannedForm.UpdateView()
        End If
    End Sub

    Private Sub updateSettingsButton()
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf updateSettingsButton))
        Else
            ' | Users | User Settings | Admin Settings | Manager Settings | Admin Chat
            ' ------------------------------------------------------------------------
            Dim cols As New List(Of DataGridViewColumn)
            For Each col As DataGridViewColumn In dgv_AdminPlace.Columns
                cols.Add(col)
            Next
            Dim selDiff As String = cb_Difficulty.SelectedText
            lblUserDifficulty.Text = "Current Difficulty: " & selDiff
            ' The above settings are used for any ranks
            If Not IsModerator Then
                TogglePage(ManagerTab, False)
                TogglePage(AdminTab, False)
                TogglePage(AdminChatTab, False)
                Label3.Text = "User Settings"
                btnToSettings.Text = "Settings"
            End If
            If IsModerator Then
                btnToSettings.Text = "Settings" & AdminBtnMessages
                Label3.Text = "Moderator settings"
                btn_forcestart.Visible = False
                btn_forceend.Visible = False
                TogglePage(AdminChatTab, True)
            End If
            If IsAdmin Then
                Label3.Text = "Admin settings"
                btn_forcestart.Visible = True
                btn_forceend.Visible = True
                TogglePage(AdminTab, True)
                updateBannedView()
            End If
            If IsManager Then
                Label3.Text = "Manager settings"
                TogglePage(ManagerTab, True)
            End If
            cols.FirstOrDefault(Function(r) r.Name = "user_FullName").Visible = IsModerator
            ' finds a column with the given name, then sets its visibility dependant on rank
            cols.FirstOrDefault(Function(r) r.Name = "user_Kick").Visible = IsModerator
            cols.FirstOrDefault(Function(r) r.Name = "user_Reset").Visible = IsAdmin
            cols.FirstOrDefault(Function(r) r.Name = "user_Ban").Visible = IsAdmin
            cols.FirstOrDefault(Function(r) r.Name = "user_Rank").Visible = IsManager
        End If
    End Sub

    Private Sub TogglePage(page As TabPage, enable As Boolean, Optional index As Integer = 0)
        ' Pages shown in the Settings tabcontrol are toggled
        If enable Then
            If Not SettingsTabControl.TabPages.Contains(page) Then
                SettingsTabControl.TabPages.Insert(index, page)
            End If
        Else
            SettingsTabControl.TabPages.Remove(page)
        End If
    End Sub

    Private Sub updateVotesLbls()
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf updateVotesLbls))
        Else
            lblMoreVotesNeeded.Text = GetVotesNeededString().ToString()
        End If
    End Sub

    Private Sub SetAutoStart(bool As Boolean)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetAutoStart(bool))
        Else
            DisregardAutoStart = True
            cbAutoStart.Checked = bool
            DisregardAutoStart = False
        End If
    End Sub

    Private Sub SetDifficulty(str As String)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetDifficulty(str))
        Else
            StillLoading = True
            For i As Integer = 0 To cb_Difficulty.Items.Count - 1
                Dim val As String = cb_Difficulty.GetItemText(cb_Difficulty.Items(i))
                val = val.Trim()
                If val = str Then
                    cb_Difficulty.SelectedIndex = i
                    Exit For
                End If
            Next
            StillLoading = False
        End If
    End Sub

    Public Sub AddLobbyMessage(from As String, msg As String, clr As Color)
        ' Adds a message to the lobby text box
        If Me.InvokeRequired Then
            Me.Invoke(Sub() AddLobbyMessage(from, msg, clr))
            Return
        End If
        Dim fntStyle As FontStyle = FontStyle.Regular
        If msg.Substring(0, 1) = "\" Then
            msg = msg.Substring(1)
            fntStyle += FontStyle.Italic
        End If
        If msg.Substring(0, 1) = "/" Then
            fntStyle += FontStyle.Bold
            msg = msg.Substring(msg.IndexOf(" ") + 1)
        End If
        If from <> "Client" Then msg = from & ": " & msg
        msg = msg.Replace(vbCrLf, vbCrLf + "        ...       ")
        rtb_lobby.ReadOnly = False
        Dim now As DateTime = DateTime.Now()
        rtb_lobby.SelectionColor = Color.Black
        rtb_lobby.SelectionStart = rtb_lobby.Text.Length
        rtb_lobby.AppendText("[" + now.ToString("hh:MM:ss") + "] ")
        rtb_lobby.SelectionColor = clr
        rtb_lobby.SelectionFont = New Font(rtb_lobby.Font, fntStyle)
        rtb_lobby.AppendText(msg)
        rtb_lobby.AppendText(vbCrLf)
        rtb_lobby.ReadOnly = True
        rtb_lobby.SelectionStart = rtb_lobby.TextLength - 1
        rtb_lobby.ScrollToCaret()
    End Sub

    Private Sub SetDifficulties(value As String())
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetDifficulties(value))
            Return
        End If
        cb_Difficulty.Items.Clear()
        For Each diff As String In value
            If String.IsNullOrWhiteSpace(diff) Then Continue For
            cb_Difficulty.Items.Add(diff)
        Next
    End Sub

    Private Sub SetInLobby(str As String)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetInLobby(str))
        Else
            InLobby = Boolean.Parse(str)
            'rtb_lobby.Text = ""
            If InLobby Then
                panel_LobbyChat.BringToFront()
                panel_LobbyChat.Size = pFrame.Size
                btnGuess.Text = "Chat"
                btnGuess.Enabled = True
                AddLobbyMessage("Client", "Lobby Chat - Talk to others below", Color.DarkCyan)
                ' Hide the drawing things since we can't..
            Else
                pFrame.BringToFront()
                btnGuess.Text = "Guess"
            End If
            panelDrawingTools.Visible = (Not InLobby) AndAlso painter
        End If
    End Sub

    Private Sub SetWhoCanVote(str As String)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetWhoCanVote(str))
        Else
            StillLoading = True
            If str = "Everyone" Then
                cb_WhoCanVote.SelectedIndex = 0
                EveryoneCanVote = True
            Else
                cb_WhoCanVote.SelectedIndex = 1
                EveryoneCanVote = False
            End If
            StillLoading = False
        End If
    End Sub

    Private Sub SetMyText(txt As String)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetMyText(txt))
        Else
            Me.Text = txt
            If IsModerator Then
            Else
                panelSettings.Hide()
            End If
            updateSettingsButton()
        End If
    End Sub

    Private Sub showConnectPanel()
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf showConnectPanel))
        Else
            panelConnect.Enabled = True
            panelConnect.Visible = True
            Rank = 0
            SetMyText(FinalTextBound)
            Try
                clientSocket.Close() ' this would probably be better. -thanks again boo
            Catch ex As Exception
            End Try
            'timerDiscordUpdate.Stop()
        End If
    End Sub

    Private Sub hideConnectPanel()
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf hideConnectPanel))
        Else
            panelConnect.Enabled = True
            panelConnect.Visible = False
        End If
    End Sub

    Public InLobby As Boolean = True ' we are (by default) in the lobby!
    Private Sub allowNewGame()
        ' Sets up the form such that a new game can be played
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf allowNewGame))
        Else
            If timeLeft <> 0 And lvGuessHistory.Items.Count > 0 Then
                'the final guess should be the correct one
                lvGuessHistory.Items.Item(lvGuessHistory.Items.Count - 1).ForeColor = Color.Red
            End If
            txtGuess.Text = ""
            btnGuess.Enabled = False
            votesToStart = 0
            lblMoreVotesNeeded.Text = GetVotesNeededString()
            btnVoteToStart.Enabled = CanVoteToStart
            btnVoteToStart.Visible = True
            lblTimeLeft.Visible = False
            lblTimeLeftlbl.Visible = False
            lblMoreVotesNeeded.Visible = True
            lblMoreVotesNeededlbl.Visible = True
            HasVotedThisGame = False
            btn_forcestart.Enabled = True
            btn_forceend.Enabled = False
            lblMisc.BackColor = Color.FromArgb(145, 184, 173)
            If InLobby Then
                panel_LobbyChat.BringToFront()
                btnGuess.Text = "Chat"
                btnGuess.Enabled = True
            Else
                pFrame.BringToFront()
                btnGuess.Text = "Guess"
            End If
        End If
    End Sub

    Private Sub updateClientLists()
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf updateClientLists))
        Else
            Dim allColors As New List(Of Color) From {Color.LightBlue, Color.LightCoral, Color.LightCyan, Color.LightGoldenrodYellow, Color.LightGray, Color.LightGreen, Color.LightPink, Color.LightSalmon, Color.LightSeaGreen, Color.LightSkyBlue, Color.LightSlateGray, Color.LightSteelBlue, Color.LightSteelBlue, Color.LightYellow}
            Dim allUsers As New Dictionary(Of String, Integer) ' Actual name: occurences
            lvLeaderboard.Items.Clear()
            dgv_AdminPlace.Rows.Clear()
            For Each usr As User In Users.Values
                lvLeaderboard.Items.Add(New ListViewItem({usr.Name, usr.Score}))
                Dim row As String() = New String() {usr.Name, usr.ActualName, usr.Score, "Reset", "Kick", "Ban", usr.Rank}
                If allUsers.ContainsKey(usr.ActualName) Then
                    allUsers(usr.ActualName) += 1
                Else
                    allUsers(usr.ActualName) = 1
                End If
                dgv_AdminPlace.Rows.Add(row)
                Dim colBack = Color.FromKnownColor(KnownColor.Window)
                Dim colFront = Color.FromKnownColor(KnownColor.WindowText)
                If usr.IsManager Then
                    colBack = Color.Red
                    colFront = Color.Red
                ElseIf usr.IsAdmin Then
                    colBack = Color.Pink
                    colFront = Color.Pink
                ElseIf usr.IsMod Then
                    colBack = Color.Orange
                    colFront = Color.Orange
                End If
                dgv_AdminPlace.Rows.Item(dgv_AdminPlace.Rows.Count - 1).Cells.Item(0).Style.BackColor = colBack
                dgv_AdminPlace.Rows.Item(dgv_AdminPlace.Rows.Count - 1).Cells.Item(1).Style.ForeColor = colFront
                lvLeaderboard.Items().Item(lvLeaderboard.Items.Count - 1).ForeColor = colFront
            Next
            For Each item As KeyValuePair(Of String, Integer) In allUsers
                If item.Value = 1 Then Continue For
                If allColors.Count = 0 Then Continue For
                Dim toBeColor As Color = allColors.Item(0)
                allColors.RemoveAt(0)
                For Each row As DataGridViewRow In dgv_AdminPlace.Rows
                    If row.Cells.Item(1).Value = item.Key.ToString() Then
                        row.Cells.Item(1).Style.BackColor = toBeColor
                    End If
                Next
            Next
            dgv_AdminPlace.ClearSelection()
            lblMoreVotesNeeded.Text = GetVotesNeededString()
        End If
    End Sub

    Private Function IdealTextColor(bg As Color) As Color
        ' Used to get the average colour for the Connect screen background
        Dim nThreshold As Integer = 105
        Dim bgDelta = Convert.ToInt32((bg.R * 0.299) + (bg.G * 0.587) + (bg.B * 0.114))
        If 255 * bgDelta < nThreshold Then
            Return Color.White
        Else
            Return Color.Black
        End If
    End Function

    Private Sub updateBtnPen()
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf updateBtnPen))
            Return
        End If
        btnPenSize.BackColor = pencolour
        btnClear.BackColor = pencolour
        btnClear.ForeColor = IdealTextColor(pencolour)
        btnClear.ForeColor = IdealTextColor(pencolour)
        btnClear.ForeColor = IdealTextColor(pencolour)
        btnClear.ForeColor = IdealTextColor(pencolour)
        btnClear.ForeColor = IdealTextColor(pencolour)
        btnPenSize.Location = New Point(btnPenSize.Location.X - ((pensize - btnPenSize.Width) / 2), btnPenSize.Location.Y - ((pensize - btnPenSize.Height) / 2))
        '                   = current location - (change in pen size /2)
        btnPenSize.Size = New Size(pensize, pensize)
    End Sub

    Private Sub beginGame()
        ' Starts the name, initiates things etc.
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf beginGame))
            Return
        End If
        pencolour = Color.Black
        pensize = 1
        updateBtnPen()
        timeLeft = 50
        lblTimeLeft.Text = timeLeft.ToString()
        lblTimeLeft.ForeColor = Color.Black
        btnVoteToStart.Visible = False
        lblMoreVotesNeededlbl.Visible = False
        lblMoreVotesNeeded.Visible = False
        lblTimeLeft.Visible = True
        lblTimeLeftlbl.Visible = True
        timerTimeLeft.Enabled = True
        btn_forcestart.Enabled = False
        btn_forceend.Enabled = True
        btnGuess.Enabled = Not painter
        SetInLobby(InLobby.ToString()) ' already has been set in the getmessage thingy
        Dim now As DateTime = DateTime.Now()
        Dim addTime As Date = now.AddSeconds(timeLeft)
    End Sub

    Private Sub drawMsg() 'draws the broadcasted coordinates to the canvas
        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf drawMsg))
            Return
        End If
        If CanvasImage Is Nothing Then
            CanvasImage = New Bitmap(pCanvas.Width, pCanvas.Height)
            pCanvas.DrawToBitmap(CanvasImage, New Rectangle(0, 0, pCanvas.Width, pCanvas.Height))
        End If
        If readData.Contains("CLEAR") Then
            pCanvas.DrawToBitmap(CanvasImage, New Rectangle(0, 0, pCanvas.Width, pCanvas.Height))
        ElseIf readData.Contains("shape") Then
            Try
                Dim shape = readData.Substring(readData.IndexOf(":") + 1)
                shape = shape.Replace("`", String.Empty)
                Dim shapeConstructor() = shape.Split(".") 'will be x.y.width.height
                Using g As Graphics = Graphics.FromImage(CanvasImage)
                    If readData.Contains("CIRCLE") Then g.DrawEllipse(New Pen(pencolour, pensize), New Rectangle(Int(shapeConstructor(0)), Int(shapeConstructor(1)), Int(shapeConstructor(2)), Int(shapeConstructor(3))))
                    If readData.Contains("RECTANGLE") Then g.DrawRectangle(New Pen(pencolour, pensize), New Rectangle(Int(shapeConstructor(0)), Int(shapeConstructor(1)), Int(shapeConstructor(2)), Int(shapeConstructor(3))))
                End Using
            Catch ex As Exception
                MsgBox(ex.ToString())
            End Try

        Else
            Using g As Graphics = Graphics.FromImage(CanvasImage)
                g.DrawLine(New Pen(pencolour, pensize), pts(0), pts(1))
            End Using
        End If
        priorCanvasImage = New Bitmap(CanvasImage)
        pCanvas.CreateGraphics.DrawImage(CanvasImage, 0, 0)
    End Sub

    Private Sub updateLabels(lblMiscText As String, lblStatusText As String, Optional dontShowVoteLabels As Boolean = False)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() updateLabels(lblMiscText, lblStatusText))
        Else
            lblMisc.Text = lblMiscText
            lblStatus.Text = lblStatusText
            lblMoreVotesNeeded.Text = GetVotesNeededString()
            If votesToStart = 0 Then btnVoteToStart.Enabled = CanVoteToStart
            If lblMoreVotesNeeded.Visible = False AndAlso dontShowVoteLabels = False Then
                lblMoreVotesNeededlbl.Visible = True
                lblMoreVotesNeeded.Visible = True
                lblTimeLeft.Visible = False
                lblTimeLeftlbl.Visible = False
            End If
            If painter And lblMisc.Text.Contains("You are drawing") Then
                lblMisc.BackColor = Color.Black
                lblMisc.ForeColor = Color.Black
            End If
        End If
    End Sub

    Private Sub updateGuessHistory(guess As String)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() updateGuessHistory(guess))
        Else
            If guess = "" Then 'erase guess history
                If lvGuessHistory.Items.Count > 0 Then lvGuessHistory.Items.Clear()
            Else
                lvGuessHistory.Items.Add(guess)
            End If
        End If
    End Sub
#End Region

#Region "Version Handling"
    Private SkipVersionChecks As Boolean = False
    Private SkipIpGet As Boolean = False
    Public Function GetLatestVersion(url As String) As Version
        ' Contacts bitbucket to find the latest version of this client.
        If SkipVersionChecks Then Return New Version("0.0.0.1")
        Try
            Dim myReq As HttpWebRequest = WebRequest.Create(url)
            Dim response As HttpWebResponse = myReq.GetResponse()
            Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
            Dim strResponse As String = resStream.ReadToEnd().ToString()
            Return New Version(strResponse)
        Catch ex As Exception
            _Log.LogError(ex.ToString())
            MsgBox(ex.ToString())
        End Try
        Return New Version("0.0.0.0")
    End Function

    Public Sub UpdateVersionOneAhead(path As String)
        ' This will automatically increment the build version for the client
        ' Version: Major.Minor.Build.Revision
        If Not File.Exists(path) Then
            MsgBox("It appears this client is running seperatly." & vbCrLf & "Good day ¯\_(ツ)_/¯")
            Return
        End If
        Dim oldLines As String() = File.ReadAllLines(path)
        Dim loopText As String() = File.ReadAllLines(path)
        For Each line As String In loopText
            If line.Contains("AssemblyFileVersion") Then
                Dim newLine As String = "<Assembly: AssemblyFileVersion(" + ControlChars.Quote
                Dim latestMinor As Integer = (LatestVersion.Build + 1)
                Dim newVersion As Version = New Version(LatestVersion.Major.ToString + "." + LatestVersion.Minor.ToString + "." + latestMinor.ToString + "." + LatestVersion.Revision.ToString)
                newLine += newVersion.ToString() + ControlChars.Quote + ")>"
                oldLines(Array.IndexOf(oldLines, line)) = newLine
            ElseIf line.Contains("AssemblyVersion") Then
                Dim newLine As String = "<Assembly: AssemblyVersion(" + ControlChars.Quote
                Dim latestMinor As Integer = LatestVersion.Build + 1
                Dim newVersion As Version = New Version(LatestVersion.Major.ToString + "." + LatestVersion.Minor.ToString + "." + latestMinor.ToString + "." + LatestVersion.Revision.ToString)
                newLine += newVersion.ToString() + ControlChars.Quote + ")>"
                oldLines(Array.IndexOf(oldLines, line)) = newLine
            End If
        Next
        File.WriteAllLines(path, oldLines)
        MsgBox("Please re-run this " & clName & " through Visual Studio again." & vbCrLf & vbCrLf & "(The client must quit because the version has been updated)" + vbCrLf + "To prevent this message, run the " + clName + " from a folder that is NOT named 'Debug' or 'Release'")
        End
    End Sub

    Const clName As String = "client"
    Public Sub HandleVersion()
        ' This actually does the full version checking..
        ' - If running latest, then mark as so and do nothing.
        ' - If running ahead, mark as so (in Me.Text) and continue.
        ' - If behind, warn the user and attempt to download the latest version
        Try
            LatestVersion = GetLatestVersion("https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/" + clName + "/" + clName + "/bin/Release/.version")
        Catch ex As WebException
            MsgBox(ex.ToString())
            LatestVersion = New Version("0.0.0.0")
        End Try
        Me.Text += " | " & ThisThingVersion.ToString()
        If ThisThingVersion.CompareTo(LatestVersion) = 0 Then
            Me.Text += " (UpToDate)"
            Try
                File.Delete("new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe")
            Catch ex As Exception
                _Log.LogError(ex.ToString())
            End Try
            ' same version, so we update our one to be ahead, if we are in VS..
            Dim curDir As String = Directory.GetCurrentDirectory()
            Dim dirSplit As String() = curDir.Split("\")
            If dirSplit(dirSplit.Count - 1) = "Debug" OrElse dirSplit(dirSplit.Count - 1) = "Release" Then
                Dim toBePath As String = ""
                For Each item As String In dirSplit
                    If Array.IndexOf(dirSplit, item) > dirSplit.Count - 3 Then
                        Continue For
                    End If
                    toBePath += item + "\"
                Next
                toBePath += "\My Project\AssemblyInfo.vb"
                UpdateVersionOneAhead(toBePath)
            End If
        ElseIf ThisThingVersion.CompareTo(LatestVersion) > 0 Then
            Me.Text += " (Ahead)"
        Else
            Me.Text += " (OutOfDate)"
            MsgBox("Warning:" & vbCrLf & "This " & clName & " is not running the latest version" & vbCrLf & "Your version: " & ThisThingVersion.ToString() & vbCrLf &
                   "Latest Version: " & LatestVersion.ToString() & vbCrLf & "Attempting to download the latest version...")
            Dim url As String = "https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/" & clName & "/" & clName & "/bin/Release/" & clName & ".exe"
            Dim newName As String = "new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe"
            Try
                Dim web_Download As New WebClient
                If File.Exists(newName) Then
                    Dim dlVersion As Version = New Version(FileVersionInfo.GetVersionInfo(newName).FileVersion)
                    If Not dlVersion = LatestVersion Then
                        File.Delete(newName) ' not the latest version
                        web_Download.DownloadFile(url, newName)
                    End If
                Else
                    web_Download.DownloadFile(url, newName)
                End If
                _Log.LogMsg("New " & clName & " has been downloaded. Checking version...") ' we dont tell them of the old version.
                Dim downloadVersion As Version = New Version(FileVersionInfo.GetVersionInfo(newName).FileVersion)
                If downloadVersion = LatestVersion Then
                    _Log.LogMsg("Version is valid. Running new client now.. this client will close.")
                    Process.Start(newName)
                    Threading.Thread.Sleep(5)
                    End
                Else
                    MsgBox("Attempts to download the new version have failed." & vbCrLf & "Please download manually.")
                End If
            Catch ex As UnauthorizedAccessException
                _Log.LogError(ex.ToString())
                MsgBox("You do not have permissions to download/write at that folder" & vbCrLf & "Please copy this client to your personal drive, or run as administrator, and try again.")
            Catch ex As WebException
                _Log.LogError(ex.ToString())
                MsgBox("You do not have permissions to download/write at that folder" & vbCrLf & "Please copy this client to your personal drive, or run as administrator, and try again.")
            Catch ex As Exception
                MsgBox(ex.ToString())
                _Log.LogError(ex.ToString())
            Finally
                MsgBox("The program will now close.")
                End
            End Try
        End If
        If Path.GetFileNameWithoutExtension(Application.ExecutablePath).ToString = "new" + clName.Substring(0, 1).ToUpper() + clName.Substring(1) + ".exe" Then
            MsgBox("WARNING:" & vbCrLf & "It is advised that you download the complete Visual Studio project of the client." & vbCrLf & "You may ignore this message.")
        End If
        FinalTextBound = Me.Text
    End Sub
#End Region

    Private Sub UnhandledException_Handler(sender As Object, e As UnhandledExceptionEventArgs)
        Dim ex As Exception = e.ExceptionObject
        _Log.LogError("Error occured and was not handled: " + ex.ToString())
        _Log.SaveLog("UnhandledException-Close")
        MsgBox(ex.ToString())
        Me.Close()
    End Sub

#Region "-----------------------------btnClicks or txtChanges etc---------------------------------"
    Private StillLoading As Boolean = True ' Prevents comboboxes from sending to server when they are being set.
    Private rand As New Random()
    Private Function GenerateRandom64Digit() As String
        ' Used in the serial generation.
        Dim str As String = ""
        For i As Integer = 0 To 64
            str += rand.Next(0, 9).ToString()
        Next
        Return str
    End Function
    Public Function EncryptSHA256Managed(ByVal ClearString As String) As String
        ' Gets the SHA256 cryptograhpic hash of the inputted plaintext
        ' This is used in:
        ' - Getting the client's hash (to ensure it hasnt been modified)
        ' - Serials - unique per computer (or possibly per account?)
        ' - Other things i guess
        Dim uEncode As New UnicodeEncoding()
        Dim bytClearString() As Byte = uEncode.GetBytes(ClearString)
        Dim sha As New System.Security.Cryptography.SHA256Managed()
        Dim hash() As Byte = sha.ComputeHash(bytClearString)
        Return Convert.ToBase64String(hash)
    End Function
    Friend Sub HandleRegistryStuff()
        ' What is saved to the registry:
        ' - Last IP used to connect
        ' - Last name used to connect
        ' - Serial of this client - this function generates and then gets the serial
        Dim readValue As String = My.Computer.Registry.GetValue(MainReg + "\Client", "SerialKey", Nothing)
        Dim tobeSerial As String = ""
        If readValue Is Nothing Then
            Try
                _Log.LogMsg("Generating new serial")
                Dim numberRand As String = GenerateRandom64Digit()
                tobeSerial = EncryptSHA256Managed(numberRand + DateTime.Now.ToString())
                My.Computer.Registry.CurrentUser.CreateSubKey("Liliana_Drawing")
                My.Computer.Registry.SetValue(MainReg + "\Client", "SerialKey", tobeSerial)
                readValue = My.Computer.Registry.GetValue(MainReg + "\Client", "SerialKey", Nothing)
                _Log.LogMsg("New serial: " & readValue)
            Catch ex As System.Security.SecurityException
                _Log.LogError("Access denied to registry: " + ex.ToString())
                readValue = tobeSerial
            Catch ex As Exception
                _Log.LogError("Unknown error in registry: " + ex.ToString())
                readValue = tobeSerial
            End Try
        End If
        Serial = readValue
        _Log.LogMsg("Client serial: " & Serial)
    End Sub
    Public Shared CLIENT_EXEC_CHECKSUM As String

    Private Function ExistsFile(s As String) As Boolean
        ' used for LINQ purposes is my guess.
        Return File.Exists(s)
    End Function

    Private Sub ChatForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If DisableMasterList Then
            lblMasterList.Hide()
            btnMasterlistRefresh.Hide()
            lblMasterListInfo.Hide()
            dgv_servers.Hide()
        End If
        Me.Size = New Size(801, 708)
        READONLYPANEL.SendToBack()
        rtb_lobby.Dock = DockStyle.Fill
        ' Get the four corners' pixels and average their colour
        Dim topLeft As Color = My.Resources.Chosen.GetPixel(0, 0)
        Dim topRight As Color = My.Resources.Chosen.GetPixel(0, My.Resources.Chosen.Height - 1)
        Dim bottomLeft As Color = My.Resources.Chosen.GetPixel(My.Resources.Chosen.Width - 1, 0)
        Dim bottomRight As Color = My.Resources.Chosen.GetPixel(My.Resources.Chosen.Width - 1, My.Resources.Chosen.Height - 1)
        Dim sampledPixels As New List(Of Color) From {topLeft, topRight, bottomLeft, bottomRight}
        ' Get four random points from top
        For i As Integer = 1 To 4
            Dim rndx As Integer = rand.Next(1, My.Resources.Chosen.Width - 2)
            sampledPixels.Add(My.Resources.Chosen.GetPixel(rndx, 0))
        Next ' Now four random points from each side
        For i As Integer = 1 To 4
            Dim rndy As Integer = rand.Next(1, My.Resources.Chosen.Height - 2) ' left side
            sampledPixels.Add(My.Resources.Chosen.GetPixel(0, rndy))
            rndy = rand.Next(1, My.Resources.Chosen.Height - 2) ' right side
            sampledPixels.Add(My.Resources.Chosen.GetPixel(My.Resources.Chosen.Width - 1, rndy))
        Next
        Dim totalR As Integer = 0
        Dim totalG As Integer = 0
        Dim totalB As Integer = 0
        For Each clr As Color In sampledPixels
            totalR += clr.R
            totalG += clr.G
            totalB += clr.B
        Next
        Dim resultR As Integer = totalR / sampledPixels.Count
        Dim resultG As Integer = totalG / sampledPixels.Count
        Dim resultB As Integer = totalB / sampledPixels.Count
        panelConnect.BackColor = Color.FromArgb(resultR, resultG, resultB)
        panelConnect.BackgroundImage = My.Resources.Chosen
        AddHandler Application.ApplicationExit, AddressOf OnApplicationExit
        AddHandler AppDomain.CurrentDomain.UnhandledException, AddressOf UnhandledException_Handler
        Dim curPath As String = Directory.GetCurrentDirectory()
        _Log = New LogHandle("logs\", "Not set.", "None.", ThisThingVersion, Environment.UserName.ToLower())
        _Log.LogMsg("Client path: " & curPath)
        If curPath.Contains("P:") Then
            MsgBox("Error" & vbCrLf & "You must run this client in your own personal drive" & vbCrLf & vbCrLf & "Please copy this client to your H:\ drive")
            Me.Close()
        End If
        ' BugReporting.dll = bug reports to trello
        ' DiscordDLL.dll = my DLL to update discord
        ' discord-rpc-w32.dll = official discord dll that is used
        Dim neededFiles As List(Of String) = New List(Of String) From {"DrawingEngine.dll", "BugReporting.dll"}
        If Not neededFiles.TrueForAll(AddressOf ExistsFile) Then
            MsgBox("Needed files are missing; they will be download those now..")
            For Each file As String In neededFiles ' Can be added later.
                Try
                    Dim url As String = "https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/" & clName & "/" & clName & "/bin/Release/Resources/" & file
                    Dim web_Download As New WebClient
                    web_Download.DownloadFile(url, file)
                Catch ex As Exception
                    _Log.LogError(ex.ToString())
                End Try
            Next
        End If
        ThisThingVersion = Assembly.GetExecutingAssembly().GetName().Version
        File.WriteAllText(".version", ThisThingVersion.ToString())
        HandleVersion()
        HandleRegistryStuff() ' Serial and all that.
        _Log.SetVersion(ThisThingVersion)
        Dim ipStored As String = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Liliana_Drawing" + "\Client", "DefaultIP", getIPaddress().ToString)
        If Not SkipIpGet Then
            Try ' Get the IP on bitbucket (so we dont have to enter it in class)
                Dim myReq As HttpWebRequest = WebRequest.Create("https://bitbucket.org/thegrandcoding/drawing/raw/HEAD/server/Connection.txt")
                Dim response As HttpWebResponse = myReq.GetResponse()
                Dim resStream As StreamReader = New StreamReader(response.GetResponseStream())
                Dim firstLine As String = resStream.ReadLine()
                Dim firstLineSplit As String() = firstLine.Split(" ")
                Dim dateOnline As DateTime = DateTime.Parse(firstLineSplit(1).Replace("_", " "))
                Dim dateDiff As TimeSpan = DateTime.Now() - dateOnline
                If dateDiff.TotalHours <= 2 Then
                    Dim secondLine As String = resStream.ReadLine()
                    secondLine = secondLine.Split(" ")(1)
                    ipStored = secondLine.Replace("_", " ")
                    Dim thirdLine As String = resStream.ReadLine()
                    thirdLine = thirdLine.Split(" ")(1)
                    Dim newServer As New ClientDisplayServer(thirdLine.Replace("_", " "), ipStored, "-1", False)
                    TestingServerConnectionDotTxt = newServer
                    _Log.LogMsg("IP got: " & ipStored)
                End If
            Catch ex As Exception
                _Log.LogError(ex.ToString())
                MsgBox(ex.ToString())
            End Try
        End If
#If DEBUG Then
        _Log.LogMsg("Running as debug")
#Else
        _Log.LogMsg("Running as release")
#End If
        txtIpAddress.Text = ipStored
        Dim thread_ As New Threading.Thread(AddressOf SetMasterList)
        thread_.Start()
        panelConnect.Location = New Point(0, 0)
        panelConnect.Visible = True
        panelSettings.Location = New Point(0, 0)
        panelSettings.Visible = False
        lblTimeLeft.Visible = False
        lblTimeLeftlbl.Visible = False
        cb_Difficulty.SelectedIndex = 0
        cb_WhoCanVote.SelectedIndex = 0
        StillLoading = False ' Allows comboboxes to send to server when changed by user
        Dim args As String() = Environment.GetCommandLineArgs
        Dim commandLine As New List(Of String)
        commandLine.AddRange(args)
        commandLine.RemoveAt(0) ' removes path argument
        Dim actualArgs As New List(Of String)
        Try
            For i As Integer = 0 To (commandLine.Count - 1) Step 2
                If commandLine.Item(i).Substring(0, 1) = "-" Then
                    actualArgs.Add(commandLine.Item(i) & " " & commandLine.Item(i + 1))
                End If
            Next
        Catch ex As Exception
        End Try
        Try
            actualArgs.AddRange(File.ReadAllLines("commandline.txt"))
        Catch ex As Exception
        End Try
        HandleCommandLine(actualArgs)
        Dim now As DateTime = DateTime.UtcNow()
        Dim nowLong As Long = CType(TimeToUnix(now), Long)
        'DiscordDLL.DiscordApp.UpdatePres(New DiscordDLL.DiscordApp.GameStatusInfo("", -1))
        DiscordClient = New DiscordRpcClient("430626605988052992", True, -1)
        DiscordClient.Logger = New Logging.FileLogger("discord-logs.txt") With {.Level = Logging.LogLevel.Info}
        AddHandler DiscordClient.OnReady, AddressOf DiscordClient_OnReady
        AddHandler DiscordClient.OnError, AddressOf DiscordClient_OnError
        AddHandler DiscordClient.OnClose, AddressOf DiscordClient_OnClose
        AddHandler DiscordClient.OnJoin, AddressOf DiscordClient_OnJoin
        AddHandler DiscordClient.OnJoinRequested, AddressOf DiscordClient_OnJoinRequested
        DiscordClient.SetSubscription(EventType.Join Or EventType.Spectate)
        DiscordClient.Initialize()
        Dim newThread = New Threading.Thread(AddressOf DiscordClient_MainLoop)
        newThread.Start()
        Try
            Dim fileStrm As FileStream = File.OpenRead(Application.ExecutablePath)
            Dim hashValue As Byte()
            Dim hash = MD5.Create()
            hashValue = hash.ComputeHash(fileStrm)
            Dim i As Integer
            Dim sOutput As New StringBuilder(hashValue.Length)
            For i = 0 To hashValue.Length - 1
                sOutput.Append(hashValue(i).ToString("X2"))
            Next
            CLIENT_EXEC_CHECKSUM = sOutput.ToString().ToLower()
        Catch ex As Exception
            MsgBox(ex.ToString())
        End Try
        panelConnect.Size = Me.Size
        panelSettings.Size = Me.Size
    End Sub
#Region "Discord Stuffs"
    Public GamePresence As New RichPresence With {
    .Details = "Not Connected"
    }
    Private Sub DiscordClient_OnReady(sender As Object, args As Message.ReadyMessage)
        _Log.LogMsg($"DiscordRPC ready, {args.Version} {args.User.Username}")
    End Sub

    Private Sub DiscordClient_MainLoop()
        While DiscordClient IsNot Nothing
            If DiscordClient IsNot Nothing Then
                DiscordClient.Invoke()
            End If
            Threading.Thread.Sleep(1000)
        End While
        DiscordClient.Dispose()
    End Sub

    Private Sub DiscordClient_OnClose(sender As Object, args As Message.CloseMessage)
        _Log.LogWarn("DiscordRPC closed, reason: " + args.Reason + ", " + args.Code)
    End Sub

    Private Sub DiscordClient_OnError(sender As Object, args As Message.ErrorMessage)
        Dim message = $"Error occured related to Discord. ({args.Code} {args.Message})"
        _Log.LogError(message)
        MsgBox(message)
    End Sub

#Region "Join Request Stuffs"
    Private Sub DiscordClient_OnJoinRequested(sender As Object, args As Message.JoinRequestMessage)
        Dim message As String = $"Discord User {args.User.Username}#{args.User.Discriminator} ({args.User.ID}) requests to join your game."
        _Log.LogMsg(message)
        If MsgBox(message + vbCrLf + "Do you want them to join?", MsgBoxStyle.YesNo, "Discord Request") = vbYes Then
            _Log.LogWarn("Responded ACCEPT to join request by " + args.User.Username)
            DiscordClient.Respond(args, True)
        Else
            DiscordClient.Respond(args, False)
        End If
    End Sub

    Public Sub DiscordClient_OnJoin(sender As Object, args As Message.JoinMessage)
        Dim servInfo = MasterlistDLL.MasterListClient.GetJoinInfo(args.Secret)
        If servInfo Is Nothing Then
            MsgBox("Unknown join info.")
        Else
            If clientSocket.Connected Then
                MsgBox("Recieved event to connect to server, but we already are?")
            Else
                MsgBox("Joining " + servInfo.ConnectionIP)
                Dim ips = New List(Of String) From {servInfo.ConnectionIP, servInfo.ConnectionAlternateIP}
                Dim success = False
                For Each ip As String In ips
                    Try
                        txtIpAddress.Text = ip
                        connect()
                        success = True
                    Catch ex As Exception
                    End Try
                Next
                If success Then
                    MsgBox("Connected!")
                Else
                    MsgBox("Errored connect!")
                End If
            End If
        End If
    End Sub
#End Region

#End Region

    Public Function TimeToUnix(ByVal dteDate As Date) As String
        If dteDate.IsDaylightSavingTime = True Then
            dteDate = DateAdd(DateInterval.Hour, -1, dteDate)
        End If
        TimeToUnix = DateDiff(DateInterval.Second, #1/1/1970#, dteDate)
    End Function


    Private lastSentMessageTime As DateTime = DateTime.Now()
    Private amountOfMessages As Integer = 0 ' sent in last 5? seconds
    Private isMuted As Boolean = False
    Private isMutedTime As DateTime = DateTime.Now()
    Private isMutedLength As Integer = 0

    Private Sub btnGuess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuess.Click
        Try
            If btnGuess.Text = "Guess" Then
                If txtGuess.Text.Contains("%") And False Then
                    MsgBox("That guess is invalid; please use letters and/or numbers only")
                    ' yes i know it allows all others, but i think security thorugh obsecurity of how this works
                ElseIf String.IsNullOrWhiteSpace(txtGuess.Text) Then
                    'just dont send it
                Else
                    Send("GUESS:" & txtName.Text & ": " & txtGuess.Text)
                End If
            Else
                ' Chat message
                If String.IsNullOrWhiteSpace(txtGuess.Text) Then
                    AddLobbyMessage("Client", "You must enter a message to send", Color.Red)
                    Return
                End If
                If txtGuess.Text.Contains("%") Or txtGuess.Text.Contains(";") Then
                    AddLobbyMessage("Client", "That message contains illegal charactors", Color.Red)
                    Return
                End If
                If txtGuess.Text.Length > 50 Then
                    AddLobbyMessage("Client", "That message is too long", Color.Red)
                    Return
                End If
                If isMuted Then
                    Dim mutedLength As TimeSpan = DateTime.Now() - isMutedTime
                    If mutedLength.TotalSeconds > isMutedLength Then
                        isMuted = False
                        AddLobbyMessage("Client", "You have been unmuted, do not spam next time.", Color.DarkCyan)
                    End If
                End If
                If isMuted Then
                    If isMutedLength > 10 Then
                        AddLobbyMessage("Client", "You are unable to speak.", Color.Red)
                    Else
                        Dim left As TimeSpan = DateTime.Now() - isMutedTime
                        Dim amnt As Integer = Math.Round(left.TotalSeconds - isMutedLength, 0)
                        AddLobbyMessage("Client", "You are muted from sending messages (expires: " & Math.Abs(amnt).ToString() & ")", Color.Red)
                    End If
                    Return
                End If
                Dim diff As TimeSpan = DateTime.Now() - lastSentMessageTime
                If diff.TotalSeconds > 5 Then
                    amountOfMessages = 0
                End If
                If amountOfMessages > 5 Then
                    isMutedLength += 5
                    If IsManager Then
                        isMutedLength = 5
                    ElseIf IsAdmin Then
                        isMutedLength = 10
                    End If
                    If isMutedLength > 10 Then
                        If IsModerator Then ' Gives protections against admins being muted for too long
                            isMutedLength = 15
                        Else
                            AddLobbyMessage("Client", "Thanos demands your silence", Color.Red)
                            isMutedLength = 999999999
                            Send("&REPORT&" & txtName.Text & ";Perm mute due to spam")
                        End If
                    End If
                    isMuted = True
                    isMutedTime = DateTime.Now()
                    AddLobbyMessage("Client", "Silence!", Color.Red)
                    Return
                ElseIf amountOfMessages > 4 Then
                    AddLobbyMessage("Client", "Stop spamming or you will be muted.", Color.Red)
                ElseIf amountOfMessages > 3 Then
                    AddLobbyMessage("Client", "If you continue to send messages, you will be muted.", Color.Red)
                End If
                amountOfMessages += 1
                lastSentMessageTime = DateTime.Now()

                If txtGuess.Text.StartsWith("/") Then
                    Send(txtGuess.Text) ' send as raw command
                Else
                    Send("&LOBBY&" & txtGuess.Text)
                End If

            End If
        Catch ex As Exception
        Finally
            txtGuess.Text = ""
            txtGuess.Focus()
        End Try
    End Sub

    Private Sub btnConnect_Click_1(sender As Object, e As EventArgs) Handles btnConnect.Click
        If String.IsNullOrWhiteSpace(txtName.Text) OrElse String.IsNullOrWhiteSpace(txtIpAddress.Text) Then
            MsgBox("Your name or IP can not be empty.")
            Return
        End If
        If IPAddress.TryParse(txtIpAddress.Text, getIPaddress()) = False Then
            MsgBox("Your IP address is not correct. it must have the format: " & vbCrLf & "x . y . z . a" & vbCrLf & vbCrLf & "Example: " + getIPaddress().ToString())
            Return
        End If
        If txtName.Text.Contains("%") Or txtName.Text.Contains("GUESS") Or txtName.Text.Contains("CLIENT") Or txtName.Text.Contains("WON") Then
            'i give up a little bit
            MsgBox("Your username is invalid - ensure that it does not use the % symbol, or any keywords") 'uses letters and/or numbers only.")
            Return
        End If
        If txtName.Text.Contains(" ") Then
            MsgBox("Your name is invalid - unfortunatly, it must not contain spaces." + vbCrLf + "Valid characers:" + vbCrLf + "QWERTYUIOPASDFGHJKLZXCVBNM1234567890" + vbCrLf + "(upper and lower case)")
            Return
        End If
        connect()
    End Sub

    Private Sub btnColour_Click(sender As Object, e As EventArgs) Handles btnPink.Click, btnPurple.Click, btnBlue.Click,
        btnGreen.Click, btnYellow.Click, btnOrange.Click, btnRed.Click, btnBlack.Click, btnWhite.Click, btnPeach.Click,
        btnBrown.Click, btnYellowGreen.Click, btnLightSkyBlue.Click, btnLightGray.Click, btnGray.Click
        If painter Then
            Dim btn As Button = CType(sender, Button)
            Send(btn.BackColor.ToKnownColor.ToString() + "#")
        End If
    End Sub

    Private Sub timerTimeLeft_Tick(sender As Object, e As EventArgs) Handles timerTimeLeft.Tick
        timeLeft -= 1
        lblTimeLeft.Text = timeLeft.ToString()
        If timeLeft = 0 Then
            timerTimeLeft.Enabled = False
            lblMisc.Text = "Game over!"
            lblStatus.Text = "Time up!"
            painter = False
            allowNewGame()
            Send("TIMEUP") ' Allows server to know things
        ElseIf timeLeft <= 40 AndAlso lblMisc.Text.Contains("drawing") = False Then
            ' Gives the painter 5 seconds to look without anyone knowing who it is
            updateLabels("What is " & whoIsDrawing & " drawing? Guess!", "You are a guesser, person drawing is: " & whoIsDrawing, True)
        ElseIf timeLeft <= 10 AndAlso timeLeft > 5 Then
            lblTimeLeft.ForeColor = Color.Orange
        ElseIf timeLeft <= 5 Then
            lblTimeLeft.ForeColor = Color.Red
        End If
    End Sub

    Private Sub timerChangeBrushSize_Tick(sender As Object, e As EventArgs) Handles timerChangeBrushSize.Tick
        If pensize + changepensize > 0 And pensize + changepensize < 50 Then
            pensize += changepensize
            btnPenSize.Size = New Size(pensize, pensize)
            If pensize Mod 2 = 0 Then btnPenSize.Location = New Point(btnPenSize.Location.X - changepensize, btnPenSize.Location.Y - changepensize)
        End If
    End Sub

    Private Sub btnBrushSize_MouseDown(sender As Object, e As MouseEventArgs) Handles btnSmallerBrush.MouseDown, btnBiggerBrush.MouseDown
        If painter Then
            changepensize = Int(CType(sender, Button).AccessibleDescription)
            timerChangeBrushSize.Enabled = True
        End If
    End Sub

    Private Sub btnBrushSize_MouseUp(sender As Object, e As MouseEventArgs) Handles btnSmallerBrush.MouseUp, btnBiggerBrush.MouseUp
        If painter Then
            timerChangeBrushSize.Enabled = False
            Send(pensize.ToString() + "BRUSHSIZE")
        End If
    End Sub

    Private HasVotedThisGame As Boolean = False
    Private Sub btnVoteToStart_Click(sender As Object, e As EventArgs) Handles btnVoteToStart.Click
        If HasVotedThisGame Then
            btnVoteToStart.Enabled = False
            Return
        End If
        'easier to debug without the 3 person rule
        'If lvLeaderboard.Items.Count <= 3 AndAlso EveryoneCanVote Then
        '    MsgBox("You need to have 3 or more players to start!")
        '    Return
        'End If
        Send("STARTVOTE")
        btnVoteToStart.Enabled = False
        HasVotedThisGame = True
    End Sub

    Private Sub ChatForm_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        'Me.Close()
        e.Cancel = False
        BannedForm.Close()
    End Sub

    Private Sub SetVisibility(bool As Boolean)
        If Me.InvokeRequired Then
            Me.Invoke(Sub() SetVisibility(bool))
            Return
        End If
        Me.Visible = bool
    End Sub

    Private Sub OnApplicationExit(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Send("&Leaving&", True)
        Catch ex As Exception
        End Try
        Try
            _Log.SaveLog("Client close")
        Catch ex As NullReferenceException
            ' No
        End Try
        Threading.Thread.Sleep(1500) ' wait for it to save. 
        Try
            clientSocket.Close()
        Catch ex As Exception
        End Try
        Try
            BugReporting.BugReporting.SendReport(_Log.SavePath, Environment.UserName, "Drawing", "Log #Not_Looked_At")
        Catch ex As Exception
            MsgBox("SendReport failed: " & ex.ToString())
        End Try
        Try
            Try
                If Me.Visible Then
                    SetVisibility(False)
                End If
            Catch ex As Exception
            End Try
            If SkipSuggestion = False Then
                Dim res As Integer = MsgBox("Do you want to send any suggestions?", vbYesNo, "Suggestions")
                If res = vbYes Then
                    Dim sug As String = InputBox("Please enter your suggestion:", "Suggestion Input", "Make it no crash")
                    If Not sug.Contains("Make it no crash") Then
                        BugReporting.BugReporting.SendSuggestion(sug, Environment.UserName, "Drawing", "User suggestion")
                    Else
                        MsgBox("Haha, very funny. Enter an actual suggestion next time.", vbOKOnly, "Suggestions Error")
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
        End
    End Sub

    Private Sub btn_EnterButton_Click(sender As Object, e As EventArgs) Handles btn_EnterButton.Click
        ' Should only be pressed due to Enter
        If Me.ActiveControl Is txtName Then
            btnConnect.PerformClick()
        ElseIf Me.ActiveControl Is txtGuess Then
            btnGuess.PerformClick()
        End If
    End Sub

    Private Sub btnToSettings_Click(sender As Object, e As EventArgs) Handles btnToSettings.Click
        panelSettings.Visible = True
        If SettingsTabControl.SelectedTab.Equals(AdminChatTab) Then
            unseenMentions = 0
            unseenMessages = 0
        End If
        updateSettingsButton()
        updateClientLists()
    End Sub

    Private Sub btnToMain_Click(sender As Object, e As EventArgs) Handles btnToMain.Click
        panelSettings.Visible = False
        updateSettingsButton()
    End Sub

    Public Sub AddPlayerMessage(msg As String, name As String, rank As String, Optional clr As Color = Nothing, Optional bld As Boolean = False, Optional italic As Boolean = False, Optional undl As Boolean = False)
        ' Admin chat message handling
        If Me.InvokeRequired Then
            Me.Invoke(Sub() AddPlayerMessage(msg, name, rank, clr, bld, italic, undl))
            Return
        End If
        If clr = Nothing Then clr = Color.Orange
        Me.AddMsg("[" + rank.Substring(0, 1).ToUpper() + "] " + name + ": " + msg, clr, bld, italic, undl)
    End Sub

    Private Sub DrawForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            Send("&Leaving&", True)
        Catch ex As Exception
        Finally
            OnApplicationExit(Nothing, Nothing)
        End Try
    End Sub

    Private Sub TryBanUser(name As String)
        Dim reason As String = InputBox("What is the reason for banning " + name + "?", "Ban Reason", "No reason given.")
        Send($"/ban {name} {reason}")
    End Sub

    Private Sub TryKickUser(name As String)
        Dim reason As String = InputBox("What is the reason for kicking " + name + "?", "Kick Reason", "No reason given.")
        Send($"/kick {name} {reason}")
    End Sub

    Private Sub dgv_AdminPlace_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_AdminPlace.CellContentClick
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        selectedRankIndex = -1
        Dim grid = DirectCast(sender, DataGridView)
        If TypeOf grid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn Then
            If grid.Columns(e.ColumnIndex).Name = "user_Reset" Then
                Dim userName As String = grid.Rows.Item(e.RowIndex).Cells.Item(0).Value
                If Users.ContainsKey(userName) Then
                    Send(userName & "&RESET&")
                End If
            ElseIf grid.Columns(e.ColumnIndex).Name = "user_Ban" Then
                Dim userName As String = grid.Rows.Item(e.RowIndex).Cells.Item(0).Value
                If Users.ContainsKey(userName) AndAlso userName <> txtName.Text Then
                    TryBanUser(userName)
                End If
            ElseIf grid.Columns(e.ColumnIndex).Name = "user_Kick" Then
                Dim userName As String = grid.Rows.Item(e.RowIndex).Cells.Item(0).Value
                If Users.ContainsKey(userName) AndAlso userName <> txtName.Text Then
                    TryKickUser(userName)
                End If
            End If
        ElseIf TypeOf grid.Columns(e.ColumnIndex) Is DataGridViewTextBoxColumn Then
            Dim row As DataGridViewRow = grid.Rows.Item(e.RowIndex)
            Dim textbox As DataGridViewTextBoxCell = DirectCast(row.Cells.Item(5), DataGridViewTextBoxCell)
            MsgBox(textbox.Value)
        End If
    End Sub
    Private selectedRankIndex As Integer = -1

    Private DisregardAutoStart As Boolean = False
    Private Sub cbAutoStart_CheckedChanged(sender As Object, e As EventArgs) Handles cbAutoStart.CheckedChanged
        If DisregardAutoStart Then Return
        Send("autostart" & cbAutoStart.Checked.ToString())
    End Sub

    Private Sub txtIPAddress_DoubleClick(sender As Object, e As EventArgs) Handles txtIpAddress.DoubleClick
        txtIpAddress.ReadOnly = False
    End Sub

    Private Sub cb_WhoCanVote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_WhoCanVote.SelectedIndexChanged
        If StillLoading Then Return
        Send("whocanvote" + cb_WhoCanVote.SelectedItem)
    End Sub

    Private Sub cb_Difficulty_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_Difficulty.SelectedIndexChanged
        If StillLoading Then Return
        Send("difficulty" + cb_Difficulty.SelectedItem.ToString().Trim())
    End Sub

    Private Sub btn_ForceManager_Click(sender As Object, e As EventArgs) Handles btn_forcestart.Click
        Send("/forcestart")
    End Sub

    Private Sub dgv_AdminPlace_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_AdminPlace.CellClick
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        Dim grid = DirectCast(sender, DataGridView)
        With grid.SelectedCells.Item(0).Style ' Selecting it makes no
            .SelectionBackColor = .BackColor ' Difference to how it looks.
            .SelectionForeColor = .ForeColor
        End With
    End Sub

    Private Sub btn_seebans_Click(sender As Object, e As EventArgs) Handles btn_seebans.Click
        BannedForm.ShowDialog()
        BannedForm.UpdateView()
    End Sub

    Public Sub CallForUnban(name As String)
        ' From the Banned Form - when someone asks for a ban to be revoked
        Send(name + "&UNBAN&")
    End Sub

    Public Sub CallForIPBanToggle(name As String)
        ' From the Banned Form - server will by default ban by IP; meaning no accoutns can join from the specified IP.
        Send(name + "&TOGGLE_IP&")
    End Sub

    Public Sub CallForSerialBanToggle(name As String)
        ' From the Banned Form - disabled by default, prevents any connection from the serial - even if they change IPs
        Send(name + "&TOGGLE_SERIAL&")
    End Sub

    Private Sub txtName_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles txtName.MouseDoubleClick
        If Not String.IsNullOrWhiteSpace(txtName.Text) Then
            ' Send connect to server with &ADMIN& after name.
            connect(True)
        End If
    End Sub

    Private Sub txtIPAddress_MouseDoubleClick(sender As Object, e As MouseEventArgs)
        txtIpAddress.ReadOnly = False
    End Sub

    Private Sub lblMisc_MouseEnter(sender As Object, e As EventArgs) Handles lblMisc.MouseEnter
        If painter AndAlso lblMisc.Text.Contains("You are drawing") Then
            lblMisc.BackColor = Color.FromArgb(145, 184, 173)
        End If
    End Sub

    Private Sub lblMisc_MouseLeave(sender As Object, e As EventArgs) Handles lblMisc.MouseLeave
        If painter AndAlso lblMisc.Text.Contains("You are drawing") Then
            lblMisc.BackColor = Color.Black
        End If
    End Sub

    Private Sub btn_forceend_Click(sender As Object, e As EventArgs) Handles btn_forceend.Click
        Send("/forceend")
    End Sub

    Private Enum LeaderBoard
        ' Easier to view tag constants
        Report
        SendMessage
        Kick
        Reset
        Ban
        SetUser
        SetMod
        SetAdmin
        BlockUser
    End Enum

    Private selectedUser As String = ""
    Private Sub lvLeaderboard_MouseUp(sender As Object, e As MouseEventArgs) Handles lvLeaderboard.MouseUp
        If e.Button <> Windows.Forms.MouseButtons.Right Then Return
        Dim selectedIndex As Integer = -1
        Try
            selectedIndex = lvLeaderboard.SelectedIndices.Item(0)
        Catch ex As Exception
            _Log.LogError(ex.ToString())
        End Try
        If selectedIndex < 0 Then Return
        Dim userSelected As String = Users.Keys(selectedIndex)
        If String.IsNullOrWhiteSpace(userSelected) Then Return
        If userSelected = txtName.Text Then Return ' Dont let them do it to themselves.
        selectedUser = userSelected
        Dim cms = New ContextMenuStrip
        cms.BackColor = Color.FromKnownColor(KnownColor.Control)
        If IsModerator Then
            cms.Items.Add(New ToolStripControlHost(New Label With {.Text = "Name: " & Users(userSelected).ActualName, .BackColor = Color.FromKnownColor(KnownColor.Control)}))
        Else
            Dim item1 = cms.Items.Add("Report")
            item1.Tag = LeaderBoard.Report
            AddHandler item1.Click, AddressOf menuChoice
        End If
        Dim itemSendMessage = cms.Items.Add("Send Message")
        itemSendMessage.Tag = LeaderBoard.SendMessage
        AddHandler itemSendMessage.Click, AddressOf menuChoice
        Dim itemBlock = cms.Items.Add("Block User")
        itemBlock.Tag = LeaderBoard.BlockUser
        AddHandler itemBlock.Click, AddressOf menuChoice
        If IsModerator Then
            Dim item6 = cms.Items.Add("Kick")
            item6.Tag = LeaderBoard.Kick
            AddHandler item6.Click, AddressOf menuChoice
        End If
        If IsAdmin Then
            Dim item2 = cms.Items.Add("Reset")
            item2.Tag = LeaderBoard.Reset
            AddHandler item2.Click, AddressOf menuChoice
            Dim item3 = cms.Items.Add("Ban")
            item3.Tag = LeaderBoard.Ban
            AddHandler item3.Click, AddressOf menuChoice
        End If
        If IsManager Then
            Dim item4 = cms.Items.Add("Set User")
            item4.Tag = LeaderBoard.SetUser
            item4.BackColor = Color.OrangeRed
            AddHandler item4.Click, AddressOf menuChoice
            Dim item5 = cms.Items.Add("Set Mod")
            item5.Tag = LeaderBoard.SetMod
            item5.BackColor = Color.Red
            AddHandler item5.Click, AddressOf menuChoice
            Dim item7 = cms.Items.Add("Set Admin")
            item7.Tag = LeaderBoard.SetAdmin
            item7.BackColor = Color.Red
            AddHandler item7.Click, AddressOf menuChoice
        End If
        cms.Show(lvLeaderboard, e.Location)
    End Sub
    Private Sub menuChoice(ByVal sender As Object, ByVal e As EventArgs)
        ' Handles right-click menu choices from the leaderboard
        Dim item = CType(sender, ToolStripMenuItem)
        Dim selection = CInt(item.Tag)
        If selection = LeaderBoard.Report Then
            Dim reason = InputBox("Reason for your report again " & selectedUser & ":", "Report Reason")
            If String.IsNullOrWhiteSpace(reason) Then Return
            Send(selectedUser + ";" + reason + "&REPORT&")
        ElseIf selection = LeaderBoard.Reset Then
            Send(selectedUser + "&RESET&")
        ElseIf selection = LeaderBoard.Ban Then
            TryBanUser(selectedUser)
        ElseIf selection = LeaderBoard.SetUser Or selection = LeaderBoard.SetMod Or selection = LeaderBoard.SetAdmin Then
            Dim rankName = item.Text.Replace("Set ", String.Empty)
            Dim rank = 0
            If rankName = "Mod" Then
                rank = 1
            ElseIf rankName = "Admin" Then
                rank = 2
            End If
            Send(selectedUser + ";" + rank.ToString() + "&SETRANK&")
        ElseIf selection = LeaderBoard.Kick Then
            TryKickUser(selectedUser)
        ElseIf selection = LeaderBoard.SendMessage Then
            Dim msg = InputBox("Type your message for " & selectedUser, "Message")
            If String.IsNullOrWhiteSpace(msg) Then Return
            Send(msg + ";" + selectedUser + "&U_MESSAGE&")
        ElseIf selection = LeaderBoard.BlockUser Then
            Send(selectedUser + "&BLOCK&")
        Else
            _Log.LogWarn("Unknown tag: " & selection.ToString())
        End If
    End Sub

    Private Sub btn_seeAchat_Click(sender As Object, e As EventArgs) Handles btn_seeAchat.Click
        'ChatForm.Show()
    End Sub

#End Region
#Region "Admin Chat Subs"
    Private unseenMessages As Integer = 0
    Private unseenMentions As Integer = 0
    Private ReadOnly Property AdminBtnMessages As String
        Get
            If unseenMentions > 0 Then
                Return " (" & unseenMentions & ")"
            End If
            Return ""
        End Get
    End Property
    Private Sub UpdateUnSeen()
        Dim page As TabPage = SettingsTabControl.TabPages(AdminChatTab.Name)
        If page Is Nothing Then Return
        page.Text = "Admin Chat (" & unseenMessages.ToString() & ")"
        If unseenMessages = 0 Then
            page.Text = "Admin Chat"
        ElseIf unseenMessages > 99 Then
            page.Text = "Admin Chat (99+)"
        End If
    End Sub
    Private Sub AddUnseenMessage(Optional msg As String = "")
        '  No. of messages that the player has not seen in the admin chat
        Dim index As Integer = SettingsTabControl.TabPages.IndexOf(AdminChatTab)
        If SettingsTabControl.SelectedIndex <> index Then
            unseenMessages += 1
        Else
            unseenMessages = 0
        End If
        UpdateUnSeen()
        If msg <> "" Then
            If msg.Contains("@" & txtName.Text) Or msg.Contains("@everyone") Then
                unseenMentions += 1
            End If
        End If
    End Sub
    Public Sub AddMsg(msg As String, Optional clr As Color = Nothing, Optional bld As Boolean = False, Optional italic As Boolean = False, Optional undl As Boolean = False)
        ' Adds a message to the admin-chat textbox, with the specified fonts etc.
        rtb_Chat.ReadOnly = False
        If clr = Nothing Then clr = Color.Black
        Dim fntStyle As New FontStyle()
        fntStyle = FontStyle.Regular
        Dim now As DateTime = DateTime.Now()
        rtb_Chat.SelectionColor = Color.Black
        rtb_Chat.SelectionStart = rtb_Chat.Text.Length
        rtb_Chat.AppendText("[" + now.ToShortTimeString() + ":" + now.Second.ToString() + "] ")
        rtb_Chat.SelectionColor = clr
        If bld Then fntStyle += FontStyle.Bold
        If italic Then fntStyle += FontStyle.Italic
        If undl Then fntStyle += FontStyle.Underline
        rtb_Chat.SelectionFont = New Font(rtb_Chat.Font, fntStyle)
        rtb_Chat.AppendText(msg)
        rtb_Chat.AppendText(vbCrLf)
        rtb_Chat.ReadOnly = True
        AddUnseenMessage(msg)
        If panelSettings.Visible = False Then
            If clr = Color.Black Then Return
            btnToSettings.BackColor = clr
            timerFlashWhenMsg.Start()
        End If
    End Sub
    Private Sub txtSend_KeyUp(sender As Object, e As KeyEventArgs) Handles txtSend.KeyUp
        If e.KeyCode = Keys.Enter Then
            btnSend.PerformClick()
        End If
    End Sub

    Private Sub btnSend_Click(sender As Object, e As EventArgs) Handles btnSend.Click
        Dim str As String = txtSend.Text
        str = str.Replace(vbCrLf, String.Empty)
        If String.IsNullOrWhiteSpace(str) Then
            txtSend.Text = ""
            Return
        End If
        If str.Contains("&") Or str.Contains("%") Then
            txtSend.Text = ""
            AddMsg(">> Error: that message is actually invalid", Color.Red)
            Return
        End If
        txtSend.Text = ""
        MessageAChat(str)
    End Sub

    Private Sub AdminTabControl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles SettingsTabControl.SelectedIndexChanged
        Dim indexofachat = SettingsTabControl.TabPages.IndexOf(AdminChatTab)
        If indexofachat = SettingsTabControl.SelectedIndex Then
            unseenMessages = 0
            unseenMentions = 0
            UpdateUnSeen()
        End If
    End Sub

    Private Sub timerFlashWhenMsg_Tick(sender As Object, e As EventArgs) Handles timerFlashWhenMsg.Tick
        Dim tim As Timer = CType(sender, Timer)
        tim.Stop()
        btnToSettings.BackColor = Color.FromArgb(145, 204, 203)
        updateSettingsButton()
    End Sub

#End Region
#Region "Section - Command line handling"
    Private TryAdminAlways As Boolean = False
    Private SkipSuggestion As Boolean = False

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub HandleCommandLine(args As List(Of String))
        ' Either commands inputted directly when running..
        ' Or commands placed in a "commandline.txt" file in the same location as client's exe.
        If args.Count = 0 Then Return
        For Each argument As String In args
            Try
                If String.IsNullOrWhiteSpace(argument) Then Continue For
                If argument.Substring(0, 1) = "#" Then Continue For
                If argument.Split(" ").Count > 2 Or argument.Split(" ").Count = 0 Then Continue For
                If argument.Substring(0, 1) = "-" Then argument = argument.Substring(1)
                Dim command As String = argument.Split(" ")(0)
                Dim value As String = Nothing
                Try
                    value = argument.Split(" ")(1)
                Catch ex As Exception
                    _Log.LogWarn(ex.ToString())
                End Try
                _Log.LogMsg("Command: " & argument)
                If command = "name" Then
                    txtName.Text = value
                    txtName.ReadOnly = True
                ElseIf command = "ip" Then
                    txtIpAddress.Text = value
                    txtIpAddress.ReadOnly = True
                ElseIf command = "admin" Then
                    TryAdminAlways = Boolean.Parse(value)
                ElseIf command = "suggest" Then
                    SkipSuggestion = Not Boolean.Parse(value)
                ElseIf command = "skipIP" Then
                    SkipIpGet = True
                ElseIf command = "skipVersion" Then
                    SkipVersionChecks = True
                End If
            Catch ex As Exception
                _Log.LogError("Invalid command line: " & argument)
                _Log.LogError(ex.ToString())
            End Try
        Next
    End Sub

    Private Sub dgv_AdminPlace_RankChanged(sender As Object, e As EventArgs)
        ' Im not sure if this is even used?
        If selectedRankIndex = -1 Then
            MsgBox("You cannot change the rank like that. Please try again")
            Return
        End If
        Dim sendingComboEdit = TryCast(sender, DataGridViewComboBoxEditingControl)
        Dim usr As User = Users.Values(selectedRankIndex)
        Send("&SETRANK&" & usr.Name & ";" & sendingComboEdit.SelectedIndex.ToString())
        selectedRankIndex = -1
    End Sub

    Private ReadOnly Property MyScore As Integer
        Get
            Dim amnt As Integer = 0
            Try
                amnt = Users(txtName.Text).Score
            Catch ex As Exception
                _Log.LogWarn("Getting score: " & ex.ToString())
            End Try
            Return amnt
        End Get
    End Property

    Private Sub timerDiscordUpdate_Tick(sender As Object, e As EventArgs) Handles timerDiscordUpdate.Tick
        If clientSocket.Connected Then
            Dim afterStatus As String = " (" & txtName.Text & ") | Score: " & MyScore
            If InLobby Then
                'DiscordDLL.DiscordApp.UpdatePres("In Lobby" & afterStatus, "", 0, 0, DiscordDLL.DiscordApp.GameState.Lobby)
                'DiscordDLL.DiscordApp.UpdatePres(New DiscordDLL.DiscordApp.GameStatusInfo(txtName.Text, MyScore))
                'DISCORD_RELOAD()
            Else
                Dim now As DateTime = DateTime.UtcNow()
                Dim timeAdd As DateTime = now.AddSeconds(timeLeft)
                Dim nowLong As Long = CType(TimeToUnix(now), Long)
                Dim timeAddLong As Long = CType(TimeToUnix(timeAdd), Long)
                gameStartTimeStamp = nowLong
                If painter Then
                    'DiscordDLL.DiscordApp.UpdatePres("In Game" & afterStatus, "Painter", nowLong, timeAddLong, DiscordDLL.DiscordApp.GameState.Painter)
                Else
                    'DiscordDLL.DiscordApp.UpdatePres("In Game" & afterStatus, "Guesser", nowLong, timeAddLong, DiscordDLL.DiscordApp.GameState.Guesser)
                End If
                DISCORD_RELOAD()
            End If
        Else
            'DiscordDLL.DiscordApp.UpdatePres(New DiscordDLL.DiscordApp.GameStatusInfo("", -1))
            'DiscordDLL.DiscordApp.UpdatePres("Not connected", "User is not connected to a server", 0, 0, DiscordDLL.DiscordApp.GameState.Lobby)
            DISCORD_RELOAD()
        End If
    End Sub

    Private Sub switchToolBgColours(tool As Button, colour As Color)
        btnStraightLineTool.BackColor = Color.White
        btnRectangleTool.BackColor = Color.White
        btnCircleTool.BackColor = Color.White

        tool.BackColor = colour
    End Sub

    Private Sub btnStraightLine_Click(sender As Object, e As EventArgs) Handles btnStraightLineTool.Click
        DrawingStraightLineTool = Not DrawingStraightLineTool
        DrawingRectangleTool = False
        DrawingCircleTool = False
        priorCanvasImage = New Bitmap(CanvasImage)
        If DrawingStraightLineTool Then
            pt1 = Nothing
            pt2 = Nothing
            startPoint = Nothing
            switchToolBgColours(btnStraightLineTool, Color.Red)
        Else
            switchToolBgColours(btnStraightLineTool, Color.White)
        End If
    End Sub

    Private Sub btnRectangleTool_Click(sender As Object, e As EventArgs) Handles btnRectangleTool.Click
        DrawingRectangleTool = Not DrawingRectangleTool
        DrawingStraightLineTool = False
        DrawingCircleTool = False
        priorCanvasImage = New Bitmap(CanvasImage)
        If DrawingRectangleTool Then
            pt1 = Nothing
            pt2 = Nothing
            startPoint = Nothing
            switchToolBgColours(btnRectangleTool, Color.Red)
        Else
            switchToolBgColours(btnRectangleTool, Color.White)
        End If
    End Sub

    Private Sub btnCircle_Click(sender As Object, e As EventArgs) Handles btnCircleTool.Click
        DrawingCircleTool = Not DrawingCircleTool
        DrawingStraightLineTool = False
        DrawingRectangleTool = False
        priorCanvasImage = New Bitmap(CanvasImage)
        If DrawingCircleTool Then
            pt1 = Nothing
            pt2 = Nothing
            startPoint = Nothing
            switchToolBgColours(btnCircleTool, Color.Red)
        Else
            switchToolBgColours(btnCircleTool, Color.White)
        End If
    End Sub

    Private Sub btnMasterlistRefresh_Click(sender As Object, e As EventArgs) Handles btnMasterlistRefresh.Click
        lblMasterListInfo.Text = "Fetching masterlist.."
        Dim mlThread = New Threading.Thread(AddressOf SetMasterList)
        mlThread.Name = "MasterlistGet"
        mlThread.Start()
    End Sub

    Private Sub dgv_servers_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_servers.CellContentClick
        If e.RowIndex < 0 Then Return
        Dim grid = DirectCast(sender, DataGridView)
        grid.ClearSelection()
        Dim serverName As String = grid.Rows.Item(e.RowIndex).Cells.Item(0).Value
        If TypeOf grid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn Then
            ' Clicked join button
            Try
                Dim serv As ClientDisplayServer = MasterList(serverName.Replace(" (PASSWORD)", ""))
                txtIpAddress.Text = serv.IPAddress
                btnConnect.PerformClick()
            Catch ex As Exception
            End Try
        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub lb_difficulties_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lb_difficulties.SelectedIndexChanged
        If lb_difficulties.SelectedIndex < 0 Then
            Return
        End If
        Dim selItem As String = lb_difficulties.SelectedItem
        Send("&DIFFICULTY&" + selItem)
    End Sub

    Private Sub txtPassword_MouseDoubleClick(sender As Object, e As MouseEventArgs) Handles txtPassword.MouseDoubleClick
        txtPassword.Enabled = True
        txtPassword.ReadOnly = False
    End Sub

#End Region
End Class