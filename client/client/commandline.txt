﻿# Commandline for debug/release testing purposes
# Available commands:
# -name [string]          - Sets and disables the name
# -ip [string-ip format]  - Sets and disable the IP
# -admin [bool]           - Always tries to join as admin
# -suggest [bool]         - Toggle whether or not suggest MsgBox opens
# -skipIP                 - If present, skips getting IP from bitbucket
# -skipVersion            - If present, skips version checks
-skipIP
-skipVerison
-suggest False
