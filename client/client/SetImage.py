"""
This module selects one random image file from the directory:
 -> '(Project Directory)\Resources\Paintings'
And makes that the background image
Making an edit to this file will automatically be copied to the output directory
"""
import os
import random
import sys
try:
    from Pillow import PIL
except ImportError:
    pass
def module_exists(module_name):
    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True
from shutil import copyfile
def main():
    yourpath = os.path.dirname(os.path.realpath(__file__))
    print ("Current path:", yourpath)
    parent = os.path.abspath(os.path.join(yourpath, os.pardir))
    print ("Parent:", parent)
    parent = os.path.abspath(os.path.join(parent, os.pardir))
    print("Next parent:", parent)
    myres = parent + "\Resources"
    print("Resource folder:", myres)
    paintpath = myres + "\Paintings"
    print("Painting folders:", paintpath)
    paintings = os.listdir(paintpath)
    print("Files or folders in path:", paintings)
    valid = []
    for img in paintings:
        if "." not in img:
            continue # skips folders
        isValid = False
        path = os.path.join(paintpath, img)
        try:
            if module_exists("Pillow"):
                with Image.open(path) as im:
                    isValid = True
            else:
                isValid = True
        except IOError as e:
            print(img, "is not a valid image file. (", e,")")
        if isValid:
            valid.append(img)
        else:
            print(img, "is invalid. (must be jpeg)")
    selectedPaint = ""
    if "Force.png" in valid:
        print("Force selecting the 'Force.png' file.")
        selectedPaint = "Force.png"
    else:
        if len(valid) == 0:
            print("There are no valid images in directory.")
        else:
            print("Valid images:", valid)
            selectedPaint = random.choice(valid)
    if selectedPaint != "":
        print("Selected image for this build:", selectedPaint)
        try: 
            copyfile(paintpath + "\\" + selectedPaint, myres + "\Chosen.jpg")
            print("Image moving complete")
        except Exception as e:
                print(e)
    else:
        print("No image has been selected")

if sys.argv[1] == "Debug":
    print("-- DEBUG: Not updating image.")
else:
    main()
