﻿Imports client.DrawForm

Public Class BannedForm
    Private Bans As List(Of Ban) = DrawForm.Bans
    Private Sub BannedForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        UpdateView()
    End Sub
    Public Sub UpdateView()
        Dim allColors As New List(Of Color) From {Color.LightBlue, Color.LightCoral, Color.LightCyan, Color.LightGoldenrodYellow, Color.LightGray, Color.LightGreen, Color.LightPink, Color.LightSalmon, Color.LightSeaGreen, Color.LightSkyBlue, Color.LightSlateGray, Color.LightSteelBlue, Color.LightSteelBlue, Color.LightYellow}
        Dim allBan As New Dictionary(Of Net.IPAddress, Integer) ' IP: occurences
        Bans = DrawForm.Bans
        ' Format currently:
        ' Name | Full name | Serial | IP | Ip ban | Serial ban | Parden
        '   0  |     1     |   2    |  3 |    4   |     5      |   6
        If DrawForm.IsAdmin = False Then
            ' Is an admin only!
            dgv_banned.Columns.Item(2).Visible = False
            dgv_banned.Columns.Item(4).Visible = False
            dgv_banned.Columns.Item(5).Visible = False
            dgv_banned.Columns.Item(6).Visible = False
            '
            dgv_banned.Columns.Item(4).ReadOnly = True
            dgv_banned.Columns.Item(5).ReadOnly = True
        Else
            dgv_banned.Columns.Item(2).Visible = True
            dgv_banned.Columns.Item(4).Visible = True
            dgv_banned.Columns.Item(5).Visible = True
            dgv_banned.Columns.Item(6).Visible = True
            '
            dgv_banned.Columns.Item(4).ReadOnly = False
            dgv_banned.Columns.Item(5).ReadOnly = False
        End If
        If Bans.Count = 0 Then
            Me.Close() ' no use keeping this up.
        End If
        dgv_banned.Rows.Clear()
        For Each usr As Ban In Bans
            Dim row As String() = New String() {usr.Name, usr.ActualName, usr.Serial, usr.IP.ToString(), "", "", "Parden"}
            If allBan.ContainsKey(usr.IP) Then
                allBan(usr.IP) += 1
            Else
                allBan(usr.IP) = 1
            End If
            dgv_banned.Rows.Add(row)
            dgv_banned.Rows.Item(dgv_banned.Rows.Count - 1).Cells.Item(4).Value = usr.IP_Ban
            dgv_banned.Rows.Item(dgv_banned.Rows.Count - 1).Cells.Item(5).Value = usr.Serial_Ban
        Next
        For Each ban_ As KeyValuePair(Of Net.IPAddress, Integer) In allBan
            If ban_.Value = 1 Then Continue For
            If allColors.Count = 0 Then
                Continue For
            End If
            Dim toBeColor As Color = allColors.Item(0)
            allColors.RemoveAt(0)
            For Each item As DataGridViewRow In dgv_banned.Rows
                If item.Cells().Item(3).Value = ban_.Key.ToString() Then
                    item.DefaultCellStyle.BackColor = toBeColor
                End If
            Next
        Next
        Me.Text = "Banned Players: " & Bans.Count.ToString()
    End Sub

    Private Sub dgv_banned_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv_banned.CellContentClick
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        Dim grid = DirectCast(sender, DataGridView)
        Dim userName As String = grid.Rows.Item(e.RowIndex).Cells.Item(0).Value
        If TypeOf grid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn Then
            DrawForm.CallForUnban(userName)
        ElseIf TypeOf grid.Columns(e.ColumnIndex) Is DataGridViewCheckBoxColumn Then
            If e.ColumnIndex = 4 Then
                ' IP
                DrawForm.CallForIPBanToggle(userName)
            ElseIf e.ColumnIndex = 5 Then
                ' Serial
                DrawForm.CallForSerialBanToggle(userName)
            End If
        End If
    End Sub
End Class