# Read Me
## Setting up

 - Download and run the client
 - It may download needed files, wait for this
 - Enter name into lower box (the IP address may be added automatically)
 - Click connect and wait.
 - You should now be in the lobby; one can talk to other connected players and potentially vote to start
Voting to start may be disabled such that only Moderators+ can vote.
Flooding (repeatedly sending multiple messages in a short space of  time) will result in you getting muted.
## How to Play
 - A random player and item to draw will be chosen
	 - This player will draw the item, and the other players will be 'guessers'
	 - Some items have aliases: giving any of the aliases will result in a correct guess
 - If someone guesses correctly, they and the painter will get one point.
 - If no one guesses within 50 seconds, the game will be over
 - The painter's name will only be shown when 40 seconds remain
 - The painter must hover over the black box to see what they must draw.
## Who is involved
 - Liliana
 - Alex
 - *you?*
## How does it work?
### Ranks
There are four 'ranks' that give access to certain permissions:

 - Moderator
	 - Can kick, given the target has been reported.
	 - Can see admin chat
 - Admin
	 - Can kick, regardless of reports
	 - Can ban
	 - Can reset someone's score
	 - Can change some settings within the game
 - Manager
	 - Can unban people
	 - Can force start/end games
### Settings
#### Drawing Settings
 - **Who can vote**: Everyone / Admins, if admins: only Moderator+ can vote, and the game only starts when *all* staff have voted.
 - **Word Difficulty**: Easy / Medium / Hard, determines which JSON config file the words are picked from.
#### Client Command Line Settings
 - **-name** ***[string]*** - Forces the name to be the one given
 - **-ip** ***[ipv4 format]*** : Sets and disables the IP
 -  **-admin** *[bool]* - Always tries to join as admin (user can do this via Double-click) -- will give Manager rights if IP is same as the server
 - **-suggest** ***[bool]*** -  toggles whether the client will open the suggestion-giving messagebox on close.
 - **-skipIP** - skips IP getting from the connection.txt file
 - **-skipVersion** - skips version checks

# Contributing
## Where can I see development?
There is a trello page at: https://trello.com/b/89uCSzRU/drawing
## How can I help?
Suggestions are welcome at https://trello.com/b/tbWB7YSo/drawing-suggesitons

If you think you will be able to code up your suggestion **(after putting it on the Suggestion trello)** then please say so, then fork the repository:

https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html

Then you can make your changes, and make a pull request back to this one.